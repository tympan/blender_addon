from os import path
import unittest

from numpy.testing import (
    assert_almost_equal,
)
import bpy

from blty import (
    testutils,
)
from blty.io_xml_project import (
    XMLImporter,
)
from blty.testutils import BlTYBaseTests
from blty.tympan_utils import bmesh_from_mesh


HAS_TYMPAN = True
try:
    import tympan  # noqa
except ImportError:
    HAS_TYMPAN = False


HERE = path.abspath(path.dirname(__file__))


def datapath(*parts):
    return path.join(HERE, 'data', *parts)


@unittest.skipUnless(HAS_TYMPAN, 'tympan Python library not available')
class XMLParserTests(BlTYBaseTests):

    expected_objs = {
        'Source ponctuelle 0': {
            'coords': [0., 2., 1.2],
            'spectrum': [10.] * 31,
        },
        'Source ponctuelle 1': {
            'coords': [1., 2, 3.1],
            'spectrum': [1.] * 31,
        },
        'Source ponctuelle A': {
            'coords': [-94, 115, 2],
            'spectrum': [92.] * 31,
        },
        'Source ponctuelle B': {
            'coords': [-90, -87, 2],
            'spectrum': [102.] * 31,
        },
        'Source alpha': {
            'coords': [56, 78, 2],
            'spectrum': [100]*4 + [120]*4 + [100]*17 + [80]*4 + [100]*2,
        },
        'Source beta': {
            'coords': [-121, 106, 10],
            'spectrum': [150]*24 + [-20]*7,
        },
        'Source teta': {
            'coords': [-121, 106, 130],
            'spectrum': [150]*24 + [-20]*7,
        },
        'Récepteur A': {
            'coords': [-23, -27, 2],
            'spectrum': [
                97.8, 97.8, 97.8, 97.8, 97.8, 97.8, 97.8, 97.7, 97.7,
                97.7, 97.6, 97.5, 97.5, 97.3, 97.2, 97.1, 96.9, 96.7,
                96.5, 96.3, 95.9, 95.5, 94.8, 93.8, 44.6, 22.9, 20.2,
                16.2, 10.1, 20.7, 6.8,
                ],
        },
        'Récepteur A1': {
            'coords': [-23, -27, 2],
            'spectrum': [-200.] * 31,
        },
        'Récepteur B1': {
            'coords': [131, -100, 5],
            'spectrum': [-200.] * 31,
        },
        'Récepteur B2': {
            'coords': [131, -100, 5],
            'spectrum': [
                91.9, 91.9, 91.9, 91.9, 91.9, 91.9, 91.9, 91.8, 91.8,
                91.7, 91.6, 91.5, 91.3, 91.1, 90.9, 90.6, 90.3, 90,
                89.6, 89.2, 88.6, 87.8, 86.6, 84.8, 39.7, 17.2, 13.3,
                7.4, -1.7,  4.5, -16,
            ],
        },
        'Récepteur B3': {
            'coords': [131, -100, 31.3560009],
            'spectrum': [
                91.5, 91.5, 91.5, 91.5, 91.5, 91.5, 91.5, 91.4, 91.4, 91.3,
                91.2, 91.1, 90.9, 90.7, 90.4, 90.2, 89.9, 89.5, 89.1, 88.7,
                88.1, 87.2, 85.9, 84, -56, -56, -56, -56, -56, -56, -56,
            ],
        },
        'Récepteur B4': {
            'coords': [131, -100, 5],
            'spectrum': [
                91.4, 90.9, 90.3, 89.6, 88.8, 88, 87.1, 86.1, 85,
                83.9, 82.7, 81.5, 80.2, 78.8, 77.5, 76.1, 74.8, 73.6,
                72.4, 71.2, 69.9, 68.5, 66.7, 64.4, -56, -56, -56,
                -56, -56, -56, -56,
            ],
        },
        'Récepteur B5': {
            'coords': [131, -100, 5],
            'spectrum': [
                92.3, 91.9, 91.5, 91, 90.4, 89.8, 89.2, 88.5, 87.8,
                87, 86.2, 85.3, 84.4, 83.4, 82.4, 81.3, 80.2, 79.1,
                78, 76.8, 75.4, 73.8, 71.9, 69.3, -56, -56, -56,
                -56, -56, -56, -56,
            ],
        },
        'Récepteur B6': {
            'coords': [131, -100, 5],
            'spectrum': [
                88.1, 87.4, 86.6, 85.8, 85, 84.1, 83.3, 82.4, 81.5,
                80.5, 79.6, 78.6, 77.5, 76.5, 75.4, 74.3, 73.3, 72.3,
                71.2, 70.2, 69.1, 67.8, 66.2, 63.9, -56, -56, -56,
                -56, -56, -56, -56,
            ],
        },
        'Récepteur B7': {
            'coords': [131, -100, 5],
            'spectrum': [
                89, 88.6, 88.2, 87.8, 87.3, 86.9, 86.4, 85.9, 85.3,
                84.7, 84, 83.3, 82.5, 81.6, 80.7, 79.8, 78.8, 77.6,
                76.4, 75.2, 73.7, 72, 70, 67.2, -56, -56, -56,
                -56, -56, -56, -56,
            ],
        },
        'Récepteur 1': {
            'coords': [-126, -111, 2],
            'spectrum': [
                62.7, 62.7, 62.7, 62.7, 62.6, 62.5, 62.4, 62.2, 62,
                61.8, 61.5, 61.4, 61.3, 61.3, 61.2, 61.2, 69, 61,
                60.9, 60.8, 60.6, 60.5, 60.2, 59.9, 59.5, 58.9, 58,
                56.7, 54.7, 51.8, 47.6,
            ],
        },
        'Récepteur 2': {
            'coords': [95, 114, 2],
            'spectrum': [
                58.5, 58.4, 58.3, 58.2, 57.9, 57.6, 57.3, 56, 54.8,
                53.1, 49.3, 46.6, 46.5, 46.3, 46.1, 45.9, 70.1, 45.3,
                44.9, 44.5, 44, 43.3, 42.4, 41.2, 39.5, 36.9, 32.9,
                27, 18.2, 5.4, -13.3,
            ],
        },
        'Machine 0': {
            'coords': [-1, -1, 0]
        },
        'Machine 3': {
            'coords': [1, 1, 11],
        },
        'Machine 4': {
            'coords': [1, 1, 1],
        },
        'Machine 5': {
            'coords': [-1, -1, 0],
            'discretization_groups': {
                'Discretization group': {
                    'spectrum': [
                        120, 120, 120, 120, 120, 120, 120, 120, 120, 90,
                        90, 90,  90, 90, 90, 90, 90, 90, 35, 35, 35, 35,
                        35,  35, 35, 35, 35, 35, 35, 35, 35,
                    ],
                },
            },
        },
        'Machine 6': {
            'coords': [1, 1, 11],
        },
        'Construction 0': {
            'coords': [5, 1, 5],
        },
        'Construction 0/Etage 0': {
            'coords': [-2.56, 1.793, 0],
            'discretization_groups': {
                'default': {
                    'spectrum': [-200] * 31,
                },
            },
            'group_spectra': {
                'default': {
                    'raw_isolation': [15.] * 31,
                    'sabine_inner_absorption': [0.2] * 31,
                    'sabine_outer_absorption': [0.2] * 31,
                },
            },
        },
        'Construction 0/Etage 1': {
            'coords': [-0.299, 1.722, 0],
            'group_spectra': {
                'default': {
                    'raw_isolation': [30.] * 31,
                    'sabine_inner_absorption': [0.3] * 31,
                    'sabine_outer_absorption': [0.4] * 31,
                },
            },
            'discretization_groups': {
                'default': {
                    'spectrum': [-200] * 31,
                },
            },
        },
        'Construction 2/Etage 0': {
            'coords': [14.063, 5.405, 0],
        },
        'Construction 3/Etage 0': {
            'coords': [5.835, 0.334, 0],
        },
        'Courbe de niveau 0': {
            'coords': [0, 0, 120],
            'vertices': [
                (-124, 117, 0), (-135, 108, 0), (-126, 90, 0),
                (-106, 91, 0), (-106, 112, 0), (-124, 117, 0),
            ],
        },
        'Courbe de niveau 1': {
            'coords': [0, 0, 100],
            'vertices': [
                (-127, 125, 0), (-148, 105, 0), (-133, 78, 0),
                (-96, 82, 0), (-93, 115, 0), (-128, 125, 0),
            ],
        },
        'Courbe de niveau 2': {
            'coords': [0, 0, 60],
            'vertices': [
                (-131, 137, 0), (-163, 110, 0), (-142, 67, 0),
                (-87, 72, 0), (-79, 118, 0), (-131, 138, 0),
            ],
        },
        'Courbe de niveau 3': {
            'coords': [0, 0, 60],
            'vertices': [
                (135, 50, 0), (71, 4, 0), (25, -77, 0), (16, -180, 0),
            ],
        },
        "Plan d'eau 0": {
            'coords': [0, 0, 8],
            'vertices': [
                (-23, 37, 0), (58.000, 31.000, 0),
                (79.000, -28.000, 0), (41.000, -65.000, 0),
                (-28.000, -72.000, 0), (-70.000, -48.000, 0),
                (-84.000, -14.000, 0), (-52.000, 32.000, 0),
            ],
        },
        'Ecran mince 0': {
            'coords': [4.013, -11.08, 0],
            'vertices': [
                (40.987, 62.08, 0), (40.987, 62.08, 12),
                (-13.013, 8.08, 0), (-13.013, 8.08, 12),
                (5.987, -2.92, 0), (5.987, -2.92, 12),
                (-32.012, -68.92, 0), (-32.012, -68.92, 12),
                ],
        },
        'Ecran mince 4': {
            'coords': [1.614, 5.75, 0],
            'vertices': [
                (-4.614, 5.25, 0), (-4.614, 5.25, 5),
                (1.386, 5.25, 0), (1.386, 5.25, 5),
                (1.386, -5.75, 0.0), (1.386, -5.75, 5),
                (-3.614, -5.75, 0.0), (-3.614, -5.75, 5),
            ],
        },
        'Hall 1/Etage 14': {
            'coords': [-3, 6.5, 0],
            'vertices': [
                (-27, -13.5, 25), (-27, 13.5, 25),
                (-27, 13.5, 0), (-27, -13.5, 0),
                (27, -13.5, 25), (27, -13.5, 0),
                (27, 13.5, 25), (27, 13.5, 0),
            ],
            'group_spectra': {
                'default': {
                    'raw_isolation': [
                        2, 2, 8, 8, 8, 13, 13, 13, 20, 20, 20, 28, 28,
                        28, 38, 38, 38, 46, 46, 46, 54, 54, 54, 65, 65,
                        65, 60, 60, 60, 40, 40
                    ],
                    'sabine_inner_absorption': [
                        0.2, 0.2, 0.5, 0.5, 0.5, 0.7, 0.7, 0.7, 0.92, 0.92,
                        0.92, 1, 1, 1, 0.99, 0.99, 0.99, 0.9, 0.9, 0.9, 0.83,
                        0.83, 0.83, 0.79, 0.79, 0.79, 0.8, 0.8, 0.8, 0.8, 0.8
                    ],
                    'sabine_outer_absorption': [0.2] * 31,
                },
            },
        },
        'Hall 2/Etage 14': {
            'coords': [-3, 6.5, 0],
            'vertices': [
                (-27, -13.5, 25), (-27, 13.5, 25),
                (-27, 13.5, 0), (-27, -13.5, 0),
                (27, -13.5, 25), (27, -13.5, 0),
                (27, 13.5, 25), (27, 13.5, 0),
            ],
            'group_spectra': {
                'default': {
                    'raw_isolation': [
                        2, 2, 8, 8, 8, 13, 13, 13, 20, 20, 20, 28, 28,
                        28, 38, 38, 38, 46, 46, 46, 54, 54, 54, 65, 65,
                        65, 60, 60, 60, 40, 40
                    ],
                    'sabine_inner_absorption': [
                        0.2, 0.2, 0.5, 0.5, 0.5, 0.7, 0.7, 0.7, 0.92, 0.92,
                        0.92, 1, 1, 1, 0.99, 0.99, 0.99, 0.9, 0.9, 0.9, 0.83,
                        0.83, 0.83, 0.79, 0.79, 0.79, 0.8, 0.8, 0.8, 0.8, 0.8
                    ],
                    'sabine_outer_absorption': [0.2] * 31,
                },
                'group1': {
                    'raw_isolation': [1.] * 31,
                    'sabine_inner_absorption': [
                        0.2, 0.2, 0.5, 0.5, 0.5, 0.7, 0.7, 0.7, 0.92, 0.92,
                        0.92, 1, 1, 1, 0.99, 0.99, 0.99, 0.9, 0.9, 0.9, 0.83,
                        0.83, 0.83, 0.79, 0.79, 0.79, 0.8, 0.8, 0.8, 0.8, 0.8
                    ],
                    'sabine_outer_absorption': [0.2] * 31,
                },
                'group2': {
                    'raw_isolation': [2.] * 31,
                    'sabine_inner_absorption': [
                        0.2, 0.2, 0.5, 0.5, 0.5, 0.7, 0.7, 0.7, 0.92, 0.92,
                        0.92, 1, 1, 1, 0.99, 0.99, 0.99, 0.9, 0.9, 0.9, 0.83,
                        0.83, 0.83, 0.79, 0.79, 0.79, 0.8, 0.8, 0.8, 0.8, 0.8
                    ],
                    'sabine_outer_absorption': [0.2] * 31,
                },
            },
        },
        'Terrain 1': {
            'coords': [0, 0, 0],
            'vertices': [
                (-72.000, 113.000, 0.000),
                (-23.000, 97.000, 0.000),
                (-37.000, 34.000, 0.000),
                (-123.000, 33.000, 0.000),
                (-122.000, 97.000, 0.000),
            ],
        },
        'Terrain 2': {
            'coords': [0, 0, 0],
            'vertices': [
                (74, 74, 0),
                (133.000, 10.000, 0.000),
                (98.000, -51.000, 0.000),
                (42.000, -2.000, 0.000),
            ],
        },
    }

    def check_imported_objects(self, expected):
        for obj_name in expected:
            expect_obj = self.expected_objs[obj_name]
            actual_obj = bpy.data.objects[obj_name]
            assert_almost_equal(actual_obj.location, expect_obj['coords'], 3)
            if 'spectrum' in expect_obj:
                testutils.check_spectrum(actual_obj, expect_obj['spectrum'])
            if 'vertices' in expect_obj:
                vertices = [v.co for v in actual_obj.data.vertices]
                assert_almost_equal(vertices, expect_obj['vertices'], 3)
            if 'group_spectra' in expect_obj:
                for actual_group in actual_obj.tympan_discretization_groups:
                    testutils.check_disctretization_group(actual_group, expect_obj)
            if 'discretization_groups' in expect_obj:
                for actual_group in actual_obj.tympan_discretization_groups:
                    testutils.check_emissivity_discretization_group(
                        actual_group,
                        expect_obj,
                    )

    def test_xml_import_project(self):
        expected = [
            'Source ponctuelle 0',
            'Source ponctuelle 1',
            'Récepteur A1',
            'Récepteur B1',
            'Machine 0',
            'Machine 4',
            'Construction 0',
            'Construction 3/Etage 0',
        ]
        xml_importer = XMLImporter(
            bpy.context,
            datapath('project.xml'),
        )
        xml_importer.import_file()
        self.check_imported_objects(expected)

    def test_xml_import_sp_rp(self):
        expected = [
            'Source alpha',
            'Source beta',
            'Récepteur A',
            'Récepteur B2',
        ]
        xml_importer = XMLImporter(
            bpy.context,
            datapath('sources_ponctuelles_recepteurs_ponctuels.xml'),
        )
        xml_importer.import_file()
        self.check_imported_objects(expected)

    def test_cubic_machine(self):
        expected = [
            'Source beta',
            'Récepteur B2',
            'Machine 0',
        ]
        xml_importer = XMLImporter(
            bpy.context,
            datapath('source_recepteur_machine_cubique_non_emissive.xml'),
        )
        xml_importer.import_file()
        self.check_imported_objects(expected)
        machine = bpy.data.objects['Machine 0']
        self.assertEqual(machine.tympan_type, 'INFRASTRUCTURE')
        self.assertEqual(machine.tympan_subtype, 'MACHINE')
        self.assertEqual(len(machine.data.vertices), 8)

    def test_cylindric_machine(self):
        expected = [
            'Source beta',
            'Récepteur B5',
            'Machine 3',
        ]
        xml_importer = XMLImporter(
            bpy.context,
            datapath('source_recepteur_machine_cylindrique_non_emissive.xml'),
        )
        xml_importer.import_file()
        self.check_imported_objects(expected)
        machine = bpy.data.objects['Machine 3']
        self.assertEqual(machine.tympan_type, 'INFRASTRUCTURE')
        self.assertEqual(machine.tympan_subtype, 'MACHINE')
        self.assertEqual(len(machine.data.vertices), 64)

    def test_cylindric_building(self):
        expected = [
            'Source beta',
            'Récepteur B4',
            'Construction 0',
        ]
        xml_importer = XMLImporter(
            bpy.context,
            datapath('source_recepteur_batiment_cylindrique_non_emissif.xml'),
        )
        xml_importer.import_file()
        self.check_imported_objects(expected)
        building = bpy.data.objects['Construction 0']
        self.assertEqual(building.tympan_type, 'INFRASTRUCTURE')
        self.assertEqual(building.tympan_subtype, 'BUILDING')
        self.assertEqual(len(building.data.vertices), 64)

    def test_polygonal_based_building(self):
        expected = [
            'Source beta',
            'Récepteur B6',
            'Construction 2/Etage 0',
        ]
        xml_importer = XMLImporter(
            bpy.context,
            datapath('source_recepteur_batiment_quelconque_non_emissif.xml'),
        )
        xml_importer.import_file()
        self.check_imported_objects(expected)
        building = bpy.data.objects['Construction 2/Etage 0']
        self.assertEqual(building.tympan_type, 'INFRASTRUCTURE')
        self.assertEqual(building.tympan_subtype, 'BUILDING')
        self.assertEqual(len(building.data.vertices), 16)

    def test_level_curves(self):
        expected = [
            'Source teta',
            'Récepteur B3',
            'Courbe de niveau 0',
            'Courbe de niveau 1',
            'Courbe de niveau 2',
            'Courbe de niveau 3',
        ]
        xml_importer = XMLImporter(
            bpy.context,
            datapath('source_recepteur_courbes_de_niveau.xml'),
        )
        xml_importer.import_file()
        self.check_imported_objects(expected)

    def test_simple_screen(self):
        expected = [
            'Ecran mince 4',
        ]
        xml_importer = XMLImporter(
            bpy.context,
            datapath('ecran_simple.xml'),
        )
        xml_importer.import_file()
        self.check_imported_objects(expected)
        screen = bpy.data.objects['Ecran mince 4']
        self.assertEqual(screen.tympan_type, 'INFRASTRUCTURE')
        self.assertEqual(screen.tympan_subtype, 'SCREEN')
        self.assertEqual(len(screen.data.vertices), 8)

    def test_screen(self):
        expected = [
            'Source beta',
            'Récepteur B7',
            'Ecran mince 0',
        ]
        xml_importer = XMLImporter(
            bpy.context,
            datapath('source_recepteur_ecran.xml'),
        )
        xml_importer.import_file()
        self.check_imported_objects(expected)
        screen = bpy.data.objects['Ecran mince 0']
        self.assertEqual(screen.tympan_type, 'INFRASTRUCTURE')
        self.assertEqual(screen.tympan_subtype, 'SCREEN')
        self.assertEqual(len(screen.data.vertices), 8)

    def test_water_body(self):
        expected = ["Plan d'eau 0"]
        xml_importer = XMLImporter(
            bpy.context,
            datapath('modele_avec_plan_eau.xml'),
        )
        xml_importer.import_file()
        self.check_imported_objects(expected)
        objects = bpy.data.objects
        with bmesh_from_mesh(objects["Plan d'eau 0"].data) as bm:
            assert len(bm.faces) == 1

    def test_results(self):
        expected = [
            'Source ponctuelle A',
            'Source ponctuelle B',
            'Récepteur 1',
            'Récepteur 2',
        ]
        xml_importer = XMLImporter(
            bpy.context,
            datapath('modele_avec_2_calculs.xml'),
        )
        xml_importer.import_file()
        self.check_imported_objects(expected)

    def test_acoustic_properties(self):
        expected = ['Hall 1/Etage 14']
        xml_importer = XMLImporter(
            bpy.context,
            datapath('projet_avec_batiment_proprietes_acoustiques_specifiques.xml'),
        )
        xml_importer.import_file()
        self.check_imported_objects(expected)

    def test_acoustic_properties_by_walls(self):
        expected = ['Hall 2/Etage 14']
        xml_importer = XMLImporter(
            bpy.context,
            datapath('projet_avec_batiment_proprietes_acoustiques_par_murs.xml'),
        )
        xml_importer.import_file()
        self.check_imported_objects(expected)

    def test_grounds(self):
        expected = ['Terrain 1', 'Terrain 2']
        xml_importer = XMLImporter(
            bpy.context,
            datapath('projet_avec_2_terrains.xml'),
        )
        xml_importer.import_file()
        self.check_imported_objects(expected)
        objects = bpy.data.objects
        with bmesh_from_mesh(objects['Terrain 1'].data) as bm:
            assert len(bm.faces) == 1
        with bmesh_from_mesh(objects['Terrain 2'].data) as bm:
            assert len(bm.faces) == 1

    def test_emissive_cubic_infra(self):
        expected = [
            'Source beta',
            'Récepteur B2',
            'Machine 5',
        ]
        xml_importer = XMLImporter(
            bpy.context,
            datapath('source_recepteur_machine_cubique_emissive.xml'),
        )
        xml_importer.import_file()
        self.check_imported_objects(expected)

    def test_emissive_cylindric_infra(self):
        expected = [
            'Source beta',
            'Récepteur B2',
            'Machine 6',
        ]
        xml_importer = XMLImporter(
            bpy.context,
            datapath('source_recepteur_machine_cylindrique_emissive.xml'),
        )
        xml_importer.import_file()
        self.check_imported_objects(expected)

    def test_construction_with_two_floors(self):
        expected = [
            'Construction 0/Etage 0',
            'Construction 0/Etage 1',
        ]
        xml_importer = XMLImporter(
            bpy.context,
            datapath('modele_avec_construction_2_etages_paves.xml'),
        )
        xml_importer.import_file()
        self.check_imported_objects(expected)
