import configparser
import os
from os import path
import tempfile
import unittest
from unittest import mock

import math
import mathutils
import numpy as np
from numpy.testing import (
    assert_almost_equal,
)
import bpy

from blty import (
    altimetry,
    core,
    element_types,
    infrastructure as iftr,
    io_html_result,
    results,
    solve,
    tympan_utils as tu,
)
import blty.element_types as types
from blty.testutils import (
    BlTYBaseTests,
    dummy_report,
)

HAS_TYMPAN = True
try:
    import tympan  # noqa
except ImportError:
    HAS_TYMPAN = False


HERE = path.abspath(path.dirname(__file__))


def datapath(*parts):
    return path.join(HERE, 'data', *parts)


class BlenderObjectsTests(BlTYBaseTests):

    def test_undefined_type(self):
        bpy.ops.object.add(type='EMPTY')
        obj = bpy.context.active_object
        obj_type = types.get_object_type(obj)
        self.assertEqual(obj_type, 'UNDEFINED')

    def test_wrong_type(self):
        bpy.ops.object.add(type='EMPTY')
        obj = bpy.context.active_object
        with self.assertRaises(TypeError):
            types.set_object_type(obj, 'NOT A TYPE')


class SoundSourcesTests(BlTYBaseTests):

    def test_type(self):
        bpy.ops.blty.add_sound_source()
        source = bpy.context.active_object
        source_type = types.get_object_type(source)
        self.assertEqual(source_type, 'SOURCE')

    def test_properties(self):
        bpy.ops.blty.add_sound_source()
        source = bpy.context.active_object
        self.assertIn('tympan_soundsource_properties', source)
        bpy.ops.object.add(type='EMPTY')
        obj = bpy.context.active_object
        self.assertNotIn('tympan_soundsource_properties', obj)

    def test_name(self):
        bpy.ops.blty.add_sound_source()
        s1 = bpy.context.active_object
        self.assertRegex(s1.name, r'Sound source.\d{3}')
        bpy.ops.blty.add_sound_source()
        s2 = bpy.context.active_object
        self.assertRegex(s2.name, r'Sound source.\d{3}')
        self.assertNotEqual(s2.name, s1.name)


class SoundReceiverTests(BlTYBaseTests):

    def test_type(self):
        bpy.ops.blty.add_sound_receiver()
        receiver = bpy.context.active_object
        receiver_type = types.get_object_type(receiver)
        self.assertEqual(receiver_type, 'RECEIVER')

    def test_properties(self):
        bpy.ops.blty.add_sound_receiver()
        receiver = bpy.context.active_object
        self.assertIn('tympan_soundreceiver_properties', receiver)


fake_mesh = (
    [(0, 0, 0),
     (0, 1, 0),
     (1, 1, 0)],
    [[0, 1, 2]],
)


@unittest.skipUnless(HAS_TYMPAN, 'tympan Python library not available')
class AltimetryTestsWithCodeTympan(BlTYBaseTests):

    def test_altimetry_feature_conversion(self):
        context = bpy.context
        bpy.ops.blty.add_level_curve()
        bpy.ops.object.mode_set(mode='OBJECT')
        obj = context.active_object
        obj.location.z += 10.
        vertices, faces = fake_mesh
        obj.data.from_pydata(vertices, [], faces)
        context.view_layer.update()
        bpy.context.scene.landtake_used_as_level_curve = True
        altimesh = core.compute_altimetry(context, dummy_report)
        self.assertIsNotNone(altimesh)
        self.assertEqual(
            len(altimesh.equivalent_site.children['LevelCurve']), 1)
        lc = altimesh.equivalent_site.children['LevelCurve'][0]
        self.assertEqual(list(reversed(lc.shape.coords[:])),
                         [v[:2] for v in vertices]+[tuple(vertices[0][:2])])
        self.assertEqual(lc.altitude, 10.)

    def test_altimetry_operator_with_infra(self):
        bpy.ops.blty.add_building()
        infra = bpy.context.active_object
        infra.scale = scale = (10, 20, 0)
        infra.location = location = (40, 60, 0)
        bpy.context.scene.landtake_used_as_level_curve = True
        bpy.ops.blty.compute_altimetry()
        self.assertIn('altimetry', bpy.data.meshes)
        altimesh = bpy.data.meshes['altimetry']
        vertices = [list(vert.co.xyz) for vert in altimesh.vertices]
        points = [
            np.array(location) + np.array(scale),
            np.array(location) + np.array(scale) * np.array((-1, 1, 1)),
            np.array(location) + np.array(scale) * np.array((1, -1, 1)),
            np.array(location) + np.array(scale) * np.array((-1, -1, 1)),
        ]
        self.assertTrue(all(list(x) in vertices for x in points))

    def test_altimetry_operator_with_infra_and_elevation(self):
        bpy.ops.blty.add_level_curve()
        bpy.ops.object.mode_set(mode='OBJECT')
        obj = bpy.context.active_object
        vertices, faces = (
            [(2, -2, 10),
             (2, 2, 10),
             (-2, 2, 10),
             (-2, -2, 10)],
            [[0, 1, 2, 3]],
        )
        obj.data.from_pydata(vertices, [], faces)
        bpy.ops.blty.add_building()
        bpy.context.scene.landtake_used_as_level_curve = True
        bpy.ops.blty.compute_altimetry()
        altimesh = bpy.data.meshes['altimetry']
        vertices = [list(vert.co.xyz) for vert in altimesh.vertices]
        expected_points = [
            [1.0, 1.0, 10.0],
            [1.0, -1.0, 10.0],
            [-1.0, 1.0, 10.0],
            [-1.0, -1.0, 10.0],
            [-200.0, -200.0, 0.0],
            [200.0, -200.0, 0.0],
            [-200.0, 200.0, 0.0],
            [200.0, 200.0, 0.0],
        ]
        self.assertTrue(all(list(x) in vertices for x in expected_points))

    def test_altimetry_operator_ground(self):
        bpy.ops.blty.add_ground_set()
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.context.scene.landtake_used_as_level_curve = True
        bpy.ops.blty.compute_altimetry()
        altimesh = bpy.data.meshes['altimetry']
        out = [list(vert.co.xy) for vert in altimesh.vertices]
        expected_verts = [
                (-200, -200, 0),
                (-200, 200, 0),
                (200, -200, 0),
                (200, 200, 0)]
        for x, y, z in expected_verts:
            self.assertIn([x, y], out)

    def test_altimetry_ground_material(self):
        bpy.ops.blty.add_ground_set()
        ground_set = bpy.context.active_object
        bpy.ops.blty.add_ground()
        ground_id = altimetry.active_ground_id(ground_set)
        self.assertEqual(ground_id, 1)
        ground = ground_set.tympan_grounds[ground_set.active_ground_index]
        material = ground.material
        self.assertEqual(material.name, 'Ground 1')
        self.assertEqual(ground.resistivity, 1)
        ground.ground_type = 'lawn'
        self.assertEqual(
            ground.resistivity,
            core.GROUND_TYPES['lawn']['resistivity'],
        )
        self.assertEqual(
            [round(c, 3) for c in ground.material.diffuse_color],
            [round(c, 3) for c in core.GROUND_TYPES['lawn']['color']],
        )
        custom_color = (0, 0, 0, 0)
        ground.material.diffuse_color = custom_color
        self.assertEqual(
            ground.resistivity,
            core.GROUND_TYPES['lawn']['resistivity'],
        )
        ground.resistivity = 21
        self.assertEqual(ground.ground_type, 'user_defined')
        self.assertEqual(
            [round(c, 3) for c in ground.material.diffuse_color],
            [round(c, 3) for c in custom_color],
        )

    def test_altimetry_feature(self):
        bpy.ops.blty.add_level_curve()
        obj = bpy.context.active_object
        self.assertTrue(element_types.is_altimetry(obj))
        bpy.ops.mesh.primitive_plane_add()
        obj = bpy.context.active_object
        self.assertNotIn('tympan_altimetry_feature', obj.data)

    @mock.patch('blty.altimetry_builder.AltimetryMesh')
    def test_compute_altimetry_operator_creates_a_mesh(self, AltimetryMesh):
        AltimetryMesh.return_value.mesh.as_arrays.return_value = fake_mesh
        bpy.context.scene.landtake_used_as_level_curve = True
        bpy.ops.blty.compute_altimetry()
        self.assertIn('altimetry', bpy.data.meshes)
        altimesh = bpy.data.meshes['altimetry']
        self.assertEqual(
            [v.co[:] for v in altimesh.vertices],
            fake_mesh[0],
        )

    def test_get_infra_contours(self):
        from blty import altimetry_builder
        bpy.ops.blty.add_building()
        infra = bpy.context.active_object
        infra.scale = scale = (10, 20, 0)
        infra.location = location = (40, 60, 0)
        bpy.context.view_layer.update()
        ground_contours = altimetry_builder.get_infra_contours(infra)
        expected_result = [[
            (np.array(location) + np.array(scale) * np.array((-1, 1, 1)))[:2],
            (np.array(location) + np.array(scale) * np.array((-1, -1, 1)))[:2],
            (np.array(location) + np.array(scale) * np.array((1, -1, 1)))[:2],
            (np.array(location) + np.array(scale))[:2],
        ]]
        np.testing.assert_array_equal(ground_contours, expected_result)

    def test_compute_altimetry_reports_when_errors(self):
        THE_FIRST_ERROR_MESSAGE = "Your geometry is completely broken"
        THE_SECOND_ERROR_MESSAGE = "Some levelcurves are overlapping..."

        def mock_build_altimetry(context, handle_error):
            handle_error(THE_FIRST_ERROR_MESSAGE)
            handle_error(THE_SECOND_ERROR_MESSAGE)
            return None
        report = mock.Mock()
        with mock.patch("blty.altimetry_builder.build_altimetry",
                        side_effect=mock_build_altimetry):
            core.compute_altimetry(bpy.context, report)
        msgs = [args[1] for args, _ in report.call_args_list
                if args[0] == {'ERROR'}]

        self.assertIn(THE_FIRST_ERROR_MESSAGE, msgs)
        self.assertIn(THE_SECOND_ERROR_MESSAGE, msgs)


def create_acoustic_material():
    material = bpy.data.materials.new(name="tympanMaterial")
    material.acoustic_properties.name = material.name
    tu.initialise_spectrum(material.acoustic_properties.spectrum)
    return material


def create_building_with_material(emissive=False):
    bpy.ops.blty.add_building()
    obj = bpy.context.active_object
    material = obj.active_material
    material.tympan_emissivity = emissive
    return (obj, material)


def set_infra_emissive(infra):
    infra.tympan_use_emissivity = True
    iftr.assign_faces_to_discretization_group(infra, 1)


def create_internal_machine(building):
    bpy.ops.mesh.primitive_cube_add()
    machine = bpy.context.active_object
    element_types.to_infrastructure(machine, 'MACHINE')
    machine.parent_building = building
    return machine


def set_infra_emissivity_group(machine, global_amplitudes):
    with tu.bmesh_from_mesh_for_update(machine.data) as bm:
        discretization_index = bm.faces.layers.int.get("discretized")
        for face in bm.faces[2:]:
            face[discretization_index] = 0
    iftr.set_groups_spectrum_from_global(
        machine,
        global_amplitudes,
    )


class InfrastructureTests(BlTYBaseTests):

    def test_type(self):
        bpy.ops.blty.add_building()
        infra = bpy.context.active_object
        infra_type = types.get_object_type(infra)
        self.assertEqual(infra_type, 'INFRASTRUCTURE')
        infra_subtype = types.get_object_subtype(infra)
        self.assertEqual(infra_subtype, 'BUILDING')

    def test_is_infrastructure(self):
        bpy.ops.blty.add_building()
        infra = bpy.context.active_object
        self.assertTrue(types.is_infrastructure(infra))

        bpy.ops.mesh.primitive_cube_add()
        not_infra = bpy.context.active_object
        self.assertFalse(types.is_infrastructure(not_infra))

    def test_create_infrastructure(self):
        bpy.ops.blty.add_building()
        infra = bpy.context.active_object
        self.assertEqual(infra.location.z, 0)
        bpy.ops.transform.transform(
            mode='TRANSLATION',
            value=(1.0, 1.0, 1.0, 1.0),
        )
        self.assertEqual(infra.location.x, 1)
        self.assertEqual(infra.location.z, 0)

    def test_find_used_infrastructures(self):
        infra_names = tuple(("Building {}".format(i) for i in range(5)))
        for name in infra_names:
            bpy.ops.blty.add_building(base_mesh="cube")
            bpy.context.active_object.name = name
        bpy.ops.blty.add_sound_source()
        bpy.ops.blty.add_sound_receiver()
        bpy.ops.mesh.primitive_plane_add()
        bpy.ops.mesh.primitive_cube_add()

        found = list(iftr.find_used_infrastructures(bpy.context))
        results = [infra.name for infra in found]

        self.assertCountEqual(infra_names, results)

    def test_export_mesh(self):
        bpy.ops.blty.add_building()
        obj = bpy.context.active_object
        points, triangles = iftr.export_mesh(obj)
        self.assertEqual(len(points), 8)
        self.assertEqual(len(triangles), 12)

    def test_get_acoustic_material(self):
        blender_material = iftr.get_acoustic_material(None)
        self.assertEqual(blender_material[0], b'default')
        material = tu.create_default_acoustic_material(
            material_name="TympanMaterial"
        )
        blender_material = iftr.get_acoustic_material(material)
        self.assertTrue(
            blender_material[0].startswith(b'TympanMaterial')
        )

    def test_toggle_infra_equivalent_sound_sources(self):
        bpy.ops.blty.add_building()
        infra = bpy.context.active_object
        self.assertFalse(infra.tympan_discretization_groups)
        self.assertFalse(iftr.has_emitting_faces(infra))
        infra.tympan_use_emissivity = True
        self.assertTrue(infra.tympan_discretization_groups[0].spectrum)
        iftr.assign_faces_to_discretization_group(infra, 1)
        self.assertTrue(iftr.has_emitting_faces(infra))
        infra.tympan_use_emissivity = False
        self.assertFalse(iftr.has_emitting_faces(infra))

    def test_apply_object_transforms(self):
        bpy.ops.blty.add_building()
        infra = bpy.context.active_object
        points = [vert.co for vert in infra.data.vertices]
        infra.scale = [10, 10, 10]
        infra.location = [2, 2, 2]
        bpy.context.view_layer.update()
        infra.select_set(True)
        bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_MASS')
        expected = [[-8, -8, 2], [-8, -8, 22], [-8, 12, 2], [-8, 12, 22],
                    [12, -8, 2], [12, -8, 22], [12, 12, 2], [12, 12, 22.]]
        bpy.context.view_layer.update()
        vertices = core.apply_object_transforms(points, infra)
        np.testing.assert_array_almost_equal(expected, vertices, decimal=5)
        infra.rotation_euler = mathutils.Euler(
            (math.radians(90.0), math.radians(90.0), 0),
            'XYZ',
        )
        bpy.context.view_layer.update()
        expected = [expected[3], expected[1], expected[7], expected[5],
                    expected[2], expected[0], expected[6], expected[4]]
        vertices = core.apply_object_transforms(points, infra)
        np.testing.assert_array_almost_equal(expected, vertices, decimal=5)

    @unittest.skipUnless(HAS_TYMPAN, 'tympan Python library not available')
    def test_apply_infrastructures_elevation(self):
        bpy.ops.blty.add_building()
        infra = bpy.context.active_object
        infra.scale = (10, 20, 0)
        infra.location = (40, 60, 0)
        infra.tympan_elevation = 2
        bpy.ops.blty.compute_altimetry()
        infra_co = [(infra.matrix_world @ v.co)
                    for v in infra.data.vertices]
        infra_zco = [co.z for co in infra_co]
        self.assertEqual(infra_zco, [2] * len(infra_co))

    @unittest.skipUnless(HAS_TYMPAN, 'tympan Python library not available')
    @mock.patch('blty.altimetry_builder.AltimetryMesh')
    def test_apply_infrastructures_elevation_with_alti(self, AltimetryMesh):
        fake_mesh = (
            [(1, 0, 1),
             (0, 1, 1),
             (1, 1, 1)],
            [[0, 1, 2]],
        )
        AltimetryMesh.return_value.mesh.as_arrays.return_value = fake_mesh

        bpy.context.scene.landtake_used_as_level_curve = True
        bpy.ops.blty.add_building()
        infra = bpy.context.active_object
        infra.scale = (-1, -1, 0)
        infra.tympan_elevation = 2
        bpy.ops.blty.compute_altimetry()
        self.assertIn('altimetry', bpy.data.meshes)
        infra_co = [(infra.matrix_world @ v.co)
                    for v in infra.data.vertices]
        infra_zco = [co.z for co in infra_co]
        self.assertEqual(infra_zco, [3] * len(infra_co))

    def test_update_infrastructure_subtype(self):
        bpy.ops.blty.add_building()
        infra = bpy.context.active_object
        for subtype in ["MACHINE", "SCREEN", "BUILDING"]:
            infra.tympan_subtype = subtype
            self.assertEqual(infra.tympan_subtype, subtype)

    def test_update_emissive_infrastructure_to_screen(self):
        bpy.ops.blty.add_building()
        infra = bpy.context.active_object
        infra.tympan_use_emissivity = True
        with self.assertRaises(TypeError):
            infra.tympan_subtype = "SCREEN"
        self.assertEqual(infra.tympan_subtype, "BUILDING")

    def test_default_material_at_infra_creation(self):
        bpy.ops.blty.add_building()
        infra = bpy.context.active_object
        default_material = infra.active_material
        spectrum = default_material.acoustic_properties.spectrum
        expected_amplitudes = [0.2] * 31
        assert_almost_equal(
            tu.spectrum_amplitudes(spectrum),
            expected_amplitudes,
        )

    def test_default_material_at_infra_convertion(self):
        bpy.ops.object.add(type="MESH")
        obj = bpy.context.active_object
        element_types.to_infrastructure(obj, 'MACHINE')
        default_material = obj.active_material
        spectrum = default_material.acoustic_properties.spectrum
        expected_amplitudes = [0.2] * 31
        assert_almost_equal(
            tu.spectrum_amplitudes(spectrum),
            expected_amplitudes,
        )

    def test_get_global_emission_spectrum(self):
        expected_amplitudes = [57.78] * 31
        bpy.ops.blty.add_building()
        infra = bpy.context.active_object
        # initialise emission spectrum
        set_infra_emissive(infra)
        spectrum = infra.tympan_discretization_groups[0].spectrum
        tu.initialise_spectrum(spectrum, [50] * 31)
        amplitudes, freqs = iftr.get_global_emission_spectrum_of_machine(infra)
        self.assertEqual(freqs, tu.LOWER_CUTOFF_FREQUENCIES)
        assert_almost_equal(amplitudes, expected_amplitudes, 2)
        # add a new emission spectrum
        infra.tympan_discretization_groups.add()
        spectrum2 = infra.tympan_discretization_groups[1].spectrum
        tu.initialise_spectrum(spectrum2, [150] * 31)
        amplitudes, _ = iftr.get_global_emission_spectrum_of_machine(infra)
        self.assertNotEqual(amplitudes, expected_amplitudes, 2)
        # assign same spectrum as material1 to material2
        tu.initialise_spectrum(spectrum2, [50] * 31)
        amplitudes, _ = iftr.get_global_emission_spectrum_of_machine(infra)
        assert_almost_equal(amplitudes, expected_amplitudes, 2)

    def test_set_groups_spectrum_from_global(self):
        bpy.ops.mesh.primitive_cylinder_add()
        infra = bpy.context.active_object
        element_types.to_infrastructure(infra, 'MACHINE')
        self.assertEqual(len(infra.tympan_discretization_groups), 0)
        set_infra_emissive(infra)
        self.assertEqual(len(infra.tympan_discretization_groups), 1)
        global_amplitudes = [50] * 31
        iftr.set_groups_spectrum_from_global(
            infra,
            global_amplitudes,
        )
        self.assertEqual(len(infra.tympan_discretization_groups), 2)
        amplitudes = []
        with tu.bmesh_from_mesh(infra.data) as bm:
            discretization_index = bm.faces.layers.int.get("discretized")
            for face in bm.faces:
                index = face[discretization_index] - 1
                group = infra.tympan_discretization_groups[index]
                amplitudes.append(
                    np.array(group.spectrum.get_toct_amplitudes())
                )
        assert_almost_equal(tu.sum_db(amplitudes), global_amplitudes, 2)

    def test_edit_global_spectrum_applies_scale(self):
        bpy.ops.mesh.primitive_cube_add()
        infra = bpy.context.active_object
        element_types.to_infrastructure(infra, 'MACHINE')
        infra.scale = mathutils.Vector([1, 2, 3])
        set_infra_emissive(infra)
        global_amplitudes = [50] * 31
        iftr.safely_set_global_spectrum(
            infra,
            global_amplitudes,
        )
        expected_scale = mathutils.Vector([1, 1, 1])
        self.assertAlmostEqual((infra.scale - expected_scale).length, 0)

    def test_edit_global_spectrum_applies_scale_in_edit_mode(self):
        bpy.ops.mesh.primitive_cube_add()
        infra = bpy.context.active_object
        element_types.to_infrastructure(infra, 'MACHINE')
        infra.scale = mathutils.Vector([1, 2, 3])
        set_infra_emissive(infra)
        bpy.ops.object.mode_set(mode='EDIT')
        global_amplitudes = [50] * 31
        iftr.safely_set_global_spectrum(
            infra,
            global_amplitudes,
        )
        expected_scale = mathutils.Vector([1, 1, 1])
        self.assertAlmostEqual((infra.scale - expected_scale).length, 0)

    def test_set_groups_parallelepiped_spectrum_from_global(self):
        bpy.ops.mesh.primitive_cube_add()
        infra = bpy.context.active_object
        element_types.to_infrastructure(infra, 'MACHINE')
        dim = (71, 13, 42)
        infra.dimensions = dim
        bpy.ops.object.transform_apply(scale=True)
        set_infra_emissive(infra)
        global_amplitudes = [100.0] * 31
        set_infra_emissivity_group(infra, global_amplitudes)
        self.assertEqual(infra.scale.to_tuple(), (1.0, 1.0, 1.0))
        self.assertEqual(infra.dimensions.to_tuple(), dim)
        self.assertEqual(len(infra.tympan_discretization_groups), 2)
        groups = infra.tympan_discretization_groups
        assert_almost_equal(
            groups[0].spectrum.get_toct_amplitudes(),
            [91.9] * 31,
            0.1,
        )
        assert_almost_equal(
            groups[1].spectrum.get_toct_amplitudes(),
            [99.3] * 31,
            0.1,
        )
        print(iftr.get_global_emission_spectrum_of_machine(infra)[0])
        assert_almost_equal(
            iftr.get_global_emission_spectrum_of_machine(infra)[0],
            global_amplitudes,
            0.1,
        )

    def test_can_building_be_emissive(self):
        bpy.ops.blty.add_building()
        building = bpy.context.active_object
        self.assertFalse(iftr.can_building_be_emissive(building))

        machine = create_internal_machine(building)
        self.assertFalse(iftr.can_building_be_emissive(building))

        machine.tympan_use_emissivity = True
        self.assertFalse(iftr.can_building_be_emissive(building))

        set_infra_emissive(machine)
        self.assertTrue(iftr.can_building_be_emissive(building))

    def test_get_discretization_group_spectrum(self):
        bpy.ops.blty.add_building()
        building = bpy.context.active_object
        bpy.ops.mesh.primitive_cube_add()

        machine = create_internal_machine(building)
        set_infra_emissive(machine)
        global_amplitudes = [100.0] * 31
        set_infra_emissivity_group(machine, global_amplitudes)

        set_infra_emissive(building)
        group = building.tympan_discretization_groups[0]
        assert_almost_equal(
            iftr.calc_building_discretization_group_spectrum(building, group),
            global_amplitudes,
            0.001,
        )


class BLTYOriginToBottom(BlTYBaseTests):

    def test_does_not_modify_origin_of_mesh_with_no_vertices(self):
        bpy.ops.object.add(type="MESH")
        obj = bpy.context.active_object
        loc = obj.location.copy()
        tu.origin_to_bottom(obj)
        self.assertEqual(loc, obj.location)

    def test_origin_to_bottom(self):
        bpy.ops.mesh.primitive_cube_add()
        obj = bpy.context.active_object
        min_z = min([v.co.z for v in obj.data.vertices])
        tu.origin_to_bottom(obj)
        self.assertTrue(obj.location.z == min_z)


def get_receiver_spectrum(receiver):
    return tu.spectrum_amplitudes(
        receiver.tympan_soundreceiver_properties[0].spectrum
    )


class BLTYSolverParametersTest(BlTYBaseTests):

    def test_write_parameters_test(self):
        props_init = bpy.context.scene.solver_properties
        self.assertFalse(props_init.showScene)
        props_init.showScene = True
        solve.write_parameters_file(props_init)
        parser = configparser.RawConfigParser()
        parser.optionxform = str
        params_file = os.path.join(
            os.environ['TYMPAN_TMP_DIR'], 'solverparams.ini'
        )
        with open(params_file) as f:
            parser.read_file(f)
        self.assertEqual(
            props_init.NbThreads,
            int(parser.get('DEFAULTSOLVER', 'NbThreads'))
        )
        self.assertEqual("True", parser.get('GEOMTRANSFORMER', 'showScene'))


class BLTYSolveTests(BlTYBaseTests):

    sources = [
        ('s1', (1., 0., 0.), [30.] * 31, 0),
        ('s2', (1., 1., 0.), [1.] * 30 + [0.], 0),
    ]
    receivers = [
        ('r1', (0., 1., 0.), 0),
        ('r2', (0., 0., 0.), 1),
    ]

    def setUp(self):
        super().setUp()
        for name, coords, spectrum, elevation in self.sources:
            bpy.ops.blty.add_sound_source()
            s = bpy.context.active_object
            s.location[:] = coords
            s.name = name
            s.tympan_elevation = elevation
            tu.initialise_spectrum(s.tympan_soundsource_properties[0].spectrum,
                                   spectrum)
        for name, coords, elevation in self.receivers:
            bpy.ops.blty.add_sound_receiver()
            r = bpy.context.active_object
            r.name = name
            r.location[:] = coords
            r.tympan_elevation = elevation
        # add cube (mesh)
        x, y, z = 0, 0, 0
        bpy.ops.mesh.primitive_cube_add(location=(x, y, z))


class FakeResult:

    class FakeSpectrum:
        dBA = 0

        @property
        def values(self):
            return [0.] * 31

        def to_dB(self):
            return self

    def spectrum(self, src, rec):
        return self.FakeSpectrum()


class SolveTests(BLTYSolveTests):

    @mock.patch('blty.infrastructure.get_infra_meshes', return_value=([]))
    @mock.patch('blty.solve.build_model')
    @mock.patch('blty.core.compute_altimetry', return_value='AN_ALTI_MESH')
    @mock.patch('blty.solve.run_solver', return_value=FakeResult())
    @mock.patch('blty.solve.manage_result')
    @mock.patch('blty.core.get_combined_dB_spectra',
                return_value=np.vstack(([1.] * 31, [2.] * 31)))
    def test_solve_operator(self, get_combined_dB_spectra, manage_result,
                            run_solver, build_altimetry, build_model,
                            get_infra_meshes):
        bpy.ops.blty.solve()
        self.assertEqual(build_altimetry.call_count, 1)
        # NOTE: retrieval is done in reverse order of insertion (or something
        # like that)
        self.assertEqual(build_model.call_count, 1)
        args, kwargs = build_model.call_args_list[0]
        self.assertEqual(args, ('AN_ALTI_MESH', ([])))
        kwargs.pop('report')
        self.assertCountEqual(kwargs['sources'],
                              core.get_all_sources(bpy.context))
        self.assertCountEqual(kwargs['receivers'],
                              core.find_used_soundreceivers(bpy.context))
        self.assertEqual(run_solver.call_count, 1)
        args, _ = run_solver.call_args_list[0]
        self.assertEqual(args, ('default', build_model.return_value, ))
        self.assertEqual(manage_result.call_count, 1)
        args, _ = manage_result.call_args_list[0]
        fpath = os.environ['TYMPAN_RESULT_FILE']
        self.assertEqual(args, (bpy.context, run_solver.return_value, fpath))
        self.assertEqual(get_infra_meshes.call_count, 1)
        args, _ = get_infra_meshes.call_args_list[0]
        self.assertEqual(args, (bpy.context,))
        self.assertEqual(get_combined_dB_spectra.call_count, 1)
        objects = bpy.data.objects
        self.assertEqual(
            get_receiver_spectrum(objects['r1']),
            [1.] * 31,
        )
        self.assertEqual(
            get_receiver_spectrum(objects['r2']),
            [2.] * 31,
        )

    @unittest.skipUnless(HAS_TYMPAN, 'tympan Python library not available')
    def test_ground_set_impacts_results(self):
        bpy.context.scene.landtake_used_as_level_curve = True
        bpy.ops.blty.solve()
        objects = bpy.data.objects
        without_ground_set = get_receiver_spectrum(objects['r1']),

        bpy.ops.blty.add_ground_set()
        bpy.ops.object.mode_set(mode='OBJECT')
        ground_set = bpy.context.active_object
        ground_set.tympan_grounds[0].resistivity = 20000
        bpy.ops.blty.solve()

        objects = bpy.data.objects
        with_ground_set = get_receiver_spectrum(objects['r1'])
        with self.assertRaises(AssertionError):
            assert_almost_equal(
                without_ground_set,
                with_ground_set,
                decimal=2,
            )

    @unittest.skipUnless(HAS_TYMPAN, 'tympan Python library not available')
    def test_solve_operator_(self):
        bpy.ops.blty.solve()
        objects = bpy.data.objects
        expected_r1 = [
            19.18, 19.18, 19.18, 19.18, 19.18, 19.18, 19.18, 19.18, 19.18,
            19.18, 19.18, 19.18, 19.18, 19.18, 19.19, 19.19, 19.20, 19.21,
            19.22, 19.24, 19.26, 19.30, 19.35, 19.44, 19.56, 19.75, 20.03,
            20.42, 20.94, 21.55, 22.09]

        expected_r2 = [
            19.17, 19.17, 19.17, 19.17, 19.17, 19.17, 19.17, 19.17, 19.17,
            19.17, 19.17, 19.17, 19.18, 19.18, 19.18, 19.18, 19.19, 19.20,
            19.21, 19.23, 19.25, 19.29, 19.35, 19.43, 19.55, 19.74, 20.02,
            20.41, 20.94, 21.54, 22.09]
        assert_almost_equal(
            get_receiver_spectrum(objects['r1']),
            expected_r1,
            decimal=2,
        )
        assert_almost_equal(
            get_receiver_spectrum(objects['r2']),
            expected_r2,
            decimal=2,
        )

    @unittest.skipUnless(HAS_TYMPAN, 'tympan Python library not available')
    def test_solve_operator_with_machine_emissivity(self):
        bpy.ops.blty.add_building()
        obj = bpy.context.active_object
        element_types.to_infrastructure(obj, 'MACHINE')
        bpy.ops.blty.solve()
        objects = bpy.data.objects
        r1_spectrum = get_receiver_spectrum(objects['r1'])
        r2_spectrum = get_receiver_spectrum(objects['r2'])
        bpy.context.view_layer.objects.active = obj
        spectrum_values = [100] * 31
        set_infra_emissive(obj)
        tu.initialise_spectrum(
            obj.tympan_discretization_groups[0].spectrum,
            spectrum_values,
        )
        bpy.ops.blty.solve()
        r1_spectrum_with_emitting_infra = get_receiver_spectrum(objects['r1'])
        r2_spectrum_with_emitting_infra = get_receiver_spectrum(objects['r2'])
        # test than the cumulated spectrum with an emitting infrastructure
        # is greater than the one without
        self.assertTrue(np.all((np.array(r1_spectrum) <
                                np.array(r1_spectrum_with_emitting_infra))))
        self.assertTrue(np.all((np.array(r2_spectrum) <
                                np.array(r2_spectrum_with_emitting_infra))))
        obj.tympan_display_eqss = False
        bpy.ops.blty.solve()
        r1_spectrum_with_emitting_infra = get_receiver_spectrum(objects['r1'])
        r2_spectrum_with_emitting_infra = get_receiver_spectrum(objects['r2'])
        # test than the cumulated spectrum with an emitting infrastructure
        # is greater than the one without
        self.assertTrue(np.all((np.array(r1_spectrum) <
                                np.array(r1_spectrum_with_emitting_infra))))
        self.assertTrue(np.all((np.array(r2_spectrum) <
                                np.array(r2_spectrum_with_emitting_infra))))

    @unittest.skipUnless(HAS_TYMPAN, 'tympan Python library not available')
    def test_solve_operator_with_selection(self):
        """Tests that an added object has infuence only if it is active."""
        # Solve with setUp configuration
        bpy.ops.blty.solve()
        r1_object = bpy.data.objects['r1']
        r1_spectrum = get_receiver_spectrum(r1_object)
        # Solve with setUp configuration and a new object but not enabled
        bpy.ops.blty.add_building()
        added_object = bpy.context.active_object
        added_object.tympan_used = False
        bpy.ops.blty.solve()
        self.assertEqual(
            r1_spectrum,
            get_receiver_spectrum(r1_object),
        )
        # Solve with setup configuration and the new object enabled
        added_object.tympan_used = True
        bpy.ops.blty.solve()
        self.assertNotEqual(
            r1_spectrum,
            get_receiver_spectrum(r1_object),
        )

    @mock.patch('blty.solve.get_spectrum',
                return_value=FakeResult.FakeSpectrum())
    def test_manage_result(self, get_spectrum):
        for index, obj in enumerate(bpy.data.objects):
            solve.append_in_tympan_solver_index(obj, index)
        result_file = tempfile.NamedTemporaryFile(suffix='.html')
        result_file.close()
        fpath = result_file.name
        solve.manage_result(bpy.context, FakeResult(), fpath)
        self.assertEqual(get_spectrum.call_count, 4)
        self.assertTrue(os.stat(fpath).st_size)  # test file is not empty

    @unittest.skipUnless(HAS_TYMPAN, 'tympan Python library not available')
    def test_index_with_several_solver_call(self):
        s1_object = bpy.data.objects['s1']
        s2_object = bpy.data.objects['s2']
        s2_object.hide_viewport = True
        bpy.ops.blty.solve()
        self.assertEqual(s1_object.tympan_solver_index[0].index, 0)
        s2_object.hide_viewport = False
        bpy.ops.blty.solve()
        self.assertEqual(s2_object.tympan_solver_index[0].index, 1)

    @unittest.skipUnless(HAS_TYMPAN, 'tympan Python library not available')
    def test_create_tympan_result(self):
        for index, obj in enumerate(bpy.data.objects):
            solve.append_in_tympan_solver_index(obj, index)
        bpy.context.scene.landtake_used_as_level_curve = True
        bpy.ops.blty.solve()
        result = solve.create_tympan_result(bpy.context, FakeResult())
        self.assertAlmostEqual(result.get_source_spectrum('s1').dBA,
                               41.89, places=2)
        self.assertAlmostEqual(result.get_receiver_spectrum('r1').dBA,
                               31.54, places=2)
        self.assertAlmostEqual(result.get_couple_spectrum('s1', 'r1').dBA,
                               0, places=2)

    @unittest.skipUnless(HAS_TYMPAN, 'tympan Python library not available')
    @mock.patch(
        'blty.solve.get_emissive_infra_result_spectra',
        return_value=[FakeResult.FakeSpectrum(), FakeResult.FakeSpectrum()]
    )
    def test_create_tympan_result_with_emissive_machine(
            self,
            get_emissive_infra_result_spectra,
    ):
        bpy.context.scene.landtake_used_as_level_curve = True
        bpy.ops.blty.add_building()
        infra = bpy.context.active_object
        element_types.to_infrastructure(infra, 'MACHINE')
        infra.name = "Machine"
        infra.scale = (10, 20, 5)
        infra.location = (40, 60, 0)
        spectrum_values = [100] * 31
        set_infra_emissive(infra)
        tu.initialise_spectrum(
            infra.tympan_discretization_groups[0].spectrum,
            spectrum_values,
        )
        bpy.ops.blty.solve()
        result = solve.create_tympan_result(bpy.context, FakeResult())
        self.assertEqual(get_emissive_infra_result_spectra.call_count, 2)
        building_expected_dba = 125.87
        r1_expected_dba = 66
        self.assertAlmostEqual(result.get_source_spectrum('Machine').dBA,
                               building_expected_dba, places=2)
        self.assertAlmostEqual(result.get_receiver_spectrum('r1').dBA,
                               r1_expected_dba, places=2)
        self.assertAlmostEqual(
            result.get_couple_spectrum('Machine', 'r1').dBA,
            0, places=2,
        )
        rtable_factory = results.ResultTableFactory(result)
        self.assertCountEqual(
            rtable_factory.global_table['col1_src'],
            ['synthesis', 's1', 's2', 'Machine'],
        )
        self.assertCountEqual(
            rtable_factory.global_table['r1'],
            [r1_expected_dba, 0, 0, 0],
        )
        self.assertCountEqual(
            rtable_factory.global_table['col2_lw'],
            [building_expected_dba, 12.88, 41.89, ''],
        )
        self.assertCountEqual(
            rtable_factory.for_frequency(5),
            {
                'col1_src': ['synthesis', 'Machine', 's1', 's2'],
                'col2_lw': ['', 10.0, 3.0, 1.0],
                'r1': [0.94, 0.0, 0.0, 0.0],
                'r2': [-0.85, 0.0, 0.0, 0.0],
                'colors0': ['white', 'red', 'white', 'white'],
                'colors1': ['white', 'red', 'white', 'white'],
            }
        )

    @unittest.skipUnless(HAS_TYMPAN, 'tympan Python library not available')
    @mock.patch(
        'blty.solve.get_emissive_infra_result_spectra',
        return_value=[FakeResult.FakeSpectrum(), FakeResult.FakeSpectrum()]
    )
    def test_solve_with_uniform_emissive_machine(
            self,
            get_emissive_infra_result_spectra,
    ):
        bpy.context.scene.landtake_used_as_level_curve = True
        bpy.ops.blty.add_building()
        infra = bpy.context.active_object
        element_types.to_infrastructure(infra, 'MACHINE')
        infra.name = "Machine"
        infra.scale = (10, 20, 5)
        infra.location = (40, 60, 0)
        spectrum_values = [100] * 31
        set_infra_emissive(infra)
        discretization_group = infra.tympan_discretization_groups[0]
        discretization_group.algorithm = "uniform"
        discretization_group.uniform_step = 0.5
        tu.initialise_spectrum(
            discretization_group.spectrum,
            spectrum_values,
        )
        bpy.ops.blty.solve()
        result = solve.create_tympan_result(bpy.context, FakeResult())
        self.assertEqual(get_emissive_infra_result_spectra.call_count, 2)
        r1_expected_dba = 75.66
        self.assertAlmostEqual(result.get_receiver_spectrum('r1').dBA,
                               r1_expected_dba, places=2)

        discretization_group.directivity_type = 'baffled'
        bpy.ops.blty.solve()
        for index, obj in enumerate(bpy.data.objects):
            solve.append_in_tympan_solver_index(obj, index)
        result = solve.create_tympan_result(bpy.context, FakeResult())
        with np.testing.assert_raises(AssertionError):
            self.assertAlmostEqual(result.get_receiver_spectrum('r1').dBA,
                                   r1_expected_dba, places=2)
        r1_expected_dba = 76.26
        self.assertAlmostEqual(result.get_receiver_spectrum('r1').dBA,
                               r1_expected_dba, places=2)


class TympanResultTableFactoryTests(BLTYSolveTests):

    @unittest.skipUnless(HAS_TYMPAN, 'tympan Python library not available')
    def test_global_result(self):
        bpy.context.scene.landtake_used_as_level_curve = True
        bpy.ops.blty.solve()
        result = solve.create_tympan_result(bpy.context, FakeResult())
        rtable_factory = results.ResultTableFactory(result)
        self.assertCountEqual(
            rtable_factory.global_table['col1_src'],
            ['synthesis', 's1', 's2'],
        )
        self.assertCountEqual(
            rtable_factory.global_table['r1'],
            [31.54, 0, 0],
        )


class TympanResultExporterHTMLTests(BLTYSolveTests):

    @unittest.skipUnless(HAS_TYMPAN, 'tympan Python library not available')
    def test_create_main_table(self):
        bpy.context.scene.landtake_used_as_level_curve = True
        bpy.ops.blty.solve()
        result = solve.create_tympan_result(bpy.context, FakeResult())
        result_exporter = io_html_result.TympanResultExporter(result)
        table = result_exporter._create_main_html_table().children[1]
        self.assertCountEqual(
            list(table.source.data.keys()),
            ['col1_src', 'col2_lw', 'r1', 'r2', 'colors0', 'colors1'],
        )


class SpectrumTests(BlTYBaseTests):

    def test_initialisation(self):
        bpy.ops.blty.add_sound_source()
        s = bpy.context.active_object
        spectrum = s.tympan_soundsource_properties[0].spectrum
        self.assertEqual(tu.spectrum_amplitudes(spectrum),
                         [tu.DEFAULT_SPECTRUM_BAND_AMPLITUDE] * 31)


class TympanResultsExporterHTMLTests(BLTYSolveTests):

    @unittest.skipUnless(HAS_TYMPAN, 'tympan Python library not available')
    def test_create_main_html_table(self):
        for index, obj in enumerate(bpy.data.objects):
            solve.append_in_tympan_solver_index(obj, index)
        bpy.context.scene.landtake_used_as_level_curve = True
        bpy.ops.blty.solve()
        result = solve.create_tympan_result(bpy.context, FakeResult())
        r_exporter = io_html_result.TympanResultExporter(result)
        select, table = r_exporter._create_main_html_table().children
        self.assertEqual(
            len(select.options),
            len(tu.LOWER_CUTOFF_FREQUENCIES) + 1  # all freqs + global
        )
        self.assertEqual(len(table.columns), 4)
        titles = [c.title for c in table.columns]
        self.assertCountEqual(titles, ['', 'LW', 'r1', 'r2'])


class TympanReferenceImageTests(BlTYBaseTests):

    def test_can_scale(self):
        bpy.ops.object.add(type='EMPTY')
        image = bpy.context.active_object
        image.empty_display_type = 'IMAGE'
        image.tympan_type = 'REFERENCE_IMAGE'
        image.tympan_subtype = 'TOPOGRAPHIC_MAP'
        assert image.scale == mathutils.Vector([1, 1, 1])

        bpy.ops.blty.set_image_scale(measured_distance=0.5, true_distance=21)

        assert image.scale == mathutils.Vector([42, 42, 1])
