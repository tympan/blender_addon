from os import path
from pathlib import Path
import tempfile
import csv
from unittest import TestCase

import pyperclip

import bpy

from blty.io_spectrum.io_csv import (
    CSVSpectrumExporter,
)
from blty import (
    tympan_utils as tu,
)
from blty.io_spectrum.io_html import create_spectrum_histogram

from blty.testutils import BlTYBaseTests


HERE = path.abspath(path.dirname(__file__))


def datapath(*parts):
    return path.join(HERE, 'data', *parts)


class CSVSpectrumExporterTC(TestCase):

    def test_header_provided_as_constructor_is_serialized(self):
        exporter = CSVSpectrumExporter('FREQ', 'AMPL')
        with tempfile.TemporaryDirectory() as tmpdir:
            output_path = Path(tmpdir, 'output.csv')
            with output_path.open('w') as output:
                exporter.export_spectrum(output, [1, 2, 3], [4, 5, 6])
            with output_path.open('r') as output:
                self.assertEqual('"FREQ","AMPL"', output.readline().strip())

    def test_rows_correctly_serialized(self):
        exporter = CSVSpectrumExporter('FREQ', 'AMPL')
        with tempfile.TemporaryDirectory() as tmpdir:
            output_path = Path(tmpdir, 'output.csv')
            frequencies = (1, 2, 3)
            amplitudes = (4, 5, 6)
            with output_path.open('w', newline='') as output:
                exporter.export_spectrum(output, frequencies, amplitudes)
            with output_path.open('r') as output:
                reader = csv.reader(output, quoting=csv.QUOTE_NONNUMERIC)
                next(reader)  # skip header
                actual_frequencies, actual_amplitudes = zip(*reader)
                self.assertEqual(actual_frequencies, frequencies)
                self.assertEqual(actual_amplitudes, amplitudes)


class TympanExportCSVSpectrumTC(BlTYBaseTests):

    def test_correctly_translate_spectrum_property(self):
        bpy.ops.blty.add_sound_source()
        with tempfile.TemporaryDirectory() as tmpdir:
            output_path = Path(tmpdir, 'output.csv')
            bpy.ops.blty.export_spectrum_as_csv(filepath=str(output_path))
            with output_path.open('r') as output:
                reader = csv.reader(output, quoting=csv.QUOTE_NONNUMERIC)
                next(reader)  # skip header
                _, actual_amplitudes = zip(*reader)
                self.assertCountEqual(
                    actual_amplitudes,
                    [tu.DEFAULT_SPECTRUM_BAND_AMPLITUDE]*31,
                )


class TympanImportCSVSpectrumTC(BlTYBaseTests):

    def test_correctly_import_spectrum_property(self):
        bpy.ops.blty.add_sound_source()
        source = bpy.context.active_object
        spectrum = source.tympan_soundsource_properties[0].spectrum

        bpy.ops.blty.import_spectrum_from_csv(
            filepath=datapath('source_spectrum.csv')
        )
        amplitudes_in_csv = [1, 11]*15 + [42]
        self.assertCountEqual(
            tu.spectrum_amplitudes(spectrum),
            amplitudes_in_csv
        )


class TympanDisplaySpectrumTC(BlTYBaseTests):

    def test_create_spectrum_histogram(self):
        bpy.ops.blty.add_sound_source()
        source = bpy.context.active_object
        spectrum = source.tympan_soundsource_properties[0].spectrum
        with tempfile.TemporaryDirectory() as tmpdir:
            output_path = Path(tmpdir, 'output.html')
            create_spectrum_histogram('name', spectrum, output_path)
            self.assertTrue(output_path.exists())


class TympanCopySpectrumTC(BlTYBaseTests):

    def test_copy_spectrum_fills_the_clipboard_with_correct_csv(self):
        bpy.ops.blty.add_sound_source()
        bpy.ops.blty.import_spectrum_from_csv(
            filepath=datapath('source_spectrum.csv'))
        bpy.ops.blty.copy_spectrum_as_csv()
        with open(datapath('source_spectrum.csv')) as f:
            self.assertEqual(pyperclip.paste(), f.read())


class TympanPasteSpectrumTC(BlTYBaseTests):

    def _paste_from_file(self, filepath):
        bpy.ops.blty.add_sound_source()

        with open(filepath) as f:
            pyperclip.copy(f.read())
        bpy.ops.blty.paste_spectrum()

    def _check_paste_from_file_succeed(self, filepath):
        self._paste_from_file(filepath)

        source = bpy.context.active_object
        spectrum = source.tympan_soundsource_properties[0].spectrum
        amplitudes_in_csv = [1, 11]*15 + [42]
        self.assertCountEqual(
            tu.spectrum_amplitudes(spectrum),
            amplitudes_in_csv
        )

    def test_paste_2_cols_with_header(self):
        self._check_paste_from_file_succeed(datapath('source_spectrum.csv'))

    def test_paste_2_col_no_header(self):
        self._check_paste_from_file_succeed(
            datapath('source_spectrum_no_header.csv'))

    def test_paste_2_col_tab_separator(self):
        self._check_paste_from_file_succeed(
                datapath('source_spectrum_tab_separator.csv'))

    def test_paste_1_col(self):
        self._check_paste_from_file_succeed(
                datapath('source_spectrum_1_column.csv'))

    def test_pasting_invalid_number_of_amplitudes_reports_an_error(self):
        with self.assertRaises(RuntimeError) as ctx:
            self._paste_from_file(
                datapath('source_spectrum_invalid_number_of_amplitudes.csv'))
        self.assertIn('argument must have 31 elements', str(ctx.exception))

    def test_pasting_non_numeric_amplitudes_reports_an_error(self):
        with self.assertRaises(RuntimeError) as ctx:
            self._paste_from_file(
                datapath('source_spectrum_non_numeric_amplitudes.csv'))
        self.assertIn('invalid amplitudes', str(ctx.exception))

    def test_pasting_with_empty_clipboard_reports_an_error(self):
        with self.assertRaises(RuntimeError) as ctx:
            self._paste_from_file(datapath('source_spectrum_empty.csv'))
        self.assertIn('Nothing to paste', str(ctx.exception))
