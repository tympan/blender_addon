import bpy
from os import path
from unittest.mock import patch

from lxml import etree
from mathutils import (
    Vector,
    Matrix,
)

from blty.io_xml_project import XMLImporter
from blty.testutils import (
    BlTYBaseTests,
    to_fake_spectrum,
)

HERE = path.abspath(path.dirname(__file__))


def datapath(*parts):
    return path.join(HERE, 'data', *parts)


class XMLParserTests(BlTYBaseTests):

    expected_sources = [
        ('Source ponctuelle 0', Vector([0., 2., 1.2]), [10.] * 31),
        ('Source ponctuelle 1', Vector([1., 2, 3.1]), [1.] * 31),
    ]

    expected_sources_local_cs = [
        ('Source ponctuelle 0', Vector([2.5, 3., 1.2]), [10.] * 31),
        ('Source ponctuelle 1', Vector([1., 8.0, -1]), [1.] * 31),
    ]

    expected_receivers = [
        ('Récepteur A1', Vector([-23.0, -27.0, 2.0]), [-200.] * 31),
        ('Récepteur B1', Vector([131.0, -100.0, 5.0]), [-200.] * 31),
    ]

    expected_machines = [
        (
            'Machine 0',
            'cubic',
            Matrix(((1.0, 0.0, 0.0, -1.0),
                    (0.0, 1.0, 0.0, -1.0),
                    (0.0, 0.0, 1.0, 9.0),
                    (0.0, 0.0, 0.0, 1.0))),
            {
                'Face 0': {
                    'vertices': [
                        Vector((9.5, -8.0, 9.0)), Vector((9.5, 8.0, 9.0)),
                        Vector((9.5, 8.0, -9.0)), Vector((9.5, -8.0, -9.0)),
                    ],
                    'emissivity_group': {
                        'default': True, 'props': None, 'algorithm': 'uniform'
                    },
                },
                'Face 1': {
                    'vertices': [
                        Vector((-9.5, -8.0, 9.0)), Vector((9.5, -8.0, 9.0)),
                        Vector((9.5, -8.0, -9.0)), Vector((-9.5, -8.0, -9.0)),
                    ],
                    'emissivity_group': {
                        'default': True, 'props': None, 'algorithm': 'uniform'
                    },
                },
                'Face 2': {
                    'vertices': [
                        Vector((-9.5, 8.0, 9.0)), Vector((-9.5, -8.0, 9.0)),
                        Vector((-9.5, -8.0, -9.0)), Vector((-9.5, 8.0, -9.0)),
                    ],
                    'emissivity_group': {
                        'default': True, 'props': None, 'algorithm': 'uniform'
                    },
                },
                'Face 3': {
                    'vertices': [
                        Vector((9.5, 8.0, 9.0)), Vector((-9.5, 8.0, 9.0)),
                        Vector((-9.5, 8.0, -9.0)), Vector((9.5, 8.0, -9.0)),
                    ],
                    'emissivity_group': {
                        'default': True, 'props': None, 'algorithm': 'uniform'
                    },
                },
                'Face 4': {
                    'vertices': [
                        Vector((-9.5, -8.0, -9.0)), Vector((9.5, -8.0, -9.0)),
                        Vector((9.5, 8.0, -9.0)), Vector((-9.5, 8.0, -9.0)),
                    ],
                    'emissivity_group': {
                        'default': True, 'props': None, 'algorithm': 'uniform'
                    },
                },
                'Face 5': {
                    'vertices': [
                        Vector((-9.5, 8.0, 9.0)), Vector((9.5, 8.0, 9.0)),
                        Vector((9.5, -8.0, 9.0)), Vector((-9.5, -8.0, 9.0)),
                    ],
                    'emissivity_group': {
                        'default': True, 'props': None, 'algorithm': 'uniform'
                    },
                },
            },
            0,
            {},
        ),
        (
            'Machine 4',
            'cylindric',
            Matrix(((1.0, 0.0, 0.0, 1.0),
                    (0.0, 1.0, 0.0, 1.0),
                    (0.0, 0.0, 1.0, 1.0),
                    (0.0, 0.0, 0.0, 1.0))),
            {
                'Cercle acoustique 0': {
                    'vertices': [
                        Vector((-9.0, 9.0, 10.0)), Vector((9.0, 9.0, 10.0)),
                        Vector((9.0, -9.0, 10.0)), Vector((-9.0, -9.0, 10.0)),
                    ],
                    'emissivity_group': {
                        'default': False, 'props': None, 'algorithm': 'circular'
                    },
                },
                'Cercle acoustique 1': {
                    'vertices': [
                        Vector((-9.0, -9.0, -10.0)), Vector((9.0, -9.0, -10.0)),
                        Vector((9.0, 9.0, -10.0)), Vector((-9.0, 9.0, -10.0)),
                    ],
                    'emissivity_group': {
                        'default': False, 'props': None, 'algorithm': 'circular'
                    },
                },
                'Surface acoustique 54': {
                    'vertices': [],
                    'emissivity_group': {
                        'default': True, 'props': None, 'algorithm': 'uniform'
                    },
                },
            },
            0,
            {},
        ),
    ]

    expected_buildings = [
        (
            'Construction 0',
            'cylindric',
            Matrix(((1.0, 0.0, 0.0, 5.0),
                    (0.0, 1.0, 0.0, 1.0),
                    (0.0, 0.0, 1.0, 5.0),
                    (0.0, 0.0, 0.0, 1.0))),
            {
                'Cercle acoustique 8': {
                    'vertices': [
                        Vector((-19.5, 19.5, 7.5)), Vector((19.5, 19.5, 7.5)),
                        Vector((19.5, -19.5, 7.5)), Vector((-19.5, -19.5, 7.5))
                    ],
                    'emissivity_group': {
                        'default': False, 'props': None, 'algorithm': 'circular'
                    },
                },
                'Cercle acoustique 9': {
                    'vertices': [
                        Vector((-19.5, -19.5, -7.5)), Vector((19.5, -19.5, -7.5)),
                        Vector((19.5, 19.5, -7.5)), Vector((-19.5, 19.5, -7.5))
                    ],
                    'emissivity_group': {
                        'default': False, 'props': None, 'algorithm': 'circular'
                    },
                },
                'Surface acoustique 66': {
                    'vertices': [],
                    'emissivity_group': {
                        'default': True, 'props': None, 'algorithm': 'uniform'
                    },
                },
            },
            5,
            {},
        ),
        (
            'Construction 3/Etage 0',
            'polygonal',
            Matrix(((1.0, 0.0, 0.0, 5.0),
                    (0.0, 1.0, 0.0, 1.0),
                    (0.0, 0.0, 1.0, 5.0),
                    (0.0, 0.0, 0.0, 1.0))),
            {
                'floor': {
                    'vertices': [
                        Vector((59.83300018310547, 52.5, 0.0)),
                        Vector((59.83300018310547, -41.5, 0.0)),
                        Vector((20.83300018310547, -30.5, 0.0)),
                        Vector((-67.16699981689453, -30.5, 0.0)),
                        Vector((-67.16699981689453, 35.5, 0.0)),
                        Vector((-6.165999889373779, 14.5, 0.0)),
                    ],
                },
                'ceiling': {
                    'vertices': [
                        Vector((-6.165999889373779, 14.5, 12.0)),
                        Vector((-67.16699981689453, 35.5, 12.0)),
                        Vector((-67.16699981689453, -30.5, 12.0)),
                        Vector((20.83300018310547, -30.5, 12.0)),
                        Vector((59.83300018310547, -41.5, 12.0)),
                        Vector((59.83300018310547, 52.5, 12.0)),
                    ],
                },
            },
            5,
            {},
        ),
    ]

    expected_level_curves = [
        (
            'Courbe de niveau 0',
            Matrix(((1.0, 0.0, 0.0, 0.0),
                    (0.0, 1.0, 0.0, 0.0),
                    (0.0, 0.0, 1.0, 0.0),
                    (0.0, 0.0, 0.0, 1.0))),
            [
                Vector((-124, 117, 120)), Vector((-135, 108, 120)),
                Vector((-126, 90, 120)), Vector((-106, 91, 120)),
                Vector((-106, 112, 120)), Vector((-124, 117, 120)),
            ],
            [
                (0, 1), (1, 2), (2, 3), (3, 4), (4, 5)
            ],
            120.0,
        ),
    ]

    def test_read_sources(self):
        with patch("blty.io_xml_project._to_solver_spectrum", wraps=to_fake_spectrum):
            importer = XMLImporter(bpy.context, datapath('project.xml'))
            sources = [(s.name, s.vertices, s.spectrum) for s in importer._read_sources()]
            self.assertEqual(sources, self.expected_sources)

    def test_read_empty(self):
        importer = XMLImporter(bpy.context, 'fakepath')
        importer.root = etree.fromstring(
            '<Tympan><Projet><Infrastructure>'
            '</Infrastructure></Projet></Tympan>'
        )
        self.assertEqual(list(importer._read_sources()), [])
        self.assertEqual(list(importer._read_receivers()), [])
        self.assertEqual(list(importer._read_infrastructures('Machine')), [])
        self.assertEqual(list(importer._read_infrastructures('Building')), [])

    def test_read_receivers(self):
        with patch("blty.io_xml_project._to_solver_spectrum", wraps=to_fake_spectrum):
            importer = XMLImporter(bpy.context, datapath('project.xml'))
            receivers = [
                (r.name, r.vertices, r.spectrum) for r in importer._read_receivers()
            ]
            self.assertEqual(receivers, self.expected_receivers)

    def test_read_sources_with_local_coordinate_systems(self):
        with patch("blty.io_xml_project._to_solver_spectrum", wraps=to_fake_spectrum):
            importer = XMLImporter(bpy.context, datapath('project_local_cs.xml'))
            sources = [(s.name, s.vertices, s.spectrum) for s in importer._read_sources()]
            self.assertEqual(sources, self.expected_sources_local_cs)

    def test_read_infrastructures(self):
        importer = XMLImporter(bpy.context, datapath('project.xml'))
        machines = list(importer._read_infrastructures('Machine'))
        self.assertEqual(len(machines), 2)
        self.assertEqual(machines, self.expected_machines)
        buildings = list(importer._read_infrastructures('Batiment'))
        self.assertEqual(len(buildings), 2)
        self.assertEqual(buildings, self.expected_buildings)
        screens = list(importer._read_infrastructures('Ecran'))
        self.assertEqual(len(screens), 0)

    def test_read_level_curves(self):
        importer = XMLImporter(bpy.context, datapath('project.xml'))
        lc = list(importer._read_level_curves())
        self.assertEqual(len(lc), 1)
        self.assertEqual(lc, self.expected_level_curves)
