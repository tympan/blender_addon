from contextlib import contextmanager

import bmesh
from mathutils import Vector

from blty.discretizers import (
    CenterDiscretizer,
    CircularDiscretizer,
    PokeDiscretizer,
    SubdivideDiscretizer,
    UniformDiscretizer,
)
from blty.testutils import BlTYBaseTests


@contextmanager
def plane_2x2_bmesh():
    plane = bmesh.new()
    bmesh.ops.create_grid(plane, size=1)
    yield plane, next(iter(plane.faces))
    plane.free()


@contextmanager
def circle_bmesh(radius):
    circle = bmesh.new()
    bmesh.ops.create_circle(circle, cap_ends=True, cap_tris=False,
                            segments=3, radius=radius)
    yield circle, next(iter(circle.faces))
    circle.free()


class CenterDiscretizerTests(BlTYBaseTests):

    def test_yield_1_point(self):
        with plane_2x2_bmesh() as (_, face):
            discretizer = CenterDiscretizer()
            points = list(discretizer.discretize_face(face))
            self.assertEqual(len(points), 1)
            self.assertEqual(points[0], Vector([0, 0, 0]))


class CircularDiscretizerTests(BlTYBaseTests):
    def test_yield_1_point_at_center(self):
        with circle_bmesh(1) as (bm, face):
            discretizer = CircularDiscretizer(2)
            points = list(discretizer.discretize_face(face))
            center_count = sum(p.length < 1e-6 for p in points)
            self.assertEqual(center_count, 1)

    def test_yield_7_points_with_1_division(self):
        with circle_bmesh(1) as (bm, face):
            discretizer = CircularDiscretizer(1)
            points = list(discretizer.discretize_face(face))
            self.assertEqual(len(points), 7)

    def test_do_not_yield_points_farther_than_radius(self):
        radius = 5
        with circle_bmesh(radius) as (bm, face):
            discretizer = CircularDiscretizer(3)
            points = list(discretizer.discretize_face(face))
            self.assertTrue(all(p.length < (radius + 10e-6) for p in points))

    def test_yield_points_on_radius(self):
        radius = 5
        with circle_bmesh(radius) as (bm, face):
            discretizer = CircularDiscretizer(3)
            points = list(discretizer.discretize_face(face))
            self.assertTrue(
                any(abs(p.length - radius) < 10e-6 for p in points)
            )


class PokeDiscretizerTests(BlTYBaseTests):

    def test_yield_point_at_center(self):
        with plane_2x2_bmesh() as (bmesh, face):
            discretizer = PokeDiscretizer(bmesh)
            points = list(discretizer.discretize_face(face))
            points_at_center = [p for p in points if p.length < 10e-10]
            self.assertEqual(len(points_at_center), 1)

    def test_yield_number_of_edges_plus_1_points(self):
        with plane_2x2_bmesh() as (bmesh, face):
            discretizer = PokeDiscretizer(bmesh)
            points = list(discretizer.discretize_face(face))
            self.assertEqual(len(points), 5)


class UniformDiscretizerTests(BlTYBaseTests):

    def test_yield_point_at_center(self):
        with plane_2x2_bmesh() as (_, face):
            discretizer = UniformDiscretizer(0.2)
            points = list(discretizer.discretize_face(face))
            points_at_center = [p for p in points if p.length < 10e-10]
            self.assertEqual(len(points_at_center), 1)

    def test_step_one_third_of_face_dimension(self):
        with plane_2x2_bmesh() as (_, face):
            discretizer = UniformDiscretizer(2./3. - 0.1)
            points = list(discretizer.discretize_face(face))
            self.assertEqual(len(points), 9)

    def test_step_greater_than_one_third_of_face_dimension(self):
        with plane_2x2_bmesh() as (_, face):
            discretizer = UniformDiscretizer(2./3. + 0.1)
            points = list(discretizer.discretize_face(face))
            self.assertEqual(len(points), 1)


class SubdivideDiscretizerTests(BlTYBaseTests):

    def test_yield_9_points_on_plane_with_2_cuts(self):
        with plane_2x2_bmesh() as (bm, face):
            discretizer = SubdivideDiscretizer(bm, 2)
            points = list(discretizer.discretize_face(face))
            self.assertEqual(len(points), 9)

    def test_yield_16_points_on_plane_with_3_cuts(self):
        with plane_2x2_bmesh() as (bm, face):
            discretizer = SubdivideDiscretizer(bm, 3)
            points = list(discretizer.discretize_face(face))
            self.assertEqual(len(points), 16)
