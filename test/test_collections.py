import pytest

import bpy

from blty.collectionwrapper import (
    TreeBuilder,
    CollectionWrapper,
    TreeUpdater,
    create_tympan_collection_definition,
)
from blty.element_types import to_infrastructure
from blty.testutils import (
    select_3dview,
    _delete_all_objects,
)


@pytest.fixture(autouse=True, scope='function')
def clean_blend():
    _delete_all_objects(),
    yield
    _delete_all_objects(),
    select_3dview()


def all_collections(collection):
    yield collection
    for collection in collection.children:
        yield from all_collections(collection)


def scene_collections(scene):
    yield from all_collections(scene.collection)


@pytest.fixture
def an_object():
    """Provides a simple object"""
    bpy.ops.object.add()
    return bpy.context.active_object


def accepts_name_length(obj):
    return len(obj.name) > 4


class TestCollectionWrapper:

    def test_doesnt_accept_an_object_by_default(self, an_object):
        wrapper = CollectionWrapper('My Collection', [])

        assert not wrapper.accepts_object(an_object)

    def test_accepts_when_accept_fn_accepts(self, an_object):
        wrapper = CollectionWrapper('My Collection', [], lambda _: True)

        assert wrapper.accepts_object(an_object)

    def test_doesnt_accept_when_accept_fn_doesnt_accept(self, an_object):
        wrapper = CollectionWrapper('My Collection', [], lambda _: False)

        assert not wrapper.accepts_object(an_object)


class TestTreeBuilder:

    def test_build_once_create_a_collection_for_each_wrapper(self):
        scene = bpy.context.scene
        col2 = CollectionWrapper('Col2', [])
        col3 = CollectionWrapper('Col3', [])
        col1 = CollectionWrapper('Col1', [col2, col3])
        tree_builder = TreeBuilder(col1)

        tree_builder.build_tree(scene.collection)
        collections_names = [col.name for col in scene_collections(scene)]

        assert 'Col1' in collections_names
        assert 'Col2' in collections_names
        assert 'Col3' in collections_names

    def test_build_twice_doesnt_duplicate_collections(self):
        scene = bpy.context.scene
        col2 = CollectionWrapper('Col2', [])
        col3 = CollectionWrapper('Col3', [])
        col1 = CollectionWrapper('Col1', [col2, col3])
        tree_builder = TreeBuilder(col1)

        tree_builder.build_tree(scene.collection)
        collections = list(scene_collections(scene))
        tree_builder.build_tree(scene.collection)

        assert list(scene_collections(scene)) == collections


@pytest.fixture
def tree_updater():
    return create_tympan_collection_definition()


class TestTreeUpdater:

    def test_acceptable_object_get_added_to_collection(self, an_object):
        scene = bpy.context.scene
        wrapper = CollectionWrapper('Col1', [], accepts_name_length)
        tree_builder = TreeBuilder(wrapper)
        updater = TreeUpdater(wrapper, tree_builder)
        assert an_object.users_collection == (scene.collection,)

        updater.update()

        assert wrapper.get_collection() in an_object.users_collection

    def test_remove_other_collections_at_root(self, tree_updater):
        scene = bpy.context.scene
        col = bpy.data.collections.new(name='Not from Tympan1')
        scene.collection.children.link(col)
        assert col in scene_collections(scene)

        tree_updater.update()

        actual_collections = scene_collections(scene)
        assert col not in actual_collections

    def test_remove_other_nested_collections(self, tree_updater):
        scene = bpy.context.scene
        tree_updater.update()
        col1 = bpy.data.collections.new(name='Not from Tympan1')
        col2 = bpy.data.collections.new(name='Not from Tympan2')
        col1.children.link(col2)
        target_col = scene.collection.children['Tympan'].children['Sources']
        target_col.children.link(col1)
        assert set([col1, col2]).issubset(scene_collections(scene))

        tree_updater.update()

        actual_collections = scene_collections(scene)
        assert col1 not in actual_collections
        assert col2 not in actual_collections


class TestCreateTympanCollectionDefinition:

    testdata = [
        (bpy.ops.blty.add_sound_source, 'Sources'),
        (bpy.ops.blty.add_sound_receiver, 'Receivers'),
        (bpy.ops.blty.add_level_curve, 'Level Curves'),
        (bpy.ops.blty.add_ground_set, 'Ground Sets'),
    ]

    @pytest.mark.parametrize("op,col_name", testdata)
    def test_created_object_collection(self, tree_updater, op, col_name):
        op()
        obj = bpy.context.active_object

        tree_updater.update()

        expected = (bpy.data.collections[col_name],)
        assert expected == obj.users_collection

    def test_created_machines_collection(self, tree_updater):
        bpy.ops.object.add(type='MESH')
        machine = bpy.context.active_object
        to_infrastructure(machine, 'MACHINE')

        tree_updater.update()

        expected = (bpy.data.collections['Machines'],)
        assert expected == machine.users_collection

    def test_screens_collection(self, tree_updater):
        bpy.ops.object.add(type='MESH')
        screen = bpy.context.active_object
        to_infrastructure(screen, 'SCREEN')

        tree_updater.update()

        expected = (bpy.data.collections['Screens'],)
        assert expected == screen.users_collection

    def test_created_building_collection(self, tree_updater):
        bpy.ops.object.add(type='MESH')
        building = bpy.context.active_object
        to_infrastructure(building, 'BUILDING')

        tree_updater.update()

        expected = (bpy.data.collections[building.name],)
        assert expected == building.users_collection

    def test_created_building_collection_internal_sources(self, tree_updater):
        bpy.ops.object.add(type='MESH')
        building = bpy.context.active_object
        to_infrastructure(building, 'BUILDING')
        bpy.ops.object.add(type='MESH')
        internal_machine = bpy.context.active_object
        to_infrastructure(internal_machine, 'MACHINE')
        internal_machine.parent_building = building
        bpy.ops.blty.add_sound_source()
        internal_point_source = bpy.context.active_object
        internal_point_source.parent_building = building

        tree_updater.update()

        expected = (bpy.data.collections[building.name],)
        assert expected == building.users_collection
        assert expected == internal_machine.users_collection
        assert expected == internal_point_source.users_collection
