import math
import numpy as np
from mathutils import (
    Matrix,
    Vector,
)
import bmesh

from . import geom2d


def _build_inverse_face_matrix(face):
    center = face.calc_center_median_weighted()
    edge = face.edges[0]
    norm = face.normal
    ex = (edge.verts[1].co - edge.verts[0].co).normalized()
    ey = ex.cross(norm).normalized()
    matrix = Matrix(
            (ex, ey, norm, center),
            )
    matrix = Matrix()
    matrix[0][0:3] = ex
    matrix[1][0:3] = ey
    matrix[2][0:3] = norm
    matrix[3][0:3] = center
    matrix.transpose()
    return matrix


class FaceDiscretizer:

    def discretize_face(self, face):
        raise NotImplementedError()


class UniformDiscretizer(FaceDiscretizer):

    def __init__(self, step):
        self._step = step

    def _delta_for_step(self, max_x, min_x, max_y, min_y):
        '''Compute the delta with the bounds to ensure that the number of
        points is always odd. In this way we can center the distribution around
        the center of the face. It is also here that we make sure that the
        number of points is not too big.
        '''
        step = self._step
        nb_point_x = math.floor((max_x - min_x) / step)
        if not (nb_point_x % 2):
            nb_point_x = nb_point_x - 1
        nb_point_y = math.floor((max_y - min_y) / step)
        if not (nb_point_y % 2):
            nb_point_y = nb_point_y - 1
        delta_x = ((max_x - min_x) - (nb_point_x-1) * step) / 2
        delta_y = ((max_y - min_y) - (nb_point_y-1) * step) / 2
        return delta_x, delta_y

    def discretize_face(self, face):
        step = self._step
        matrix_face_inv = _build_inverse_face_matrix(face)
        matrix_face = matrix_face_inv.inverted()
        verts = [matrix_face @ v.co for v in face.verts]
        min_x, max_x, min_y, max_y = geom2d.polygon_bounding_box(verts)
        delta_x, delta_y = self._delta_for_step(
            max_x, min_x,
            max_y, min_y,
        )
        x_range = np.arange(min_x + delta_x, max_x - delta_x + step, step)
        y_range = np.arange(min_y + delta_y, max_y - delta_y + step, step)
        if len(x_range) == 0 and len(y_range) == 0:
            yield face.calc_center_median_weighted()
            return
        for i in x_range:
            for j in y_range:
                point = Vector((i, j, 0))
                if geom2d.point_inside_polygon(point, verts):
                    yield matrix_face_inv @ point


class PokeDiscretizer(FaceDiscretizer):

    def __init__(self, bm):
        self._bm = bm

    def discretize_face(self, face):
        yield face.calc_center_median_weighted()
        new_geom = bmesh.ops.poke(self._bm, faces=[face])
        for f in new_geom['faces']:
            yield f.calc_center_median_weighted()


class CenterDiscretizer(FaceDiscretizer):

    def discretize_face(self, face):
        yield face.calc_center_median_weighted()


class SubdivideDiscretizer(FaceDiscretizer):

    def __init__(self, bm, cuts):
        self._bm = bm
        self._cuts = cuts

    def _subdivide_face(self, face):
        ret = bmesh.ops.subdivide_edges(
            self._bm,
            edges=face.edges,
            use_grid_fill=True,
            cuts=self._cuts,
        )
        for elt in ret['geom']:
            if isinstance(elt, bmesh.types.BMFace):
                yield elt

    def discretize_face(self, face):
        bmesh.ops.split_edges(self._bm, edges=face.edges)
        for new_face in self._subdivide_face(face):
            yield new_face.calc_center_median_weighted()


PI2 = math.pi * 2


class CircularDiscretizer(FaceDiscretizer):
    def __init__(self, divisions):
        self.divisions = divisions

    def _face_radius(self, center, face):
        return max((vert.co - center).magnitude for vert in face.verts)

    def _discretize_circle(self, radius, point_count):
        for point_idx in range(point_count):
            angle = point_idx * PI2 / point_count
            x = math.cos(angle) * radius
            y = math.sin(angle) * radius
            yield Vector((x, y, 0))

    def discretize_face(self, face):
        center = face.calc_center_median_weighted()
        yield center
        matrix_face_inv = _build_inverse_face_matrix(face)
        outer_radius = self._face_radius(center, face)
        step_size = outer_radius / self.divisions
        for circle_idx in range(1, self.divisions + 1):
            circle_radius = circle_idx * step_size
            perimeter = PI2 * circle_radius
            pt_count = math.floor(perimeter / step_size)
            yield from (
                matrix_face_inv @ point
                for point in self._discretize_circle(circle_radius, pt_count)
            )
