import bmesh
import bpy
from bpy.types import Operator
from bpy.utils import (
    register_class,
    unregister_class,
)
from collections import namedtuple
import math
from mathutils import (
    Vector,
)
import numpy as np

from . import (
    core,
    element_types as et,
    tympan_utils as tu,
)

from blty.discretizers import (
    CenterDiscretizer,
    PokeDiscretizer,
    UniformDiscretizer,
    SubdivideDiscretizer,
    CircularDiscretizer,
)


class TympanAddBuilding(Operator):
    bl_label = "Add a building"
    bl_idname = tu.blty_idname('add_building')

    base_mesh: bpy.props.EnumProperty(items=(
        ('cube', 'Cube',
         'Add a cubic building', 'MESH_CUBE', 0),
        ('cylinder', 'Cylinder',
         'Add a cylindrical building', 'MESH_CYLINDER', 1),
        ('cone', 'Cone',
         'Add a conical building', 'MESH_CONE', 2),
        ('uv_sphere', 'Sphere',
         'Add a spherical building', 'MESH_UVSPHERE', 4),
    ))

    def execute(self, context):
        addon_prefs = bpy.context.preferences.addons['blty'].preferences
        size = addon_prefs.building_size
        if self.base_mesh == "cube":
            bpy.ops.mesh.primitive_cube_add(size=size)
        elif self.base_mesh == "cylinder":
            bpy.ops.mesh.primitive_cylinder_add(radius=size/2, depth=size)
        elif self.base_mesh == "cone":
            bpy.ops.mesh.primitive_cone_add(radius1=size/2, depth=size)
        else:
            assert self.base_mesh == "uv_sphere"
            bpy.ops.mesh.primitive_uv_sphere_add(radius=size/2)
        obj = context.active_object
        et.to_infrastructure(obj, "BUILDING")
        obj.location.z = 0
        obj.lock_location = (False, False, True)
        return {'FINISHED'}


def init_building_absorption_parameters_spectra(group_props, spectra={}):
    tu.initialise_spectrum(
        group_props.raw_isolation.spectrum, spectra.get('raw_isolation')
    )
    tu.initialise_spectrum(
        group_props.sabine_inner_absorption.spectrum,
        spectra.get('sabine_inner_absorption'),
    )
    tu.initialise_spectrum(
        group_props.sabine_outer_absorption.spectrum,
        spectra.get('sabine_outer_absorption'),
    )


class TYMPANVIEW3D_PT_add_discretization_group(Operator):
    bl_label = 'Add tympan discretization group'
    bl_idname = tu.blty_idname('add_tympan_discretization_group')

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and et.can_be_emissive(obj)

    def execute(self, context):
        infra = context.active_object
        name_prefix = 'Discretization Group'
        new_group = create_discretization_group(infra, name_prefix)
        tu.initialise_spectrum(new_group.spectrum)
        if et.is_building(infra):
            init_building_absorption_parameters_spectra(new_group)
        return {'FINISHED'}


class TYMPANVIEW3D_PT_remove_discretization_group(Operator):
    bl_label = 'Remove tympan discretization group'
    bl_idname = tu.blty_idname('remove_tympan_discretization_group')

    @classmethod
    def poll(cls, context):
        obj = context.object
        groups = obj.tympan_discretization_groups
        return obj and et.can_be_emissive(obj) and len(groups) > 1

    def execute(self, context):
        infra = context.active_object
        groups = infra.tympan_discretization_groups
        active_index = infra.active_discretization_group_index
        remove_faces_from_discretization_group(
            infra,
            infra.tympan_discretization_groups[active_index].id,
            all_faces=True,
        )
        active_index = infra.active_discretization_group_index
        groups.remove(active_index)
        infra.active_discretization_group_index = 0
        tu.redraw_3d_view()
        return {'FINISHED'}


class TYMPANVIEW3D_PT_assign_faces_to_discretization_group(Operator):
    bl_label = 'Assign discretization to group'
    bl_idname = tu.blty_idname('assign_faces_to_discretization_group')

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and et.use_emissivity(obj)

    def execute(self, context):
        infra = context.active_object
        active_index = infra.active_discretization_group_index
        assign_faces_to_discretization_group(
            infra,
            infra.tympan_discretization_groups[active_index].id,
        )
        tu.redraw_3d_view()
        return {'FINISHED'}


class TYMPANVIEW3D_PT_unassign_faces_to_discretization_group(Operator):
    bl_label = 'Unassign discretization to group'
    bl_idname = tu.blty_idname('remove_faces_from_discretization_group')

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and et.use_emissivity(obj)

    def execute(self, context):
        infra = context.active_object
        active_index = infra.active_discretization_group_index
        remove_faces_from_discretization_group(
            infra,
            infra.tympan_discretization_groups[active_index].id,
        )
        tu.redraw_3d_view()
        return {'FINISHED'}


class TYMPANVIEW3D_PT_select_faces_for_discretization_group(Operator):
    bl_label = 'Select faces of discretization to group'
    bl_idname = tu.blty_idname('select_faces_in_discretization_group')

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and et.use_emissivity(obj)

    def execute(self, context):
        infra = context.active_object
        active_index = infra.active_discretization_group_index
        set_select_for_active_group_faces(
            infra,
            infra.tympan_discretization_groups[active_index].id,
            True,
        )
        return {'FINISHED'}


class TYMPANVIEW3D_PT_deselect_faces_for_discretization_group(Operator):
    bl_label = 'Deselect faces of discretization to group'
    bl_idname = tu.blty_idname('deselect_faces_in_discretization_group')

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and et.use_emissivity(obj)

    def execute(self, context):
        infra = context.active_object
        active_index = infra.active_discretization_group_index
        set_select_for_active_group_faces(
            infra,
            infra.tympan_discretization_groups[active_index].id,
            False,
        )
        return {'FINISHED'}


class TYMPANVIEW3D_PT_select_child_of_building(Operator):
    bl_label = 'Select child of the building'
    bl_idname = tu.blty_idname('select_child_of_building')

    child_name: bpy.props.StringProperty()

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and et.is_building(obj)

    def execute(self, context):
        bpy.ops.object.select_all(action='DESELECT')
        context.object.select_set(False)
        child = bpy.data.objects[self.child_name]
        child.select_set(True)
        bpy.context.view_layer.objects.active = child
        return {'FINISHED'}


def find_all_infrastructures(context):
    for obj in context.scene.objects:
        if et.is_infrastructure(obj):
            yield obj


def find_used_infrastructures(context):
    for infra in find_all_infrastructures(context):
        if infra.tympan_used:
            yield infra


def find_unused_infrastructures(context):
    for infra in find_all_infrastructures(context):
        if not infra.tympan_used:
            yield infra


def find_emitting_infrastructures(context):
    """return all active emitting buildings and external machines"""
    for obj in find_used_infrastructures(context):
        if has_emitting_faces(obj):
            if et.is_building(obj) or et.is_external_machine(obj):
                yield obj


def get_infra_meshes(context):
    for infra in find_used_infrastructures(context):
        yield export_mesh(infra), infra.active_material


def faces_with_material(obj, material, bm):
    for face in bm.faces:
        mat = obj.material_slots[face.material_index].material
        if mat and mat.name == material.name:
            yield face


def discretizer_from_discretization_group(bm, group):
    algo = group.algorithm
    if algo == 'center':
        return CenterDiscretizer()
    elif algo == 'subdivide':
        cuts = group.subdivision_number
        return SubdivideDiscretizer(bm, cuts)
    elif algo == 'poke':
        return PokeDiscretizer(bm)
    elif algo == 'uniform':
        step = group.uniform_step
        return UniformDiscretizer(step)
    elif algo == 'circular':
        divisions = group.circular_divisions
        return CircularDiscretizer(divisions)
    else:
        raise AssertionError(f'Invalid discretization algortithm {algo}')


EquivalentSoundSource = namedtuple(
    'EquivalentSoundSource', ['coords', 'normal', 'area']
)


def compute_eq_sound_sources(obj, discretization_group):
    """Calculate equivalent sound sources properties"""
    eqss_props = []
    with tu.bmesh_from_mesh(obj.data) as bm:
        discretizer = discretizer_from_discretization_group(
            bm,
            discretization_group,
        )
        discretization_index = bm.faces.layers.int.get("discretized")
        for face in list(bm.faces):
            if face[discretization_index] == discretization_group.id:
                points_coords = list(discretizer.discretize_face(face))
                area = face.calc_area() / len(points_coords)
                for position in points_coords:
                    eqss_props.append(
                        EquivalentSoundSource(
                            coords=obj.matrix_world @ position,
                            normal=face.normal,
                            area=area,
                        )
                    )
    return eqss_props


def get_eq_sound_sources_for_infra(infra):
    sources = {}
    for group in infra.tympan_discretization_groups:
        sources[group] = compute_eq_sound_sources(infra, group)
    return sources


def calc_building_discretization_group_spectrum(building, group):
    group_area = 0
    with tu.bmesh_from_mesh_for_update(building.data) as bm:
        discretization_index = bm.faces.layers.int.get("discretized")
        for f in bm.faces:
            if f[discretization_index] == group.id:
                group_area += f.calc_area()
    t_area = get_infra_emissive_area(building)
    global_amplitudes, _ = get_global_emission_spectrum(building)
    return np.array(global_amplitudes) + 10 * np.log10(group_area/t_area)


def get_infras_eq_sound_sources(context):
    """Retrieve equivalent sound sources for emitting infrastructure"""
    for infra in find_emitting_infrastructures(context):
        infra_sources = get_eq_sound_sources_for_infra(infra)
        for discretization_group, sources in infra_sources.items():
            if et.is_building(infra):
                sp_amplitudes = calc_building_discretization_group_spectrum(
                    infra,
                    discretization_group,
                )
            else:
                sp_amplitudes = np.array(
                    discretization_group.spectrum.get_toct_amplitudes()
                )
            for eq_ss in sources:
                eq_ss_coords = eq_ss.coords
                amplitudes = sp_amplitudes - 10 * np.log10(len(eq_ss_coords))
                directivity = None
                if discretization_group.directivity_type != "spherical":
                    directivity = {
                        'type': discretization_group.directivity_type,
                        'normal': eq_ss.normal,
                        'area': eq_ss.area,
                    }
                yield infra, eq_ss_coords, amplitudes, directivity


def get_discretization_group_by_id(infra, identifier):
    for group in infra.tympan_discretization_groups:
        if group.id == identifier:
            return group
    return None


def get_infra_discretization_group_by_faces(infra):
    groups = {}
    with tu.bmesh_from_mesh(infra.data) as bm:
        discretization_index = bm.faces.layers.int.get("discretized")
        for face in bm.faces:
            if face[discretization_index] > 0:
                group_id = face[discretization_index]
                groups[face] = get_discretization_group_by_id(infra, group_id)
    return groups


def get_global_emission_spectrum(infra):
    '''Sum (in dB) of the power of the emissive faces of an emissive infra'''
    if et.is_machine(infra):
        amplitudes, freqs = get_global_emission_spectrum_of_machine(infra)
    elif et.is_building(infra):
        amplitudes, freqs = get_global_emission_spectrum_of_building(infra)
    return amplitudes, freqs


def get_global_emission_spectrum_of_machine(machine):
    '''Sum (in dB) of the power of the emissive faces of an emissive machine'''
    assert has_emitting_faces(machine), f"Machine {machine} is not emissive"
    global_amplitudes = []
    faces_amplitudes = []
    machine_group = get_infra_discretization_group_by_faces(machine)
    for face, group in machine_group.items():
        faces_amplitudes.append(
                    np.array(group.spectrum.get_toct_amplitudes())
                )
    if faces_amplitudes:
        global_amplitudes = list(tu.sum_db(faces_amplitudes))
    return global_amplitudes, tu.LOWER_CUTOFF_FREQUENCIES


def get_global_emission_spectrum_of_building(building):
    '''Sum (in dB) of the power of the internal sources an emissive building'''
    assert has_emitting_faces(building), f"Building {building} is not emissive"

    global_amplitudes = []
    spectra_amplitudes = []
    for src in et.get_internal_sources(building):
        if et.is_internal_point_source(src):
            spectrum = src.tympan_soundsource_properties[0].spectrum
            spectra_amplitudes.append(spectrum.get_toct_amplitudes())
        elif et.is_internal_machine(src) and has_emitting_faces(src):
            machine_spectrum = get_global_emission_spectrum_of_machine(src)[0]
            if machine_spectrum:
                spectra_amplitudes.append(machine_spectrum)
    if spectra_amplitudes:
        global_amplitudes = list(tu.sum_db(spectra_amplitudes))
    return global_amplitudes, tu.LOWER_CUTOFF_FREQUENCIES


def get_infra_emissive_area(infra):
    area = 0
    with tu.bmesh_from_mesh(infra.data) as bm:
        discretization_index = bm.faces.layers.int.get("discretized")
        for face in bm.faces:
            if face[discretization_index] != 0:
                area += face.calc_area()
    return area


def set_spectrum_amplitudes(spectrum, amplitudes, face_area, t_area):
    """The spectrum amplitudes (in dB) is equal to:
    amplitudes + 10*log10(face_area/t_area)
    with
    amplitudes: infra global spectrum amplitudes in dB
    face_area: the area of the emissive face
    t_area: the emissive area of the infra"""
    sp_amplitudes = np.array(amplitudes) + 10 * np.log10(face_area/t_area)
    tu.initialise_spectrum(spectrum, sp_amplitudes)


def create_discretization_group(infra, name_prefix):
    infra_groups = infra.tympan_discretization_groups
    new_id = infra_groups[-1].id + 1
    new_group = infra_groups.add()
    new_group.name = '{}-{}'.format(name_prefix, new_id)
    new_group.id = new_id
    return new_group


def duplicate_discretization_group(infra, group):
    new_group = create_discretization_group(
        infra,
        '{}.copy'.format(group.name),
    )
    new_group.algorithm = group.algorithm
    new_group.subdivision_number = group.subdivision_number
    new_group.uniform_step = group.uniform_step
    return new_group


def safely_set_global_spectrum(infra, amplitudes):
    with tu.object_mode():
        if infra.scale != Vector([1, 1, 1]):
            infra.select_set(True)
            bpy.ops.object.transform_apply(
                location=False,
                rotation=False,
                scale=True,
                properties=False,
            )
        set_groups_spectrum_from_global(infra, amplitudes)


def set_groups_spectrum_from_global(infra, amplitudes):
    """Divide global emission spectrum in face spectrum.
    Create new group if needed"""
    groups = {}
    visited_groups = set()
    infra_emissive_area = get_infra_emissive_area(infra)
    with tu.bmesh_from_mesh_for_update(infra.data) as bm:
        discretization_index = bm.faces.layers.int.get("discretized")
        for face in bm.faces:
            if face[discretization_index] > 0:
                discret_id = face[discretization_index]
                cgroup = get_discretization_group_by_id(infra, discret_id)
                face_area = round(face.calc_area(), 3)
                key = (cgroup.id, face_area)
                if key in groups:
                    _, faces, discret_id = groups[key]
                    faces.append(face)
                    face[discretization_index] = discret_id
                elif cgroup.id in visited_groups:
                    new_group = duplicate_discretization_group(infra, cgroup)
                    face[discretization_index] = new_group.id
                    new_key = (cgroup.id, face_area)
                    groups[new_key] = (new_group, [face], new_group.id)
                    spectrum = new_group.spectrum
                    set_spectrum_amplitudes(
                        spectrum, amplitudes, face_area, infra_emissive_area
                    )
                    assign_faces_to_discretization_group(
                        infra,
                        new_group.id,
                        [face],
                    )
                else:
                    visited_groups.add(cgroup.id)
                    groups[key] = (cgroup, [face], cgroup.id)
                    spectrum = cgroup.spectrum
                    set_spectrum_amplitudes(
                        spectrum, amplitudes, face_area, infra_emissive_area
                    )


def export_mesh(obj):
    # Get a BMesh representation
    with tu.bmesh_from_mesh(obj.data) as bm:
        bmesh.ops.triangulate(bm, faces=bm.faces[:],
                              quad_method='BEAUTY', ngon_method='BEAUTY')
        vertices = core.apply_object_transforms(
            [vert.co for vert in bm.verts],
            obj,
        )
        faces = []
        for face in bm.faces:
            faces.append([vert.index for vert in face.verts])
    return vertices, faces


def get_acoustic_material(material):
    if material is None:
        return (
            b'default',
            np.zeros(31),
            "0"
        )
    acoustic_properties = material.acoustic_properties
    spectrum = np.array(
        [band.amplitude for band in acoustic_properties.spectrum.bands]
    )
    return (
        acoustic_properties.name.encode('utf-8'),
        spectrum,
        acoustic_properties.volumeID,
    )


def init_discretization_layer(infra):
    with tu.bmesh_from_mesh_for_update(infra.data) as bm:
        discretization_index = bm.faces.layers.int.new("discretized")
        for face in bm.faces:
            face[discretization_index] = 0


def assign_faces_to_discretization_group(infra, group_id, faces=None):
    with tu.bmesh_from_mesh_for_update(infra.data) as bm:
        discretization_index = bm.faces.layers.int.get("discretized")
        for face in bm.faces:
            if (faces and face in faces) or face.select:
                face[discretization_index] = group_id


def remove_faces_from_discretization_group(infra, group_id, all_faces=False):
    '''Assign non discretization group (index=0) to all faces assigned
    to current group'''
    with tu.bmesh_from_mesh_for_update(infra.data) as bm:
        discretization_index = bm.faces.layers.int.get("discretized")
        for face in bm.faces:
            if ((all_faces or face.select)
                    and face[discretization_index] == group_id):
                face[discretization_index] = 0


def create_first_discretization_group(infra):
    group_props = infra.tympan_discretization_groups.add()
    group_props.id = 1  # 0 is keep for non discretization group
    group_props.name = "Discretization group"
    tu.initialise_spectrum(group_props.spectrum)
    init_discretization_layer(infra)
    init_building_absorption_parameters_spectra(group_props)
    return group_props


def toggle_infra_equivalent_sound_sources(self, context):
    """Add spectrum and equivalent sound sources"""
    if et.use_emissivity(self) and not self.tympan_discretization_groups:
        create_first_discretization_group(self)


def set_select_for_active_group_faces(infra, group_id, is_selected):
    '''Select or deselect faces of the current discretization group'''
    with tu.bmesh_from_mesh_for_update(infra.data) as bm:
        discretization_index = bm.faces.layers.int.get("discretized")
        for f in bm.faces:
            if f[discretization_index] == group_id:
                f.select_set(is_selected)


def is_active_group_empty(infra):
    active_index = infra.active_discretization_group_index
    if len(infra.tympan_discretization_groups) <= active_index:
        return True
    active_group = infra.tympan_discretization_groups[active_index]
    return is_group_empty(infra, active_group)


def is_group_empty(infra, group):
    with tu.bmesh_from_mesh(infra.data) as bm:
        discretization_index = bm.faces.layers.int.get("discretized")
        for f in bm.faces:
            if f[discretization_index] == group.id:
                return False
    return True


def has_emitting_faces(infra):
    if et.use_emissivity(infra):
        for group in infra.tympan_discretization_groups:
            if not is_group_empty(infra, group):
                return True
    return False


def can_building_be_emissive(obj):
    if obj and et.is_building(obj):
        for child in et.get_internal_sources(obj):
            if et.is_internal_point_source(child) or has_emitting_faces(child):
                return True
    return False


def get_selected_emissive_faces_area(infra):
    area = 0
    if et.use_emissivity(infra):
        with tu.bmesh_from_mesh_for_update(infra.data) as bm:
            discretization_index = bm.faces.layers.int.get("discretized")
            for f in bm.faces:
                if f[discretization_index] != 0 and f.select:
                    area += f.calc_area()
    return area


def get_translation_vector(obj, newz):
    """We compensate the rotation before applying the translation"""
    rotation_matrix = obj.rotation_euler.to_matrix()
    rotation_matrix.invert()
    return Vector((0, 0, newz)) @ rotation_matrix


def apply_infrastructures_elevation(context, alti_mesh):
    '''Reposition infra to the correct height'''
    for infra in find_used_infrastructures(context):
        infra_coord = [(infra.matrix_world @ v.co)
                       for v in infra.data.vertices]
        infra_min_z = min([co.z for co in infra_coord])
        infra_base_vertice = [co for co in infra_coord
                              if co.z == infra_min_z][0]
        alti_vertice = [v for v in alti_mesh.vertices
                        if v.co.xy == infra_base_vertice.xy][0]
        infra_alti = infra.tympan_elevation
        newz = alti_vertice.co.z - infra_base_vertice.z + infra_alti
        obj = bpy.data.objects[infra.name]
        obj.location = obj.location + get_translation_vector(obj, newz)


def remove_double_vertices(mesh):
    '''Remove useless vertices from mesh'''
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.remove_doubles()
    bpy.ops.object.mode_set(mode='OBJECT')


def get_cylinder_parameters(coords):
    '''Get the cylinder parameters contained between the two
    rectangular surfaces. For that, we take 2 opposite points of a
    surface and 1 point of the other surface.
    radius = distance (x,y) beetween 2 points of the same surface
    height = distance (z) beetwen 2 points of different surface
    center (x,y) = middle of the segment between 2 points of the same surface
    center (z) = middle of the segment betwwen 2 points of different surface
    '''
    p1, p2, p3 = coords[0], coords[1], coords[-1]
    height = abs(p1[2] - p3[2])
    x1, y1, x2, y2 = p1[:2] + coords[2][:2]
    radius = math.sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)/2
    elevation = (p1[2] - p3[2])/2 + min([p1[2], p3[2]])
    center_base = [(x1 + x2)/2, (y1 + y2)/2, elevation]
    return radius, height, center_base


def build_polygonal_infra_mesh(walls, obj_mesh):
    '''We build the infrastructure wall by wall'''
    bm = bmesh.new()
    used_vertices = []
    wn = bm.faces.layers.string.new("wall_name")
    for wall_name, wall_properties in walls.items():
        face_vertices = []
        for coords in wall_properties['vertices']:
            new = True
            coords = [round(v, 3) for v in coords]
            for known_vert in used_vertices:
                known_coords = [round(v, 3) for v in known_vert.co]
                if coords == known_coords:
                    face_vertices.append(known_vert)
                    new = False
                    break
            if new:
                vert = bm.verts.new(coords)
                face_vertices.append(vert)
                used_vertices.append(vert)
        face = bm.faces.new(face_vertices)
        face[wn] = str.encode(wall_name)
    bm.to_mesh(obj_mesh)
    bm.clear()


def _get_cylinder_wall_names(walls):
    last_z = None
    for name, wall in walls.items():
        if wall['vertices']:
            z = wall['vertices'][0].z
            if not last_z:
                top_wall_name = name
                bottom_wall_name = name
            elif z < last_z:
                bottom_wall_name = name
            else:
                top_wall_name = name
        else:
            side_wall_name = name
    return top_wall_name, bottom_wall_name, side_wall_name


def build_cylindric_infra_mesh(walls, radius, height, obj_mesh):
    '''We build the infrastructure wall by wall'''
    top_wall_name, bottom_wall_name, side_wall_name = _get_cylinder_wall_names(walls)
    bm = bmesh.new()
    wn = bm.faces.layers.string.new("wall_name")
    top_vertices = [
        bm.verts.new(Vector((math.cos(x)*radius, math.sin(x)*radius, height)))
        for x in np.linspace(0, 2 * math.pi, 32)
    ]
    top_face = bm.faces.new(top_vertices)
    top_face[wn] = str.encode(top_wall_name)
    bottom_vertices = [
        bm.verts.new(Vector((math.cos(x)*radius, math.sin(x)*radius, 0)))
        for x in np.linspace(0, 2 * math.pi, 32)
    ]
    bottom_face = bm.faces.new(bottom_vertices)
    bottom_face[wn] = str.encode(bottom_wall_name)
    for v1, v2, v3, v4 in zip(
            top_vertices[:-1], top_vertices[1:],
            bottom_vertices[1:], bottom_vertices[:-1]):
        face = bm.faces.new([v1, v2, v3, v4])
        face[wn] = str.encode(side_wall_name)
    bm.to_mesh(obj_mesh)
    bm.clear()


def build_screen_infra_mesh(coords, height, thickness, obj_mesh):
    '''We build the screen mesh from its squelette that is extruded.'''
    bm = bmesh.new()

    for v in coords:
        bm.verts.new(v)
        bm.verts.new((v.x, v.y, height))
        bm.edges.new(bm.verts[-2:])

    for e1, e2 in zip(bm.edges, bm.edges[1:]):
        verts = []
        verts.extend(e1.verts)
        e2verts = list(e2.verts)
        e2verts.reverse()
        verts.extend(e2verts)
        bm.faces.new(verts)

    bm.to_mesh(obj_mesh)

    m = bpy.context.active_object.modifiers.new("Solidify", type='SOLIDIFY')
    m.thickness = thickness
    m.use_even_offset = True
    m.offset = 0

    bm.clear()


def make_cubic_infra(context, infra, infra_type):
    '''To build the infra from the corrdinates of its faces.
    At the end we remove the duplicates points'''
    bpy.ops.object.add(type="MESH")
    o = context.active_object
    o.name = infra.name
    o.tympan_elevation = infra.elevation
    build_polygonal_infra_mesh(infra.walls, o.data)
    o.location = infra.matrix @ o.location
    remove_double_vertices(o.data)
    et.to_infrastructure(o, infra_type)
    _assign_walls_to_emissivity_groups(o, infra.walls)


def _assign_walls_to_emissivity_groups(obj, walls):
    with tu.bmesh_from_mesh_for_update(obj.data) as bm:
        wn = bm.faces.layers.string.get("wall_name")
        discretization_index = bm.faces.layers.int.new("discretized")
        index_group = 2
        for face in list(bm.faces):
            face[discretization_index] = 0
            wall_name = face[wn].decode('utf8')
            wall_emissivity_group = walls[wall_name]['emissivity_group']
            wall_emissivity_props = wall_emissivity_group['props']
            if wall_emissivity_props and wall_emissivity_props['is_emissive']:
                obj.tympan_use_emissivity = True
                if wall_emissivity_group['default']:
                    group = obj.tympan_discretization_groups[0]
                else:
                    group = obj.tympan_discretization_groups.add()
                    group.id = index_group
                    group.name = f"group{index_group}"
                    index_group += 1
                group.uniform_step = wall_emissivity_props['density']
                group.algorithm = wall_emissivity_group['algorithm']
                spectrum_values = wall_emissivity_props['spectrum']
                tu.initialise_spectrum(group.spectrum, spectrum_values)
                face[discretization_index] = group.id


def make_cylindric_infra(context, infra, infra_type):
    bases_coords = []
    for wall in infra.walls.values():
        if 'vertices' in wall:
            bases_coords.extend(wall['vertices'])
    radius, height, location = get_cylinder_parameters(bases_coords)
    bpy.ops.object.add(type="MESH")
    o = bpy.context.active_object
    o.name = infra.name
    et.to_infrastructure(o, infra_type)
    build_cylindric_infra_mesh(infra.walls, radius, height, o.data)
    o.tympan_elevation = infra.elevation
    o.location = infra.matrix @ o.location
    _assign_walls_to_emissivity_groups(o, infra.walls)


def _assign_walls_to_discretization_group(obj, walls_group_spectra):
    with tu.bmesh_from_mesh_for_update(obj.data) as bm:
        wn = bm.faces.layers.string.get("wall_name")
        discretization_index = bm.faces.layers.int.new("discretized")
        existing_groups = []
        group_index = 1
        for face in list(bm.faces):
            face[discretization_index] = 0
            wall_name = face[wn].decode('utf8')
            face_group = walls_group_spectra.get(wall_name)
            if not face_group:
                continue
            group_name = face_group['group_name']
            if group_name in existing_groups:
                group = obj.tympan_discretization_groups[group_name]
                face[discretization_index] = group.id
            else:
                group = obj.tympan_discretization_groups.add()
                group.id = group_index
                group_index += 1
                group.name = group_name
                tu.initialise_spectrum(group.spectrum)
                init_building_absorption_parameters_spectra(
                    group,
                    face_group['spectra']
                )
                face[discretization_index] = group.id
                existing_groups.append(group_name)


def make_polygonal_infra(context, infra, infra_type):
    bpy.ops.object.add(type="MESH")
    o = bpy.context.active_object
    o.name = infra.name
    build_polygonal_infra_mesh(infra.walls, o.data)
    o.tympan_elevation = infra.elevation
    o.location = infra.matrix @ o.location
    et.to_infrastructure(o, infra_type)
    if infra.walls_group_spectra:
        _assign_walls_to_discretization_group(o, infra.walls_group_spectra)


def make_screen_infra(context, infra):
    bpy.ops.object.add(type="MESH")
    o = bpy.context.active_object
    o.name = infra.name
    base_coords = infra.vertices
    height = float(infra.height)
    thickness = float(infra.thickness)
    build_screen_infra_mesh(base_coords, height, thickness, o.data)
    o.tympan_elevation = infra.elevation
    o.location = infra.matrix @ o.location
    et.to_infrastructure(o, "SCREEN")


def get_eq_sound_sources_for_display():
    points = []
    for infra in find_used_infrastructures(bpy.context):
        if has_emitting_faces(infra) and infra.tympan_display_eqss:
            infra_sources = get_eq_sound_sources_for_infra(infra)
            for sources in infra_sources.values():
                points.extend([s.coords for s in sources])
    return points


classes = (
    TympanAddBuilding,
    TYMPANVIEW3D_PT_add_discretization_group,
    TYMPANVIEW3D_PT_remove_discretization_group,
    TYMPANVIEW3D_PT_assign_faces_to_discretization_group,
    TYMPANVIEW3D_PT_unassign_faces_to_discretization_group,
    TYMPANVIEW3D_PT_select_faces_for_discretization_group,
    TYMPANVIEW3D_PT_deselect_faces_for_discretization_group,
    TYMPANVIEW3D_PT_select_child_of_building,
)


def register():
    bpy.types.Object.tympan_use_emissivity = \
        bpy.props.BoolProperty(name='Emissive', default=False, options=set(),
                               update=toggle_infra_equivalent_sound_sources)
    bpy.types.Object.tympan_display_eqss = \
        bpy.props.BoolProperty(name='Show/Hide equivalent sound sources',
                               default=True, options=set())
    bpy.types.Object.active_discretization_group_index = \
        bpy.props.IntProperty(name='Active discretization group', default=0,
                              min=0, options=set())
    for cls in classes:
        register_class(cls)


def unregister():
    for cls in classes:
        unregister_class(cls)
    del bpy.types.Object.tympan_use_emissivity
    del bpy.types.Object.active_discretization_group_index
    del bpy.types.Object.tympan_display_eqss
