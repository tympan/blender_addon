import math


def polygon_bounding_box(verts):
    max_x = max_y = -math.inf
    min_x = min_y = math.inf
    for vert in verts:
        max_x = max(max_x, vert.x)
        max_y = max(max_y, vert.y)
        min_x = min(min_x, vert.x)
        min_y = min(min_y, vert.y)
    return min_x, max_x, min_y, max_y


def point_inside_polygon(point, vertices, include_edges=False):
    '''
    Test if point is inside the polygon.
    Geometrical idea: point is inside polygon if horisontal beam
    to the right from point crosses polygon even number of times.
    Works fine for non-convex polygons.
    '''
    n_vertices = len(vertices)
    inside = False
    for n in range(n_vertices):
        p1, p2 = vertices[n], vertices[(n + 1) % n_vertices]
        if p1.y == p2.y:
            if point.y == p1.y:
                if min(p1.x, p2.x) <= point.x <= max(p1.x, p2.x):
                    inside = include_edges
                    break
                elif point.x < min(p1.x, p2.x):
                    inside = not inside
        else:
            if min(p1.y, p2.y) <= point.y <= max(p1.y, p2.y):
                xintersection = ((point.y - p1.y) * (p2.x - p1.x)
                                 / float(p2.y - p1.y) + p1.x)
                if point.x == xintersection:
                    inside = include_edges
                    break
                if point.x < xintersection:
                    inside = not inside
    return inside


def is_point_in_aabb(aabb, point):
    min_x, max_x, min_y, max_y = aabb
    return min_x < point.x < max_x and min_y < point.y < max_y
