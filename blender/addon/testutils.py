import unittest

from numpy.testing import (
    assert_almost_equal,
)
import bpy
from blty.io_spectrum.utils import find_active_spectrum
import bpy.ops

from blty import (
    tympan_utils,
)


def _delete_all_objects():
    for obj in bpy.data.objects:
        bpy.data.objects.remove(obj)
    for mesh in bpy.data.meshes:
        bpy.data.meshes.remove(mesh)
    for collection in bpy.data.collections:
        bpy.data.collections.remove(collection)


def check_spectrum(actual_obj, expct_spectrum):
    '''We need to change the active object to get its spectrum.
    So we put the context in its previous state after recovery
    the spectrum to avoid this function to change the context'''
    curent_active_object = bpy.context.view_layer.objects.active
    bpy.context.view_layer.objects.active = actual_obj
    spectrum = find_active_spectrum(bpy.context)
    bpy.context.view_layer.objects.active = curent_active_object
    assert_almost_equal(
        tympan_utils.spectrum_amplitudes(spectrum),
        expct_spectrum,
        3,
    )


def check_emissivity_discretization_group(actual_group, expect_obj):
    group_name = actual_group.name
    expected_group = expect_obj['discretization_groups'][group_name]
    actual_spectrum = tympan_utils.spectrum_amplitudes(
        actual_group.spectrum
    )
    assert_almost_equal(expected_group['spectrum'], actual_spectrum)


def check_disctretization_group(actual_group, expect_obj):
    group_name = actual_group.name
    expected_group = expect_obj['group_spectra'][group_name]
    actual_raw_isolation_spectrum = tympan_utils.spectrum_amplitudes(
        actual_group.raw_isolation.spectrum
    )
    assert_almost_equal(
        expected_group['raw_isolation'],
        actual_raw_isolation_spectrum,
    )
    actual_sabine_inner_absorption_spectrum = tympan_utils.spectrum_amplitudes(
        actual_group.sabine_inner_absorption.spectrum
    )
    assert_almost_equal(
        expected_group['sabine_inner_absorption'],
        actual_sabine_inner_absorption_spectrum,
    )
    actual_sabine_outer_absorption_spectrum = tympan_utils.spectrum_amplitudes(
        actual_group.sabine_outer_absorption.spectrum
    )
    assert_almost_equal(
        expected_group['sabine_outer_absorption'],
        actual_sabine_outer_absorption_spectrum,
    )


def select_3dview():
    '''Maximizing the 3D view with an override context allows the current context
    to be correctly initialized. In fact this action select the 3D view area'''
    for area in bpy.context.screen.areas:
        if area.type == 'VIEW_3D':
            override = {'area': area}
            bpy.ops.screen.screen_full_area(override)
            break


def dummy_report(log_levels, message):
    pass


def to_fake_spectrum(spectrum):
    return list(spectrum)


class BlTYBaseTests(unittest.TestCase):
    """Base class for Blender-Tympan unit tests."""

    def setUp(self):
        _delete_all_objects()
        select_3dview()
        addon_prefs = bpy.context.preferences.addons['blty'].preferences
        addon_prefs.building_size = 2

    def tearDown(self):
        _delete_all_objects()
