
from bokeh.layouts import (
    column,
    widgetbox,
    row,
)
from bokeh.models import (
    ColumnDataSource,
    CustomJS,
)
from bokeh.models.widgets import (
    DataTable,
    Div,
    HTMLTemplateFormatter,
    Select,
    TableColumn,
)
from bokeh.plotting import (
    output_file,
    save,
)
import bpy
from bpy.types import Operator
from bpy.utils import (
    register_class,
    unregister_class,
)
import math
import os

from . import (
    bokeh_utils,
    tympan_utils as tu,
)


MAX_CELL_COLOR = 'red'


class TympanResults:

    def __init__(self, sources, receivers, couples):
        """ Data class to bundle results.
        @param sources: dictionary of spectrum sources
        @param receivers: dictionary of spectrum receivers
        @param couples: dictionary of spectrum couples with key = (src, rec)
        """
        self._sources = sources
        self._receivers = receivers
        self._couples = couples

    def get_source_spectrum(self, src):
        """get global spectrum for source"""
        return self._sources[src]

    def get_receiver_spectrum(self, rec):
        """get global spectrum for receiver"""
        return self._receivers[rec]

    def get_couple_spectrum(self, src, rec):
        """get the spectrum of the couple (src, rec)"""
        return self._couples[(src, rec)].to_dB()

    @property
    def receivers(self):
        return list(self._receivers.keys())

    @property
    def sources(self):
        return list(self._sources.keys())


def format_table_value(value):
    '''We wants to display result with 2 decimals.
    Bokeh doesn't support NaN values in javascript callback,
    So we return a string instead of NaN'''
    return round(value, 2) if not math.isnan(value) else 'NaN'


def get_background_cell_colors(col):
    """return the list of background color of the columnDataSource.
    All cells are white except the cell that contain the max.
    We don't take the 1st value in account because it is the synthesis"""
    colors = ['white'] * len(col)
    colors[col[1:].index(max(col[1:])) + 1] = MAX_CELL_COLOR
    return colors


class ResultTableFactory:
    """ Example of result table with two sources (s1, s2),
    two receivers (r1, r2) and one emitted building.
    >>> ResultTableFactory(result).for_frequency(5)
    ... {
         'col1_src': ['synthesis', 'Building', 's1', 's2'],
         'col2_lw': ['', 10.0, 3.0, 1.0],
         'r1': [0.0, 0.0, 0.0, 0.0],
         'r2': [0.0, 0.0, 0.0, 0.0],
         'colors0': ['white', 'red', 'white', 'white'],
         'colors1': ['white', 'red', 'white', 'white'],
        }
    """

    def __init__(self, result):
        self.result = result

    def _table_header(self, func):
        return {
            'col1_src': ['synthesis'] + self.result.sources,
            'col2_lw': [''] + [
                format_table_value(func(self.result.get_source_spectrum(s)))
                for s in self.result.sources],
        }

    def _receiver_synthesis(self, func, rec):
        return func(self.result.get_receiver_spectrum(rec))

    def _table_receiver_column(self, func, rec):
        col = [self._receiver_synthesis(func, rec)]
        for src in self.result.sources:
            col.append(
                func(self.result.get_couple_spectrum(src, rec))
            )
        return [format_table_value(val) for val in col]

    def _build_table(self, func):
        table = {}
        table.update(self._table_header(func))
        for index, rec in enumerate(self.result.receivers):
            table[rec] = self._table_receiver_column(func, rec)
            table['colors{}'.format(index)] = get_background_cell_colors(
                table[rec]
            )
        return table

    @property
    def global_table(self):
        """Format global results data as table"""
        def get_dba(spectrum):
            return spectrum.dBA

        return self._build_table(get_dba)

    def for_frequency(self, freq_index):
        """Format frequency result data as table"""
        def get_amplitude_at_freq(spectrum):
            return spectrum.values[freq_index]

        return self._build_table(get_amplitude_at_freq)


class TympanResultExporter:

    def __init__(self, result):
        self.result = result
        self.result_table_factory = ResultTableFactory(result)

    @property
    def tables_by_frequency(self):
        """Precompute all result tables by frequency"""
        tables = {}
        for index, freq in enumerate(tu.LOWER_CUTOFF_FREQUENCIES):
            table_name = "{}Hz".format(freq)
            tables[table_name] = self.result_table_factory.for_frequency(index)
        return tables

    @property
    def couples_spectra_tables(self):
        tables = {}
        for src in self.result.sources:
            tables[src] = {}
            for rec in self.result.receivers:
                tables[src][rec] = [
                    round(f, 2) for f in
                    self.result.get_couple_spectrum(src, rec).to_dB().values
                ]
        return tables

    def _create_main_html_table(self):
        '''Create the results table (global or by frequencies).
        Also create the select widget to switch between frequencies'''
        frequencies = ['{}Hz'.format(f) for f in tu.LOWER_CUTOFF_FREQUENCIES]
        select = Select(
            title="",
            options=["global"] + frequencies,
            width=30,
        )
        columns = [
            TableColumn(field="col1_src", title=""),
            TableColumn(field="col2_lw", title="LW"),
        ]
        template = """
        <div style="background:<%=colors{}%>"; color="white";>
        <%= (value) %></div>
        """
        for index, rec in enumerate(self.result.receivers):
            cformatter = HTMLTemplateFormatter(template=template.format(index))
            columns.append(
                TableColumn(
                    field=rec,
                    title=rec,
                    formatter=cformatter,
                )
            )
        data = {"global": self.result_table_factory.global_table}
        data.update(self.tables_by_frequency)
        source = ColumnDataSource(data['global'])
        main_table = DataTable(source=source, columns=columns, height=250)
        callback = CustomJS(
            args=dict(source=source, data=data, select=select),
            code="""
            // update sources
            es = document.getElementById(select.id);
            value = es.options[es.selectedIndex].value;
            source.data = data[value]
            source.change.emit();
            """
        )
        select.js_on_change('value', callback)
        return widgetbox(select, main_table)

    def _create_couple_spectrum_result(self):
        """Create spectrum table and histogram."""
        section_title = widgetbox(
            Div(text="""<h2>Couples spectra</h2>""")
        )
        select_src = Select(title="Source", value="src1",
                            options=self.result.sources)
        select_rec = Select(title="Receiver", value="rec1",
                            options=self.result.receivers)
        data = self.couples_spectra_tables
        default_data = {
            'freq': tu.LOWER_CUTOFF_FREQUENCIES,
            'values': data[self.result.sources[0]][self.result.receivers[0]],
        }
        source = ColumnDataSource(default_data)
        columns = [
            TableColumn(field="freq", title="Frequencies"),
            TableColumn(field="values", title="Values"),
        ]
        data_table = DataTable(
            source=source,
            columns=columns,
            height=800,
            width=600,
        )
        histogram = bokeh_utils.create_histogram(source)
        callback = CustomJS(
            args=dict(
                source=source,
                data=data,
                src=select_src,
                rec=select_rec,
            ),
            code="""
            // update sources
            es = document.getElementById(src.id);
            src_value = es.options[es.selectedIndex].value;
            er = document.getElementById(rec.id);
            rec_value = er.options[er.selectedIndex].value;
            source.data.values = data[src_value][rec_value];
            source.change.emit();
            """
        )
        select_src.js_on_change('value', callback)
        select_rec.js_on_change('value', callback)
        return column(
            section_title,
            row(select_src, select_rec),
            row(data_table, histogram),
        )

    def write_html_result(self, result_file_path):
        """Write and display global results."""
        document_title = widgetbox(
            Div(text="""<h1>Tympan Result<h1>""")
        )
        global_section_title = widgetbox(
            Div(text="""<h2>Global/Frequency results</h2>""")
        )
        output_file(result_file_path, mode="inline")
        save(column(
            document_title,
            global_section_title,
            self._create_main_html_table(),
            self._create_couple_spectrum_result(),
        ))


class TympanDisplayResults(Operator):
    bl_label = 'Display Solver Results'
    bl_idname = tu.blty_idname('display_results')

    def execute(self, context):
        bpy.ops.wm.path_open(filepath=os.getenv('TYMPAN_RESULT_FILE'))
        return {'FINISHED'}


def register():
    register_class(TympanDisplayResults)


def unregister():
    unregister_class(TympanDisplayResults)
