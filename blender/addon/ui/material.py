from bpy.types import Panel, UIList


class MATERIAL_UL_matslots(UIList):
    """Slot for material management"""
    def draw_item(self, context, layout, data, item, icon,
                  active_data, active_propname, index):
        material = item.material
        if self.layout_type in {'DEFAULT', 'COMPACT'}:
            if material:
                layout.prop(material, "name", text="",
                            emboss=False, icon_value=icon)
            else:
                layout.label(text="", icon_value=icon)
        elif self.layout_type == 'GRID':
            layout.alignment = 'CENTER'
            layout.label(text="", icon_value=icon)


class MATERIAL_PT_context_material(Panel):
    """Panel for editing materials of infrastructure"""
    bl_label = ""
    bl_options = {'HIDE_HEADER'}
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "material"

    @classmethod
    def poll(cls, context):
        return context.object.type == 'MESH'

    def draw(self, context):
        layout = self.layout
        active_object = context.object
        if active_object:
            is_sortable = len(active_object.material_slots) > 1
            rows = 1
            if is_sortable:
                rows = 4
            row = layout.row()
            row.template_list("MATERIAL_UL_matslots", "", active_object,
                              "material_slots", active_object,
                              "active_material_index", rows=rows)
            col = row.column(align=True)
            col.operator("object.material_slot_add",
                         icon='ADD',
                         text="")
            col.operator("object.material_slot_remove",
                         icon='REMOVE',
                         text="")
            if is_sortable:
                col.separator()
                col.operator("object.material_slot_move",
                             icon='TRIA_UP',
                             text=""
                             ).direction = 'UP'
                col.operator("object.material_slot_move",
                             icon='TRIA_DOWN',
                             text=""
                             ).direction = 'DOWN'
            if active_object.mode == 'EDIT':
                row = layout.row(align=True)
                row.operator("object.material_slot_assign", text="Assign")
                row.operator("object.material_slot_select", text="Select")
                row.operator("object.material_slot_deselect", text="Deselect")
        split = layout.split()
        if active_object:
            split.template_ID(active_object,
                              "active_material",
                              new="blty.add_acoustic_material")
            row = split.row()
