from bpy.types import Panel
from bpy.utils import (
    register_class,
    unregister_class,
)

from blty.element_types import is_reference_image


class TYMPANVIEW3D_PT_reference_image_properties(Panel):
    """Panel for viewing reference images properties"""
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = 'object'
    bl_label = 'Reference Image'
    bl_options = {'HIDE_HEADER'}

    def draw(self, context):
        object = context.object
        self.layout.prop(object, 'color', index=3, slider=True,
                         text='Opacity')

    @classmethod
    def poll(cls, context):
        obj = context.object
        return is_reference_image(obj)


class TYMPANVIEW3D_PT_reference_image_tools(Panel):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_label = 'Reference Image'
    bl_options = {'HIDE_HEADER'}

    def draw(self, context):
        self.layout.operator('blty.draw_image_scale', icon='IMAGE_PLANE',
                             text='')
        self.layout.operator(
            'blty.draw_image_north',
            icon='RESTRICT_SELECT_OFF',
            text='',
        )

    @classmethod
    def poll(self, context):
        obj = context.object
        return obj and is_reference_image(obj)


classes = (
    TYMPANVIEW3D_PT_reference_image_properties,
    TYMPANVIEW3D_PT_reference_image_tools,
)


def register():
    for cls in classes:
        register_class(cls)


def unregister():
    for cls in classes:
        unregister_class(cls)
