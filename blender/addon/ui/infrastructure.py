from bpy.types import (
    Panel,
    UIList,
)
from bpy.utils import (
    register_class,
    unregister_class,
)
import numpy as np

from blty import (
    infrastructure as iftr,
    tympan_utils as tu,
)
from blty.element_types import (
    can_be_emissive,
    get_internal_sources,
    is_building,
    is_infrastructure,
    is_machine,
    use_emissivity,
)
from . import (
    emissive_template_spectrum,
    object_template_spectrum,
    absorption_parameter_template_spectrum,
    material,
)


class TYMPANVIEW3D_PT_acoustic_material(Panel):
    """Panel for viewing an acoustic material"""
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "material"
    bl_label = "Acoustic material"
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        active_material = context.object.active_material
        props = active_material.acoustic_properties
        object_template_spectrum(self.layout, props.spectrum, editable=True)

    @classmethod
    def poll(cls, context):
        if context.material is None:
            return False
        # Only activate for Mesh object (not curves, etc.)
        obj = context.object
        return obj and is_infrastructure(obj)


class TYMPANVIEW3D_PT_acoustic_material_metadata(Panel):
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "material"
    bl_label = "Metadata"
    bl_options = {"DEFAULT_CLOSED"}
    bl_parent_id = "TYMPANVIEW3D_PT_acoustic_material"

    def draw(self, context):
        active_material = context.object.active_material
        props = active_material.acoustic_properties
        self.layout.prop(props, "volumeID")


class TYMPANVIEW3D_PT_infrastructure(Panel):
    """Panel for grouping infrastructure properties"""
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "object"
    bl_label = "Tympan infrastructure"
    bl_options = {'HIDE_HEADER'}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and is_infrastructure(obj)


class TYMPANVIEW3D_PT_emissivity(Panel):
    """Panel for editing the infrastructure emissivity"""
    bl_parent_id = "TYMPANVIEW3D_PT_infrastructure"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "object"
    bl_label = "Emissivity"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        obj = context.object
        if is_building(obj) and not iftr.can_building_be_emissive(obj):
            layout.label(
                text="Can't be emissive: No emissive internal sources"
            )
        else:
            layout.prop(obj, "tympan_use_emissivity", text="Emissive")

    @classmethod
    def poll(cls, context):
        return can_be_emissive(context.object)


class OBJECT_UL_discretization_groups(UIList):
    def draw_item(self, context, layout, data, item, icon, active_data,
                  active_propname, index):
        layout.prop(item, "name", text="", emboss=False)


class TYMPANVIEW3D_PT_infra_emissivity(Panel):
    """Panel for editing the infrastructure emissivity"""
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "physics"
    bl_label = "Emissivity"

    def draw(self, context):
        obj = context.object
        layout = self.layout
        layout.active = use_emissivity(obj)

        row = layout.row()
        row.template_list("OBJECT_UL_discretization_groups", '', obj,
                          'tympan_discretization_groups', obj,
                          'active_discretization_group_index', rows=2)
        col = row.column(align=True)
        col.operator(
            tu.blty_idname('add_tympan_discretization_group'),
            icon='ADD',
            text='',
        )
        col.operator(
            tu.blty_idname('remove_tympan_discretization_group'),
            icon='REMOVE',
            text='',
        )
        if obj.data.is_editmode:
            row = layout.row(align=True)
            row.operator(
                tu.blty_idname('assign_faces_to_discretization_group'),
                text='Assign',
            )
            row.operator(
                tu.blty_idname('remove_faces_from_discretization_group'),
                text='Unassign',
            )
            row.operator(
                tu.blty_idname('select_faces_in_discretization_group'),
                text='Select',
            )
            row.operator(
                tu.blty_idname('deselect_faces_in_discretization_group'),
                text='Deselect',
            )
        active_index = obj.active_discretization_group_index
        props = obj.tympan_discretization_groups[active_index]
        self.layout.use_property_split = True
        layout.prop(props, "directivity_type", text="Directtivity Type")
        layout.prop(props, "algorithm", text="Algorithm")
        if props.algorithm == "subdivide":
            layout.prop(props, "subdivision_number", text="Subidivisions")
        elif props.algorithm == "uniform":
            layout.prop(props, "uniform_step", text="Step")
        elif props.algorithm == "circular":
            layout.prop(props, "circular_divisions", text="Circles")

    @classmethod
    def poll(cls, context):
        return use_emissivity(context.object)


class TYMPANVIEW3D_PT_empty_discretization_group(Panel):
    """Panel for editing the emissivity spectrum"""
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = 'physics'
    bl_label = 'Emissivity Spectrum'
    bl_parent_id = 'TYMPANVIEW3D_PT_infra_emissivity'
    bl_options = {'HIDE_HEADER'}

    def draw(self, context):
        self.layout.label(
            text='There are no face associated with this group yet'
        )

    @classmethod
    def poll(cls, context):
        return iftr.is_active_group_empty(context.object)


class TYMPANVIEW3D_PT_discretization_group_spectrum(Panel):
    """Panel for editing the machine emissivity spectrum"""
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = 'physics'
    bl_label = 'Emissivity Spectrum'
    bl_parent_id = 'TYMPANVIEW3D_PT_infra_emissivity'
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        obj = context.object
        layout = self.layout
        active_index = obj.active_discretization_group_index
        props = obj.tympan_discretization_groups[active_index]
        emissive_template_spectrum(layout, props.spectrum)

    @classmethod
    def poll(cls, context):
        obj = context.object
        return is_machine(obj) and not iftr.is_active_group_empty(obj)


class TYMPANVIEW3D_PT_building_discretization_group_parameters(Panel):
    """Panel for editing the building parameters"""
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = 'physics'
    bl_label = 'Parameters'
    bl_parent_id = 'TYMPANVIEW3D_PT_infra_emissivity'
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        obj = context.object
        layout = self.layout
        row = layout.row(align=True)
        active_index = obj.active_discretization_group_index
        props = obj.tympan_discretization_groups[active_index]
        row.prop(props, 'active_absorption_parameter', text='parameter')
        if props.active_absorption_parameter == 'raw_isolation':
            absorption_parameter_template_spectrum(
                layout, props.raw_isolation.spectrum
            )
        elif props.active_absorption_parameter == 'sabine_inner_absorption':
            absorption_parameter_template_spectrum(
                layout, props.sabine_inner_absorption.spectrum
            )
        else:
            absorption_parameter_template_spectrum(
                layout, props.sabine_outer_absorption.spectrum
            )

    @classmethod
    def poll(cls, context):
        o = context.object
        return is_building(o) and not iftr.is_active_group_empty(o)


class TYMPANVIEW3D_PT_infra_elevation(Panel):
    """Panel for editing the infrastructure elevation"""
    bl_parent_id = "TYMPANVIEW3D_PT_infrastructure"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "object"
    bl_label = ""
    bl_options = {'HIDE_HEADER'}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        row = layout.row()
        row.prop(context.object, "tympan_elevation")

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and is_infrastructure(obj)


class TYMPANVIEW3D_PT_infra_is_used(Panel):
    """Panel for use/non-use the infrastructure"""
    bl_parent_id = "TYMPANVIEW3D_PT_infrastructure"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "object"
    bl_label = ""
    bl_options = {'HIDE_HEADER'}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        row = layout.row()
        row.prop(context.object, 'tympan_used', text='Send to solver')

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and is_infrastructure(obj)


class TYMPANVIEW3D_PT_parent_building(Panel):
    """Panel for editing the machine optional parent building"""
    bl_parent_id = "TYMPANVIEW3D_PT_infrastructure"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "object"
    bl_label = "Building"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        obj = context.object
        layout.prop(obj, 'parent_building', text='Building')

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and is_machine(obj)


class TYMPANVIEW3D_PT_building_children(Panel):
    """Panel for display the children (machines and soundsources)"""
    bl_parent_id = "TYMPANVIEW3D_PT_infrastructure"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "object"
    bl_label = "Children"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        box = layout.box()
        for child in get_internal_sources(context.object):
            row = box.row()
            select_operator = row.operator(
                tu.blty_idname('select_child_of_building'),
                text=child.name,
            )
            select_operator.child_name = child.name
            child_type = "MACHINE" if is_machine(child) else "SOURCE"
            row.label(text=child_type)

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and is_building(obj) and get_internal_sources(obj)


class TYMPANVIEW3D_PT_infra_display_eqss(Panel):
    """Panel for show/hide the infrastructure equivalent sound sources"""
    bl_parent_id = "TYMPANVIEW3D_PT_emissivity"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "object"
    bl_label = ""
    bl_options = {'HIDE_HEADER'}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        row = layout.row()
        row.prop(context.object, "tympan_display_eqss")

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and iftr.has_emitting_faces(obj)


class TYMPANVIEW3D_PT_infra_global_eqss_spectrum(Panel):
    """Panel for display the global spectrum of emissive infrastructure"""
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "physics"
    bl_label = "Global Spectrum"

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        infra = context.object
        amplitudes = []
        amplitudes, freqs = iftr.get_global_emission_spectrum(infra)
        if amplitudes:
            layout.label(text="Global emission spectrum")
            sublayout = layout.split(factor=0.5)
            if is_machine(infra):
                sublayout.operator(
                    tu.blty_idname('confirm_edit_global_emission_spectrum'),
                    text='edit',
                )
            global_grid = sublayout.grid_flow(columns=1)
            for freq, ampl in zip(freqs, np.around(amplitudes, 2)):
                box = global_grid.box()
                box.scale_y = 0.4
                row = box.row().column_flow(columns=2)
                row.label(text=str(freq))
                row.label(text=str(ampl))

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and iftr.has_emitting_faces(obj)


class TYMPANVIEW3D_PT_calc_spectrum_of_selected_faces(Panel):
    """Panel for display the spectrum of selected faces of building"""
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "physics"
    bl_label = "Spectrum of selected faces"

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.operator(
            tu.blty_idname('calc_emissivity_spectrum_of_selected_faces'),
            text='compute spectrum',
        )

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and iftr.has_emitting_faces(obj)


classes = (
    material.MATERIAL_PT_context_material,
    TYMPANVIEW3D_PT_infrastructure,
    TYMPANVIEW3D_PT_infra_elevation,
    TYMPANVIEW3D_PT_infra_is_used,
    OBJECT_UL_discretization_groups,
    TYMPANVIEW3D_PT_emissivity,
    TYMPANVIEW3D_PT_infra_emissivity,
    TYMPANVIEW3D_PT_infra_display_eqss,
    TYMPANVIEW3D_PT_parent_building,
    TYMPANVIEW3D_PT_building_children,
    TYMPANVIEW3D_PT_calc_spectrum_of_selected_faces,
    TYMPANVIEW3D_PT_infra_global_eqss_spectrum,
    TYMPANVIEW3D_PT_acoustic_material,
    TYMPANVIEW3D_PT_acoustic_material_metadata,
    TYMPANVIEW3D_PT_discretization_group_spectrum,
    TYMPANVIEW3D_PT_empty_discretization_group,
    TYMPANVIEW3D_PT_building_discretization_group_parameters,
)


def register():
    register_class(material.MATERIAL_UL_matslots)
    for cls in classes:
        register_class(cls)


def unregister():
    for cls in classes:
        unregister_class(cls)
