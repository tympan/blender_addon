import os

import bpy
from bpy.types import (
    Header,
    Panel,
)
from bpy.utils import (
    register_class,
    unregister_class,
)

from blty import (
    element_types as et,
    tympan_utils as tu,
)


class TYMPANPROPERTIES_HT_Header(Header):
    bl_space_type = 'PROPERTIES'

    def draw(self, context):
        layout = self.layout
        layout.template_header()
        scene = context.scene
        layout.prop(scene, 'data_mode', expand=True, text='')

    def get_datamode_items(scene, context):
        items = []
        obj = bpy.context.object
        if obj is not None:
            items.append(('OBJECT', 'Object', 'Object data',
                          'OBJECT_DATAMODE'))
            if obj.type == 'MESH' and not et.is_ground_set(obj):
                items.append(('MATERIAL', 'Material', 'Material data',
                              'MATERIAL_DATA'))
            if et.use_emissivity(obj):
                items.append(('PHYSICS', 'Object',
                              'Physics', 'PHYSICS'))
        return tuple((item + (idx,) for idx, item in enumerate(items)))

    def get_datamode_enum(scene):
        cls = TYMPANPROPERTIES_HT_Header
        current_datamode = bpy.context.space_data.context
        items = cls.get_datamode_items(scene, None)
        for item_idx, datamode in enumerate(items):
            if current_datamode == datamode[0]:
                return item_idx
        return -1

    def set_datamode_enum(scene, key):
        cls = TYMPANPROPERTIES_HT_Header
        items = cls.get_datamode_items(scene, None)
        bpy.context.space_data.context = items[key][0]

    @classmethod
    def register(cls):
        bpy.types.Scene.data_mode = bpy.props.EnumProperty(
            items=cls.get_datamode_items,
            get=cls.get_datamode_enum,
            set=cls.set_datamode_enum)

    @classmethod
    def unregister(cls):
        del bpy.types.Scene.data_mode


class TYMPANVIEW3D_PT_Solve(Panel):
    """Panel for the "Solve" tab."""
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Solve"
    bl_context = "objectmode"
    bl_label = "Solve"

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        col = layout.column(align=True)

        row = col.row(align=True)
        row.operator(tu.blty_idname("compute_altimetry"),
                     text="Compute altimetry")
        if context.scene.altimetry_errors:
            error_col = row.column(align=True)
            error_col.alert = True
            error_col.operator(tu.blty_idname("show_altimetry_errors"),
                               text="", icon="ERROR")

        row = col.row(align=True)
        row.operator(tu.blty_idname("solve"), text="Solve")
        row.operator(tu.blty_idname("solver_parameters"), text="",
                     icon='PREFERENCES')


class TYMPANVIEW3D_PT_Landtake(Panel):
    """Panel to show if the objects are send or not to solver"""
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Solve"
    bl_label = "Landtake"
    bl_parent_id = "TYMPANVIEW3D_PT_Solve"

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False
        col = layout.column(align=True)
        col.prop(context.scene, 'landtake_xsize')
        col.prop(context.scene, 'landtake_ysize')
        col.prop(context.scene, 'landtake_used_as_level_curve')
        if context.scene.landtake_used_as_level_curve:
            col.prop(context.scene, 'landtake_height')


class TYMPANVIEW3D_PT_Default_Ground(Panel):
    """Panel to set the default Ground"""
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Solve"
    bl_label = "Default Ground"
    bl_parent_id = "TYMPANVIEW3D_PT_Solve"

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False
        col = layout.column(align=True)
        col.prop(context.scene.default_ground, 'resistivity')
        col.prop(context.scene.default_ground, 'deviation')
        col.prop(context.scene.default_ground, 'length')


class TYMPANVIEW3D_PT_Summary(Panel):
    """Panel to show if the objects are send or not to solver"""
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Solve"
    bl_context = "objectmode"
    bl_label = "Summary"
    bl_options = {'HIDE_HEADER'}

    @staticmethod
    def draw_display_summary(layout):
        layout.operator(tu.blty_idname("display_summary"),
                        text="Summary")

    def draw(self, context):
        col = self.layout.column(align=True)
        self.draw_display_summary(col)


class TYMPANVIEW3D_PT_Result(Panel):
    """Panel for the "Result" tab."""
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Solve"
    bl_context = "objectmode"
    bl_label = "Result"

    @staticmethod
    def draw_display_results_action(layout):
        layout.operator(tu.blty_idname("display_results"),
                        text="See result")

    def draw(self, context):
        col = self.layout.column(align=True)
        self.draw_display_results_action(col)

    @classmethod
    def poll(cls, context):
        # check if file is not empty
        return os.stat(os.getenv('TYMPAN_RESULT_FILE')).st_size


class TYMPANVIEW3D_PT_tools_add_object(Panel):
    """Panel for the "Create" tab providing access to "primitive" object
    creation.
    """
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Create"
    bl_context = "objectmode"
    bl_label = "Add Primitive"
    bl_options = {'HIDE_HEADER'}

    @staticmethod
    def draw_add_sound_source(layout):
        layout.operator(tu.blty_idname("add_sound_source"),
                        text="Sound source")

    @staticmethod
    def draw_add_sound_receptor(layout):
        layout.operator(tu.blty_idname("add_sound_receiver"),
                        text="Sound receiver")

    @staticmethod
    def draw_add_infrastructure(layout):
        layout.label(text="Building:", icon="MOD_BUILD")
        layout.operator_enum(tu.blty_idname("add_building"), "base_mesh")

    @staticmethod
    def draw_add_reference_image(layout):
        layout.operator_enum(tu.blty_idname("import_reference_image"),
                             "image_type")

    @staticmethod
    def draw_add_altimetry_feature(layout):
        col = layout.column(align=True)
        col.operator(tu.blty_idname('add_level_curve'),
                     icon=et.type_icon('ALTIMETRY', 'LEVEL CURVE'))
        col.operator(tu.blty_idname('add_ground_set'),
                     icon=et.type_icon('ALTIMETRY', 'GROUND SET'))
        col.operator(tu.blty_idname('add_water_body'),
                     icon=et.type_icon('ALTIMETRY', 'WATER BODY'))

    def draw(self, context):
        layout = self.layout

        col = layout.column(align=True)
        col.label(text="Tympan:")
        self.draw_add_sound_source(col)
        self.draw_add_sound_receptor(col)

        col = layout.column(align=True)
        col.label(text="Infrastructure:")
        self.draw_add_infrastructure(col)

        col = layout.column(align=True)
        col.label(text="Altimetry:")
        self.draw_add_altimetry_feature(col)

        col = layout.column(align=True)
        col.label(text="Reference Image:")
        self.draw_add_reference_image(col)


def absorption_parameter_template_spectrum(layout, spectrum):
    layout.use_property_split = True
    row = layout.row(align=True)
    row.operator(tu.blty_idname('copy_parameter_spectrum_as_csv'), text='',
                 icon='COPYDOWN')
    row.operator(tu.blty_idname('paste_parameter_spectrum'),
                 text='', icon='PASTEDOWN')
    row.operator(tu.blty_idname('export_parameter_spectrum_as_csv'),
                 text='', icon='EXPORT')
    row.operator(tu.blty_idname('import_parameter_spectrum_from_csv'),
                 text='', icon='IMPORT')
    row.operator(tu.blty_idname('display_parameter_spectrum'), text='',
                 icon='SEQ_HISTOGRAM')
    col = layout.column(align=True)
    template_spectrum_bands(col, spectrum)


def emissive_template_spectrum(layout, spectrum):
    layout.use_property_split = True
    row = layout.row(align=True)
    row.operator(tu.blty_idname('copy_emission_spectrum_as_csv'), text='',
                 icon='COPYDOWN')
    row.operator(tu.blty_idname('paste_emission_spectrum'),
                 text='', icon='PASTEDOWN')
    row.operator(tu.blty_idname('export_emission_spectrum_as_csv'),
                 text='', icon='EXPORT')
    row.operator(tu.blty_idname('import_emission_spectrum_from_csv'),
                 text='', icon='IMPORT')
    row.operator(tu.blty_idname('display_emission_spectrum'), text='',
                 icon='SEQ_HISTOGRAM')
    col = layout.column(align=True)
    template_spectrum_bands(col, spectrum)


def object_template_spectrum(layout, spectrum, editable=False):
    layout.use_property_split = True
    row = layout.row(align=True)
    row.operator(tu.blty_idname('copy_spectrum_as_csv'), text='',
                 icon='COPYDOWN')
    if editable:
        row.operator(tu.blty_idname('paste_spectrum'),
                     text='', icon='PASTEDOWN')
    row.operator(tu.blty_idname('export_spectrum_as_csv'),
                 text='', icon='EXPORT')
    if editable:
        row.operator(tu.blty_idname('import_spectrum_from_csv'),
                     text='', icon='IMPORT')
    row.operator(tu.blty_idname('display_spectrum'), text='',
                 icon='SEQ_HISTOGRAM')
    col = layout.column(align=True)
    if not editable:
        col.enabled = False
    template_spectrum_bands(col, spectrum)


def template_spectrum_bands(col, spectrum):
    col.label(text="dBA: {}".format(spectrum.dBA))
    col.label(text='one-third octave bands')
    for band in spectrum.bands:
        col.prop(band, 'amplitude', text='{0:g}'.format(band.frequency))


class TYMPANVIEW3D_PT_soundsource_properties(Panel):
    """Panel for editing the sound source properties of the active object"""
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "object"
    bl_label = ""
    bl_options = {'HIDE_HEADER'}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        obj = context.object
        layout.prop(obj, 'tympan_elevation', text='elevation')
        layout.prop(obj, 'tympan_used', text='Send to solver')
        layout.prop(obj, 'parent_building', text='building')
        spectrum = obj.tympan_soundsource_properties[0].spectrum
        object_template_spectrum(layout, spectrum, editable=True)

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and len(obj.tympan_soundsource_properties) > 0


class TYMPANVIEW3D_PT_soundreceiver_properties(Panel):
    """Panel for viewing the soundreceiver properties of the active object"""
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "object"
    bl_label = ""
    bl_options = {'HIDE_HEADER'}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        obj = context.object
        layout.prop(obj, 'tympan_elevation', text='elevation')
        layout.prop(context.object, 'tympan_used', text='Send to solver')
        spectrum = obj.tympan_soundreceiver_properties[0].spectrum
        object_template_spectrum(layout, spectrum)

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and 'tympan_soundreceiver_properties' in obj


class ObjectSubtypeTypePanel(Panel):
    """Panel for editing the type of an object"""
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "object"
    bl_label = "Type"
    bl_options = {'HIDE_HEADER'}

    def draw(self, context):
        self.layout.prop(context.object, "tympan_subtype", expand=True)


class TYMPANPROPERTIES_PT_infrastructure_type(ObjectSubtypeTypePanel):

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and et.is_infrastructure(obj)


class TYMPANPROPERTIES_PT_undefined_mesh_type(ObjectSubtypeTypePanel):

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and obj.type == 'MESH' and et.is_undefined(obj)


class TYMPANPROPERTIES_PT_image_type(ObjectSubtypeTypePanel):

    @staticmethod
    def is_empty_image(obj):
        return obj.type == 'EMPTY' and obj.empty_display_type == 'IMAGE'

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and cls.is_empty_image(obj)


def enable_object_button(self, context):
    self.layout.row().prop(context.object, "hide_viewport")


classes = (
    TYMPANPROPERTIES_HT_Header,
    TYMPANVIEW3D_PT_tools_add_object,
    TYMPANVIEW3D_PT_Summary,
    TYMPANVIEW3D_PT_Solve,
    TYMPANVIEW3D_PT_Landtake,
    TYMPANVIEW3D_PT_Default_Ground,
    TYMPANVIEW3D_PT_Result,
    TYMPANVIEW3D_PT_soundsource_properties,
    TYMPANVIEW3D_PT_soundreceiver_properties,
    TYMPANPROPERTIES_PT_infrastructure_type,
    TYMPANPROPERTIES_PT_undefined_mesh_type,
    TYMPANPROPERTIES_PT_image_type,
)


removed_classes = (
    bpy.types.PROPERTIES_HT_header,
    bpy.types.DATA_PT_modifiers,
    bpy.types.OBJECT_PT_constraints,
    bpy.types.OBJECT_PT_custom_props,
    bpy.types.OBJECT_PT_delta_transform,
    bpy.types.OBJECT_PT_display,
    bpy.types.OBJECT_PT_motion_paths,
    bpy.types.OBJECT_PT_relations,
    bpy.types.OBJECT_PT_visibility,
    bpy.types.OBJECT_PT_collections,
    bpy.types.OBJECT_PT_display_bounds,
    bpy.types.OBJECT_PT_instancing,
    bpy.types.OBJECT_PT_motion_paths_display,
    bpy.types.MATERIAL_MT_context_menu,
    bpy.types.MATERIAL_UL_matslots,
    bpy.types.MATERIAL_PT_preview,
    bpy.types.EEVEE_MATERIAL_PT_context_material,
    bpy.types.EEVEE_MATERIAL_PT_surface,
    bpy.types.EEVEE_MATERIAL_PT_volume,
    bpy.types.EEVEE_MATERIAL_PT_settings,
    bpy.types.EEVEE_MATERIAL_PT_viewport_settings,
    bpy.types.MATERIAL_PT_custom_props,

    bpy.types.PHYSICS_PT_add,

    bpy.types.VIEW3D_PT_context_properties,
    bpy.types.VIEW3D_PT_grease_pencil,
    bpy.types.VIEW3D_PT_transform_orientations,
    bpy.types.VIEW3D_PT_view3d_properties,
)


def register():
    for cls in removed_classes:
        unregister_class(cls)
    for cls in classes:
        register_class(cls)
    bpy.types.OBJECT_PT_context_object.append(enable_object_button)
    from . import infrastructure
    from . import reference_images
    from . import altimetry
    infrastructure.register()
    reference_images.register()
    altimetry.register()


def unregister():
    bpy.types.OBJECT_PT_context_object.remove(enable_object_button)
    for cls in classes:
        unregister_class(cls)
    for cls in removed_classes:
        register_class(cls)
    from . import altimetry
    from . import reference_images
    from . import infrastructure
    altimetry.unregister()
    reference_images.unregister()
    infrastructure.unregister()
