from bpy.types import (
    Panel,
    UIList,
)
from bpy.utils import (
    register_class,
    unregister_class,
)

from blty import tympan_utils as tu
from blty.element_types import (
    is_altimetry_mesh,
    is_level_curve,
    is_ground_set,
    is_water_body,
)


class TYMPANPROPERTIES_PT_level_curve(Panel):
    """Panel for grouping infrastructure properties"""
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "object"
    bl_label = "Tympan Level Curve"
    bl_options = {'HIDE_HEADER'}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        obj = context.object
        height = obj.location.z
        if len(obj.data.vertices) > 1:
            height += obj.data.vertices[0].co.z
        layout.operator(
            'blty.set_level_curve_height',
            text='Set Level Curve Height ({} m)'.format(height),
        )

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and is_level_curve(obj)


class TYMPANPROPERTIES_PT_water_body(Panel):
    """Panel for grouping water body properties"""
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "object"
    bl_label = "Tympan Water Body"
    bl_options = {'HIDE_HEADER'}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        obj = context.object
        height = obj.location.z
        if len(obj.data.vertices) > 1:
            height += obj.data.vertices[0].co.z
        layout.operator(
            'blty.set_water_body_height',
            text='Set Water Body Height ({} m)'.format(height),
        )

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and is_water_body(obj)


class OBJECT_UL_ground_set_grounds(UIList):

    def draw_item(self, context, layout, data, item, icon, active_data,
                  active_propname, index):
        layout.prop(item, "name", text="", emboss=False)


class TYMPANPROPERTIES_PT_ground_set(Panel):
    """Panel for associating ground types to faces of a ground set"""
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = 'object'
    bl_label = "Tympan Ground Set"
    bl_options = {'HIDE_HEADER'}

    def draw(self, context):
        obj = context.object
        layout = self.layout
        row = layout.row()
        row.template_list('OBJECT_UL_ground_set_grounds', '', obj,
                          'tympan_grounds', obj,
                          'active_ground_index', rows=2)
        col = row.column(align=True)
        col.operator(tu.blty_idname('add_ground'), text='', icon='ADD')
        col.operator(tu.blty_idname('remove_ground'), text='', icon='REMOVE')
        if obj.data.is_editmode:
            row = layout.row(align=True)
            row.operator(
                tu.blty_idname('assign_faces_to_ground'),
                text='Assign'
            )
            row.operator(
                tu.blty_idname('select_active_ground_faces'),
                text='Select'
            )
            row.operator(
                tu.blty_idname('deselect_active_ground_faces'),
                text='Deselect'
            )
        layout.use_property_split = True
        current_ground = obj.tympan_grounds[obj.active_ground_index]
        layout.prop(current_ground, "ground_type")
        layout.prop(current_ground, "resistivity")
        layout.prop(current_ground.material, "diffuse_color")

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and is_ground_set(obj)


class TYMPANPROPERTIES_PT_altimetry_mesh(Panel):
    """Panel for editing properties of the altimetry mesh"""
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = 'object'
    bl_label = "Tympan Altimetry"
    bl_options = {'HIDE_HEADER'}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True

        layout.operator("blty.update_altimetry_projection",
                        text="Update Projections",
                        icon="FILE_REFRESH")

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and is_altimetry_mesh(obj)


class TYMPANPROPERTIES_PT_altimetry_mesh_layers_opacity(Panel):
    bl_label = "Layers Opacity"
    bl_parent_id = "TYMPANPROPERTIES_PT_altimetry_mesh"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = 'object'

    def draw(self, context):
        layout = self.layout
        layout.use_property_decorate = False
        layout.use_property_split = True

        obj = context.object
        material = obj.material_slots[0].material

        # Grounds Projection
        grounds_alpha_mult = material.node_tree.nodes["grounds"].inputs[0]
        layout.prop(grounds_alpha_mult, "default_value",
                    text="Grounds", slider=True)

        # Reference Image Projection
        projected_images = reversed(list(enumerate(obj.projected_images)))
        for idx, projected_image in projected_images:
            layer_mixer_node = material.node_tree.nodes[f"image-{idx}"]
            image_alpha_mult = layer_mixer_node.inputs[0]
            layout.prop(image_alpha_mult, "default_value",
                        text=projected_image.name, slider=True)

        # Height Map
        image_alpha_mult = material.node_tree.nodes["height"].inputs[0]
        layout.prop(image_alpha_mult, "default_value",
                    text="Height Map", slider=True)


class TYMPANPROPERTIES_PT_altimetry_mesh_height_map(Panel):
    bl_label = "Height Map"
    bl_parent_id = "TYMPANPROPERTIES_PT_altimetry_mesh"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = 'object'
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False

        obj = context.object
        material = obj.material_slots[0].material
        nodes = material.node_tree.nodes
        height_map_range = nodes["height_map_range"]

        col = layout.column(align=True)
        col.prop(height_map_range.inputs[1], "default_value",
                 text="Min Height")
        col.prop(height_map_range.inputs[2], "default_value",
                 text="Max Height")

        height_map_val_to_rgb_node = nodes["height_map_val_to_rgb"]
        layout.template_color_ramp(height_map_val_to_rgb_node, "color_ramp")


classes = (
    OBJECT_UL_ground_set_grounds,
    TYMPANPROPERTIES_PT_level_curve,
    TYMPANPROPERTIES_PT_water_body,
    TYMPANPROPERTIES_PT_ground_set,
    TYMPANPROPERTIES_PT_altimetry_mesh,
    TYMPANPROPERTIES_PT_altimetry_mesh_layers_opacity,
    TYMPANPROPERTIES_PT_altimetry_mesh_height_map,
)


def register():
    for cls in classes:
        register_class(cls)


def unregister():
    for cls in classes:
        unregister_class(cls)
