from itertools import chain
import os

import bgl
import bpy
import bpy.ops
import bpy.props
from bpy.types import (
    Operator,
    VIEW3D_MT_object,
)
from bpy.utils import (
    register_class,
    unregister_class,
)
import gpu
from gpu_extras import batch
from mathutils import Vector

import math
import numpy as np

from . import (
    element_types as et,
    infrastructure as iftr,
    solve,
    tympan_utils as tu,
    geom2d,
)


GROUND_TYPES = {
    'user_defined': {
        'name': 'Custom',
        'resistivity': 0,
    },
    'asphalt': {
        'name': 'Asphalt',
        'resistivity': 20000,
        'color': (0, 0, 0.55, 1),
    },
    'paved_floor': {
        'name': 'Pavement',
        'resistivity': 9500,
        'color': (0.66, 0.76, 0.83, 1),
    },
    'compacted_clay': {
        'name': 'Compact Ground',
        'resistivity': 6666,
        'color': (0.43, 0.27, 0.12, 1),
    },
    'compacted_sand': {
        'name': 'Compact Sand',
        'resistivity': 1300,
        'color': (0.89, 0.75, 0.13, 1),
    },
    'low_vegetation': {
        'name': 'Low Vegetation',
        'resistivity': 666,
        'color': (0.89, 0.90, 0.25, 1),
    },
    'lawn': {
        'name': 'Lawn',
        'resistivity': 350,
        'color': (0, 0.49, 0, 1),
    },
    'grassy_ground': {
        'name': 'Grassy Ground',
        'resistivity': 156,
        'color': (0, 0.85, 0, 1),
    },
    'field_ground': {
        'name': 'Field Ground',
        'resistivity': 45,
        'color': (0.67, 0.67, 0.5, 1),
    },
    'hard_snow': {
        'name': 'Hard Snow',
        'resistivity': 26.66,
        'color': (0.88, 0.87, 0.84, 1),
    },
    'fresh_snow': {
        'name': 'Fresh Snow',
        'resistivity': 5.55,
        'color': (1, 1, 1, 1),
    },
}


class SetFeatureHeightOperatorMixin():
    z: bpy.props.FloatProperty(name='Z')

    def execute(self, context):
        obj = context.object
        with tu.bmesh_from_mesh_for_update(obj.data) as bm:
            for vertice in bm.verts:
                vertice.co[2] = 0
        obj.location[2] = self.z
        return {'FINISHED'}

    def invoke(self, context, event):
        self.z = context.object.location.z
        return context.window_manager.invoke_props_dialog(self)


class TympanSetLevelCurveHeight(SetFeatureHeightOperatorMixin, Operator):
    bl_idname = tu.blty_idname('set_level_curve_height')
    bl_label = 'Set Level Curve Height'
    bl_description = 'Move the level curve to the provided height'

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and et.is_level_curve(obj)


class TympanSetWaterBodyHeight(SetFeatureHeightOperatorMixin, Operator):
    bl_idname = tu.blty_idname('set_water_body_height')
    bl_label = 'Set Water Body Height'
    bl_description = 'Move the water body to the provided height'

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and et.is_water_body(obj)


class TympanSelectObject(Operator):
    bl_idname = tu.blty_idname('select_toggle')
    bl_label = 'Select tympan object'

    obj_name: bpy.props.StringProperty()

    def execute(self, context):
        obj = bpy.data.objects[self.obj_name]
        obj.select_set(not obj.select_get())
        context.view_layer.objects.active = obj
        return {'FINISHED'}


class TympanDisplaySummary(Operator):
    bl_idname = tu.blty_idname('display_summary')
    bl_label = 'Status of tympan objects'

    obj_types: bpy.props.EnumProperty(
        items=tuple(
            [(o, o, o) for o in ['Sources', 'Receivers', 'Infrastructures']]
        )
    )

    def draw(self, context):
        layout = self.layout
        layout.row().prop(self, 'obj_types', expand=True)
        layout.use_property_split = True
        if self.obj_types == 'Sources':
            objs = find_all_soundsources(context)
        elif self.obj_types == 'Receivers':
            objs = find_all_soundreceivers(context)
        else:
            objs = iftr.find_all_infrastructures(context)
        for o in objs:
            row = layout.row()
            row.label(text=o.name)
            row.operator(
                tu.blty_idname('select_toggle'),
                text='',
                icon='RESTRICT_SELECT_OFF'
            ).obj_name = o.name
            row.prop(o, 'tympan_used', text='')

    def execute(self, context):
        return {"FINISHED"}

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self, width=300)


class SpectrumBand(bpy.types.PropertyGroup):
    frequency: bpy.props.FloatProperty(name='frequency', min=0)
    amplitude: bpy.props.FloatProperty(name='module', options=set())


class SpectrumProperty(bpy.types.PropertyGroup):
    def get_toct_amplitudes(self):
        base_amplitudes = tu.spectrum_amplitudes(self)
        if len(self.bands) == len(tu.LOWER_CUTOFF_FREQUENCIES):
            return base_amplitudes

        else:
            first_amplitudes = [base_amplitudes[0]]*2
            last_amplitudes = [base_amplitudes[8]]*2
            center_amplitudes = [amp for amp in base_amplitudes
                                 for i in range(3)]
            return first_amplitudes + center_amplitudes + last_amplitudes

    def get_oct_amplitudes(self):
        base_amplitudes = tu.spectrum_amplitudes(self)
        if len(self.bands) != len(tu.LOWER_CUTOFF_FREQUENCIES):
            return base_amplitudes
        else:
            return [math.fsum(base_amplitudes[i*3+2:i*3+5])/3
                    for i in range(9)]

    def get_dBA(self):
        try:
            from tympan.models import Spectrum
            spectrum = np.array(self.get_toct_amplitudes())
            return str(round(Spectrum(spectrum).dBA, 2))
        except ImportError:
            return "not available"

    # http://personal.cityu.edu.hk/~bsapplec/spectrum.htm
    bands: bpy.props.CollectionProperty(type=SpectrumBand)
    # See OSpectre toTOct() and toOct() methods in Code Tympan for conversion.
    dBA: bpy.props.StringProperty(name='dBA', get=get_dBA)


class SoundSourceProperties(bpy.types.PropertyGroup):
    spectrum: bpy.props.PointerProperty(type=SpectrumProperty)

    def is_user_updatable(self):
        return True


class AbsorptionParameterProperty(bpy.types.PropertyGroup):
    """Building spectrum parameters"""
    spectrum: bpy.props.PointerProperty(
        type=SpectrumProperty,
        name='spectrum',
    )

    def is_user_updatable(self):
        return True


class EquivalentSoundSourceProperties(bpy.types.PropertyGroup):
    name: bpy.props.StringProperty()
    id: bpy.props.IntProperty()
    spectrum: bpy.props.PointerProperty(
        type=SpectrumProperty,
        name="spectrum",
    )
    algorithm: bpy.props.EnumProperty(
        items=(
            ('center', 'Center',
             'Get center of faces', 'CENTER', 0),
            ('subdivide', 'Subdivide',
             'get the grid of a subdivide mesh', 'SUBDIVIDE', 1),
            ('poke', 'Poke',
             'get the distribution by splitting into a triangle fan', 2),
            ('uniform', 'Uniform',
             'get the distribution by drawing a uniform grid', 3),
            ('circular', 'Circular',
             'use a subdivision algorithm adapted to circles', 4),
        ),
        options=set(),
    )
    subdivision_number: bpy.props.IntProperty(default=2, min=1, options=set())
    uniform_step: bpy.props.FloatProperty(
        name='step', default=1, min=0.05, options=set()
    )
    circular_divisions: bpy.props.IntProperty(default=2, min=1, options=set())
    directivity_type: bpy.props.EnumProperty(
        items=[
            ('spherical', 'Spherical',
             'Use spherical directivity'),
            ('surface', 'Surface',
             'Use surface directivity'),
            ('baffled', 'Baffled',
             'Use baffled directivity'),
            ('chimney', 'Chimney',
             'Use chimney directivity'),
        ],
        options=set(),
        default='surface',
        name="type of directivity",
    )
    # Following properties are only for buildings
    active_absorption_parameter: bpy.props.EnumProperty(
        items=[
            ('raw_isolation', 'Raw Isolation',
             'raw isolation building spectrum'),
            ('sabine_inner_absorption', 'Sabine Inner Absorption',
             'sabine inner absorption spectrum'),
            ('sabine_outer_absorption', 'Sabine Outer Absorption',
             'sabine outer absorption spectrum')
        ],
        options=set(),
        name="active absorption parameter")
    raw_isolation: bpy.props.PointerProperty(
        type=AbsorptionParameterProperty,
        options=set(),
        name='raw_isolation',
    )
    sabine_inner_absorption: bpy.props.PointerProperty(
        type=AbsorptionParameterProperty,
        options=set(),
        name='sabine_inner_absorption',
    )
    sabine_outer_absorption: bpy.props.PointerProperty(
        type=AbsorptionParameterProperty,
        options=set(),
        name='sabine_outer_absorption',
    )

    def is_user_updatable(self):
        return True


def update_ground_properties(self, context):
    if self.ground_type != 'user_defined':
        ground = GROUND_TYPES[self.ground_type]
        self.resistivity = ground['resistivity']
        self.material.diffuse_color = ground['color']


def update_resistivity(self, context):
    ground = GROUND_TYPES[self.ground_type]
    if ground["resistivity"] != round(self.resistivity, 2):
        self.ground_type = "user_defined"


class GroundProperty(bpy.types.PropertyGroup):
    id: bpy.props.IntProperty()
    name: bpy.props.StringProperty()
    ground_type: bpy.props.EnumProperty(
        name="Ground Type",
        items=(
            tuple(
                [(k, v["name"], f'resistivity: {v["resistivity"]}')
                 for k, v in GROUND_TYPES.items()]
            )
        ),
        options=set(),
        update=update_ground_properties,
    )
    resistivity: bpy.props.FloatProperty(
        name='Resistivity', default=1, options=set(), update=update_resistivity
    )
    material: bpy.props.PointerProperty(type=bpy.types.Material, options=set())


class TympanCalcFacesEmissivitySpectrum(Operator):
    bl_label = 'Calculate emissivity spectrum of faces selection'
    bl_idname = tu.blty_idname('calc_emissivity_spectrum_of_selected_faces')

    spectrum: bpy.props.PointerProperty(type=SpectrumProperty)

    def draw(self, context):
        self.layout.use_property_split = True
        col = self.layout.column(align=True)
        col.label(text="dBA: {}".format(self.spectrum.dBA))
        col.prop(self.spectrum, 'scale', text='')
        for band in self.spectrum.bands:
            col.prop(band, 'amplitude', text='{0:g}'.format(band.frequency))

    def execute(self, context):
        return {"FINISHED"}

    def invoke(self, context, event):
        infra = context.object
        f_area = iftr.get_selected_emissive_faces_area(infra)
        if not f_area:
            return
        t_area = iftr.get_infra_emissive_area(infra)
        amplitudes, _ = iftr.get_global_emission_spectrum(infra)
        iftr.set_spectrum_amplitudes(self.spectrum, amplitudes, f_area, t_area)
        return context.window_manager.invoke_props_dialog(self, width=300)

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and iftr.has_emitting_faces(obj)


class TympanEditGlobalSpectrum(Operator):
    bl_label = "Edit global emission spectrum"
    bl_idname = tu.blty_idname('edit_global_emission_spectrum')

    spectrum: bpy.props.PointerProperty(type=SpectrumProperty)

    def draw(self, context):
        self.layout.use_property_split = True
        col = self.layout.column(align=True)
        col.label(text="dBA: {}".format(self.spectrum.dBA))
        col.prop(self.spectrum, 'scale', text='')
        for band in self.spectrum.bands:
            col.prop(band, 'amplitude', text='{0:g}'.format(band.frequency))

    def execute(self, context):
        infra = context.active_object
        iftr.safely_set_global_spectrum(
            infra,
            self.spectrum.get_toct_amplitudes(),
        )
        return {"FINISHED"}

    def invoke(self, context, event):
        obj = context.active_object
        amplitudes, _ = iftr.get_global_emission_spectrum_of_machine(obj)
        tu.initialise_spectrum(self.spectrum, amplitudes)
        return context.window_manager.invoke_props_dialog(self, width=300)

    @classmethod
    def poll(cls, context):
        obj = context.active_object
        return (obj and et.is_infrastructure(obj)
                and iftr.has_emitting_faces(obj))


class TympanConfirmEditGlobalSpectrum(Operator):
    """Edit the object's global spectrum, applying scale transforms"""
    bl_label = 'Editing global emission spectrum will apply scale transform'
    bl_idname = tu.blty_idname('confirm_edit_global_emission_spectrum')
    bl_options = {'INTERNAL', 'REGISTER'}

    def execute(self, context):
        bpy.ops.blty.edit_global_emission_spectrum('INVOKE_DEFAULT')
        return {'FINISHED'}

    def invoke(self, context, event):
        infra = context.active_object
        if infra.scale != Vector([1, 1, 1]):
            return context.window_manager.invoke_confirm(self, event)
        return self.execute(context)

    @classmethod
    def poll(cls, context):
        obj = context.active_object
        return obj and iftr.has_emitting_faces(obj)


def find_all_soundsources(context):
    for obj in context.scene.objects:
        if et.is_external_source(obj):
            yield obj


def find_used_soundsources(context):
    for source in find_all_soundsources(context):
        if source.tympan_used:
            yield source


def find_unused_soundsources(context):
    for source in find_all_soundsources(context):
        if not source.tympan_used:
            yield source


class TympanAddPointSoundSource(Operator):
    bl_label = 'Add a Sound Emitter'
    bl_idname = tu.blty_idname('add_sound_source')

    def execute(self, context):
        src = et.make_acoustic_source(context)
        src.location.z = 0
        src.lock_location = (False, False, True)
        return {'FINISHED'}


class SoundReceiverProperties(bpy.types.PropertyGroup):
    spectrum: bpy.props.PointerProperty(type=SpectrumProperty)

    def is_user_updatable(self):
        return False


class TympanAddPointSoundReceiver(Operator):
    bl_label = 'Add a Sound Receiver'
    bl_idname = tu.blty_idname('add_sound_receiver')

    def execute(self, context):
        rec = et.make_acoustic_receiver(context)
        rec.location.z = 0
        rec.lock_location = (False, False, True)
        return {'FINISHED'}


def find_all_soundreceivers(context):
    for obj in context.scene.objects:
        if 'tympan_soundreceiver_properties' in obj:
            yield obj


def find_used_soundreceivers(context):
    for receiver in find_all_soundreceivers(context):
        if receiver.tympan_used:
            yield receiver


def find_unused_soundreceivers(context):
    for receiver in find_all_soundreceivers(context):
        if not receiver.tympan_used:
            yield receiver


class AcousticMaterialProperties(bpy.types.PropertyGroup):
    """Custom properties to describe acoustic material information."""
    name: bpy.props.StringProperty(name='name')
    volumeID: bpy.props.StringProperty(name='volumeID')
    spectrum: bpy.props.PointerProperty(
        type=SpectrumProperty,
        name="spectrum",
    )

    def is_user_updatable(self):
        return True


class TympanMaterial(Operator):
    bl_label = 'Add an acoustic material'
    bl_idname = tu.blty_idname('add_acoustic_material')

    def execute(self, context):
        bpy.ops.material.new()
        active_material = context.object.active_material
        tu.initialise_spectrum(active_material.acoustic_properties.spectrum)
        active_material.acoustic_properties.volumeID = context.object.name
        return {'FINISHED'}


def _handle_objs_outside_landtake(objs, report):
    objs = list(objs)
    for obj in objs:
        obj.tympan_used = False
    if len(objs) == 1:
        message = f'Object "{objs[0].name}" is outside the landtake. '
    elif len(objs) == 2:
        message = (
            f'Objects "{objs[0].name}" and "{objs[1].name}" '
            f'are outside the landtake. '
        )
    else:
        message = (
            f'Objects "{objs[0].name}" and {len(objs) - 1 } '
            f'others are outside the landtake.'
        )
    message += (
        "\nInfrastructures, sources and receivers outside of the landtake will"
        "be ignored."
    )
    report({'ERROR'}, message)


def _landtake_bounds(scene):
    landtake_xsize = scene.landtake_xsize
    landtake_ysize = scene.landtake_ysize
    max_x = landtake_xsize/2
    min_x = -max_x
    max_y = landtake_ysize/2
    min_y = -max_y
    return min_x, max_x, min_y, max_y


def _is_infra_in_landtake(landtake_bounds, infra):
    with tu.bmesh_from_mesh(infra.data) as bm:
        for vert in bm.verts:
            point = infra.matrix_world @ vert.co
            if not geom2d.is_point_in_aabb(landtake_bounds, point):
                return False
    return True


def _ignore_out_of_landtake_objects(context, report):
    scene = context.scene
    landtake_bounds = _landtake_bounds(scene)
    misplaced_objects = set()
    for emitting_obj, position, _, _ in get_all_sources(context):
        if not geom2d.is_point_in_aabb(landtake_bounds, position):
            misplaced_objects.add(emitting_obj)
    for receiver in find_used_soundreceivers(context):
        if not geom2d.is_point_in_aabb(landtake_bounds, receiver.location):
            misplaced_objects.add(receiver)
    for infra in iftr.find_used_infrastructures(context):
        if not _is_infra_in_landtake(landtake_bounds, infra):
            misplaced_objects.add(infra)
    if misplaced_objects:
        _handle_objs_outside_landtake(misplaced_objects, report)


def compute_altimetry(context, report):
    from . import altimetry_builder
    report({'INFO'}, 'computing altimetry...')

    def handle_error(error):
        error_item = context.scene.altimetry_errors.add()
        error_item.message = error

    context.scene.altimetry_errors.clear()
    altimetry_mesh = altimetry_builder.build_altimetry(context, handle_error)
    show_altimetry_errors(context, report)
    return altimetry_mesh


class TympanComputeAltimetry(Operator):
    bl_label = 'Compute altimetry'
    bl_idname = tu.blty_idname('compute_altimetry')
    bl_options = {'UNDO', 'REGISTER'}

    def execute(self, context):
        _ignore_out_of_landtake_objects(context, self.report)
        compute_altimetry(context, self.report)
        return {'FINISHED'}


def show_altimetry_errors(context, report):
    for item in context.scene.altimetry_errors:
        report({'ERROR'}, item.message)


class TympanShowAltimetryBuildErrors(Operator):
    """Display errors that occured during the latest altimetry computation
    """
    bl_label = 'Altimetry Build Errors'
    bl_idname = tu.blty_idname('show_altimetry_errors')
    bl_options = {'UNDO', 'REGISTER'}

    def execute(self, context):
        show_altimetry_errors(context, self.report)
        return {'FINISHED'}

    @classmethod
    def poll(cls, context):
        return context.scene.altimetry_errors != ""


def get_all_sources(context):
    """get sound sources and equivalent sound sources"""
    for src in find_used_soundsources(context):
        source_property = src.tympan_soundsource_properties[0]
        spectrum_property = source_property.spectrum
        amplitudes = spectrum_property.get_toct_amplitudes()
        yield src, src.location, amplitudes, None
    yield from iftr.get_infras_eq_sound_sources(context)


def obj_ray_cast(obj, matrix, ray_origin):
    """Wrapper for blender ray casting"""
    matrix_inv = matrix.inverted()
    ray_origin_obj = matrix_inv @ ray_origin
    success, location, _, _ = obj.ray_cast(ray_origin_obj, Vector([0, 0, 1]))
    if success:
        return location


def apply_sources_receivers_elevation(context, alti):
    '''Reposition sources and receivers to the correct height'''
    for obj in chain(
            find_all_soundsources(context),
            find_all_soundreceivers(context)
    ):
        origin_loc = Vector((obj.location.x, obj.location.y, -1))
        location = obj_ray_cast(alti, alti.matrix_world, origin_loc)
        if location:
            obj.location = (
                location.x,
                location.y,
                location.z + obj.tympan_elevation
            )


def clean_acoustic_elements(context):
    """Reset all sound sources and sound receivers solver indexes"""
    [obj.tympan_solver_index.clear() for obj in chain(
        find_used_soundsources(context),
        find_used_soundreceivers(context),
        iftr.find_used_infrastructures(context),
    )]


def apply_object_transforms(points, obj):
    """Return the coordinates of object vertices after geometric
    transformations"""
    return np.array([obj.matrix_world @ point for point in points])


def linear_pressure_to_dB(spectrum):
    """Change a sound Pressure from linear to dB"""
    ref = 4e-10
    return 10 * np.log10(spectrum / ref)


def get_combined_dB_spectra(result):
    """Return a 2D numpy array of combined sources spectra values per
    receptor (or None if no receptors).
    XXX This function is inspired by the combined_spectra method of ResultModel
        (CODE_TYMPAN/python/tympan/models/_solver.pyx.in)
        In the long run it would be better in the CODE_TYMPAN project.
    """
    if result.nreceptors == 0:
        return None
    if result.nsources == 0:
        return np.zeros((result.nreceptors, 31))
    spectra = np.empty((result.nreceptors, 31))
    for rec in range(result.nreceptors):
        sp_cumul = sum(result.spectrum(rec, src).values
                       for src in range(result.nsources))
        spectra[rec] = linear_pressure_to_dB(sp_cumul)
    return spectra


class ResetSolverParameters(Operator):
    bl_label = "Reset solver parameters"
    bl_idname = tu.blty_idname('reset_solver_parameters')

    def execute(self, context):
        context.scene.property_unset("solver_properties")
        return {'FINISHED'}


class TympanSolverParameters(Operator):
    bl_label = 'solver preferences'
    bl_idname = tu.blty_idname('solver_parameters')
    bl_description = 'Edit the solver parameters'

    def draw(self, context):
        props = context.scene.solver_properties
        layout = self.layout
        gflow = layout.grid_flow(row_major=True, columns=4, align=True)
        gflow.prop(props, 'active_param_category', text='solver', expand=True)
        layout.use_property_split = True
        current_group = props.parameters_groups[props.active_param_category]
        for param_name in current_group:
            col = layout.column()
            col.prop(props, param_name, text=param_name)
            layout.separator()
        layout.operator(
            tu.blty_idname('reset_solver_parameters'),
            text='Reset parameters',
        )

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self, width=500)

    def execute(self, context):
        solve.write_parameters_file(context.scene.solver_properties)
        return {'FINISHED'}


class TympanSolve(Operator):
    bl_label = 'Solve the acoustic problem'
    bl_idname = tu.blty_idname('solve')
    bl_description = 'Solve the acoustic problem'

    def _warn_unsolvable(self, model):
        message = (
            f"Can't run solver with "
            f"{model.nsources} sources and "
            f"{model.nreceptors} receivers"
        )
        self.report({'ERROR'}, message)

    def execute(self, context):
        solve.write_parameters_file(context.scene.solver_properties)
        _ignore_out_of_landtake_objects(context, self.report)
        altimetry_mesh = compute_altimetry(context, self.report)
        if altimetry_mesh is None:
            return {'CANCELLED'}
        clean_acoustic_elements(context)
        # infrastructure
        infra_meshes = iftr.get_infra_meshes(context)
        self.report({'INFO'}, 'building solver model')
        model = solve.build_model(
            altimetry_mesh,
            infra_meshes,
            sources=get_all_sources(context),
            receivers=find_used_soundreceivers(context),
            report=self.report,
        )
        if not model.nsources or not model.nreceptors:
            self._warn_unsolvable(model)
            return {'CANCELLED'}
        solver_name = 'default'
        self.report({'INFO'}, 'running "{}" solver'.format(solver_name))
        result = solve.run_solver(solver_name, model, report=self.report)
        spectra = get_combined_dB_spectra(result)
        for r, spectrum in zip(find_used_soundreceivers(context), spectra):
            receiver_properties = r.tympan_soundreceiver_properties[0]
            bands_property = receiver_properties.spectrum.bands
            for band_idx, amplitude in enumerate(spectrum):
                bands_property[band_idx].amplitude = round(amplitude, 2)
        result_file_path = os.environ['TYMPAN_RESULT_FILE']
        solve.manage_result(bpy.context, result, result_file_path)
        return {'FINISHED'}


class TympanSetOriginToBottom(Operator):
    bl_label = "Set origin to bottom"
    bl_idname = tu.blty_idname('origin_to_bottom')
    bl_description = 'Set object origin at its base'

    def execute(self, context):
        for obj in bpy.context.selected_objects:
            tu.origin_to_bottom(obj)
        return {'FINISHED'}

    @classmethod
    def poll(cls, context):
        return context.object and context.object.type == 'MESH'


class TympanShowHideEqSoundSources(Operator):
    bl_label = "Show/Hide Equivalent Sound Sources"
    bl_idname = tu.blty_idname('show_hide_eqss')
    bl_description = 'Show/Hide equivalent sound sources'

    def execute(self, context):
        active_object = context.active_object
        is_visible = active_object.tympan_display_eqss
        for obj in bpy.context.selected_objects:
            if et.is_infrastructure(obj):
                obj.tympan_display_eqss = not is_visible
                tu.redraw_3d_view()
        return {'FINISHED'}

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and iftr.has_emitting_faces(obj)


def draw_item(self, context):
    self.layout.menu(VIEW3D_MT_TympanMenu.bl_idname)


class VIEW3D_MT_TympanMenu(bpy.types.Menu):
    bl_label = 'Tympan'
    bl_idname = 'VIEW3D_MT_tympan_menu'

    def draw(self, context):
        layout = self.layout
        layout.operator(TympanSetOriginToBottom.bl_idname)
        layout.operator(TympanShowHideEqSoundSources.bl_idname)


class TympanIndexProperties(bpy.types.PropertyGroup):
    """Used to keep solver index of sources and receivers"""
    index: bpy.props.IntProperty()


class TympanWarningDialog(bpy.types.Operator):
    bl_label = ""
    bl_idname = tu.blty_idname('warning_dialog')

    message: bpy.props.StringProperty('')

    def execute(self, context):
        return {'FINISHED'}

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self, width=400)

    def draw(self, context):
        self.layout.label(text=self.message)


def draw_points(shader, points, color):
    if points:
        colors = [color] * len(points)
        mbatch = batch.batch_for_shader(shader, "POINTS",
                                        {"pos": points, "color": colors})
        mbatch.draw(shader)


def draw_tris(shader, points, indices, color):
    colors = [color] * len(points)
    mbatch = batch.batch_for_shader(
        shader, "TRIS",
        {"pos": points, "color": colors},
        indices=indices,
    )
    mbatch.draw(shader)


def draw_lines(shader, points, indices, color):
    if points:
        colors = [color] * len(points)
        mbatch = batch.batch_for_shader(
            shader, 'LINES', {"pos": points, "color": colors}, indices=indices
        )
        mbatch.draw(shader)


def draw_bgl():
    def draw_unused():
        shader.bind()
        bgl.glPointSize(5)
        bgl.glEnable(bgl.GL_DEPTH_TEST)
        context = bpy.context
        color = (0.15, 0.15, 0.15, 1)
        points = [s.location for s in find_unused_soundsources(context)]
        points.extend(
            [r.location for r in find_unused_soundreceivers(context)]
        )
        draw_points(shader, points, color)
        for obj in iftr.find_unused_infrastructures(context):
            mesh = obj.data
            mesh.calc_loop_triangles()
            vertices = [obj.matrix_world @ v.co for v in mesh.vertices]
            indices = [loop.vertices for loop in mesh.loop_triangles]
            draw_tris(shader, vertices, indices, color)

    def draw_used():
        shader.bind()
        bgl.glPointSize(5)
        bgl.glEnable(bgl.GL_DEPTH_TEST)
        context = bpy.context
        used_sources = [
            s.location for s in find_used_soundsources(context)
        ]
        color = (85, 0, 255, 1)
        draw_points(shader, used_sources, color)
        used_receivers = [
            s.location for s in find_used_soundreceivers(context)
        ]
        color = (255, 255, 0, 1)
        draw_points(shader, used_receivers, color)
        eq_sources = iftr.get_eq_sound_sources_for_display()
        color = (0, 0, 255, 1)
        draw_points(shader, eq_sources, color)

    def draw_landtake():
        shader.bind()
        bgl.glLineWidth(1.5)
        bgl.glEnable(bgl.GL_DEPTH_TEST)
        context = bpy.context
        x = context.scene.landtake_xsize / 2
        y = context.scene.landtake_ysize / 2
        landtake = [(-x, y, 0), (x, y, 0), (x, -y, 0), (-x, -y, 0)]
        indices = ((0, 1), (1, 2), (2, 3), (3, 0))
        color = (0.5, 0.3, 0.4, 1)
        draw_lines(shader, landtake, indices, color)

    shader = gpu.shader.from_builtin("3D_FLAT_COLOR")
    space_view_3d = bpy.types.SpaceView3D
    space_view_3d.draw_handler_add(draw_used, (), 'WINDOW', 'POST_VIEW')
    space_view_3d.draw_handler_add(draw_unused, (), 'WINDOW', 'POST_VIEW')
    space_view_3d.draw_handler_add(draw_landtake, (), 'WINDOW', 'POST_VIEW')


bpy.app.timers.register(draw_bgl, first_interval=0)


classes = (
    TympanAddPointSoundSource,
    ResetSolverParameters,
    SpectrumBand,
    SpectrumProperty,
    SoundSourceProperties,
    AbsorptionParameterProperty,
    EquivalentSoundSourceProperties,
    GroundProperty,
    TympanEditGlobalSpectrum,
    TympanConfirmEditGlobalSpectrum,
    TympanAddPointSoundReceiver,
    SoundReceiverProperties,
    TympanIndexProperties,
    AcousticMaterialProperties,
    TympanSetLevelCurveHeight,
    TympanSetWaterBodyHeight,
    TympanSelectObject,
    TympanDisplaySummary,
    TympanComputeAltimetry,
    TympanShowAltimetryBuildErrors,
    TympanSolverParameters,
    TympanSolve,
    TympanSetOriginToBottom,
    TympanShowHideEqSoundSources,
    TympanMaterial,
    TympanWarningDialog,
    VIEW3D_MT_TympanMenu,
    TympanCalcFacesEmissivitySpectrum,
)


def register():
    for cls in classes:
        register_class(cls)
    bpy.types.Object.tympan_soundsource_properties = \
        bpy.props.CollectionProperty(type=SoundSourceProperties)
    bpy.types.Object.tympan_soundreceiver_properties = \
        bpy.props.CollectionProperty(type=SoundReceiverProperties)
    bpy.types.Object.tympan_elevation = \
        bpy.props.FloatProperty(name='Elevation', default=0.0, options=set())
    bpy.types.Material.acoustic_properties = \
        bpy.props.PointerProperty(type=AcousticMaterialProperties)
    bpy.types.Object.tympan_discretization_groups = \
        bpy.props.CollectionProperty(type=EquivalentSoundSourceProperties)
    bpy.types.Object.tympan_grounds = \
        bpy.props.CollectionProperty(type=GroundProperty)
    bpy.types.Object.tympan_solver_index = \
        bpy.props.CollectionProperty(type=TympanIndexProperties)
    bpy.types.Object.parent_building = \
        bpy.props.PointerProperty(name='parent_building',
                                  type=bpy.types.Object,
                                  options=set(),
                                  poll=et.is_building_poll)
    bpy.types.Object.tympan_used = \
        bpy.props.BoolProperty(name='tympan_used', default=True, options=set())
    VIEW3D_MT_object.prepend(draw_item)


def unregister():
    del bpy.types.Object.tympan_elevation
    del bpy.types.Object.tympan_soundsource_properties
    del bpy.types.Object.tympan_soundreceiver_properties
    del bpy.types.Material.acoustic_properties
    del bpy.types.Object.tympan_solver_index
    VIEW3D_MT_object.remove(draw_item)
    for cls in classes:
        unregister_class(cls)
