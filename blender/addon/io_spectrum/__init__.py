from bpy.utils import (
    register_class,
    unregister_class,
)

from blty.io_spectrum.io_csv import (
    TympanExportCSVSpectrum,
    TympanExportCSVEmissionSpectrum,
    TympanExportCSVParameterSpectrum,
    TympanImportCSVSpectrum,
    TympanImportCSVEmissionSpectrum,
    TympanImportCSVParameterSpectrum,
    TympanCopyCSVSpectrum,
    TympanCopyCSVEmissionSpectrum,
    TympanCopyCSVParameterSpectrum,
    TympanPasteSpectrum,
    TympanPasteEmissionSpectrum,
    TympanPasteParameterSpectrum,
)

from blty.io_spectrum.io_html import (
    TympanDisplaySpectrum,
    TympanDisplayEmissionSpectrum,
    TympanDisplayParameterSpectrum,
)


def register():
    register_class(TympanExportCSVSpectrum)
    register_class(TympanExportCSVEmissionSpectrum)
    register_class(TympanExportCSVParameterSpectrum)
    register_class(TympanImportCSVSpectrum)
    register_class(TympanImportCSVEmissionSpectrum)
    register_class(TympanImportCSVParameterSpectrum)
    register_class(TympanCopyCSVSpectrum)
    register_class(TympanCopyCSVEmissionSpectrum)
    register_class(TympanCopyCSVParameterSpectrum)
    register_class(TympanPasteSpectrum)
    register_class(TympanPasteEmissionSpectrum)
    register_class(TympanPasteParameterSpectrum)
    register_class(TympanDisplaySpectrum)
    register_class(TympanDisplayEmissionSpectrum)
    register_class(TympanDisplayParameterSpectrum)


def unregister():
    unregister_class(TympanDisplaySpectrum)
    unregister_class(TympanDisplayEmissionSpectrum)
    unregister_class(TympanDisplayParameterSpectrum)
    unregister_class(TympanPasteSpectrum)
    unregister_class(TympanPasteEmissionSpectrum)
    unregister_class(TympanPasteParameterSpectrum)
    unregister_class(TympanCopyCSVSpectrum)
    unregister_class(TympanCopyCSVEmissionSpectrum)
    unregister_class(TympanCopyCSVParameterSpectrum)
    unregister_class(TympanImportCSVSpectrum)
    unregister_class(TympanImportCSVEmissionSpectrum)
    unregister_class(TympanImportCSVParameterSpectrum)
    unregister_class(TympanExportCSVSpectrum)
    unregister_class(TympanExportCSVEmissionSpectrum)
    unregister_class(TympanExportCSVParameterSpectrum)
