import bpy.ops
import bpy.types
from bpy.types import Operator
from bpy.utils import (
    register_class,
    unregister_class,
)

from . import (
    element_types as et,
    tympan_utils as tu,
)


def make_level_curve(context, level_curve):
    o = add_level_curve(context, name=level_curve.name)
    o.data.from_pydata(level_curve.vertices, level_curve.edges, [])
    o.data.update()
    bpy.ops.blty.set_level_curve_height(z=level_curve.height)


def make_water_body(context, water_body):
    o = add_water_body(context, name=water_body.name)
    with tu.bmesh_from_mesh_for_update(o.data) as bm:
        bm_verts = [bm.verts.new(vertice) for vertice in water_body.vertices]
        for face in water_body.faces:
            bm.faces.new([bm_verts[vert_idx] for vert_idx in face])
    bpy.ops.blty.set_water_body_height(z=water_body.height)


def make_ground(context, ground):
    ground_set = add_ground_set(context, name=ground.name)
    tground = add_ground(ground_set, name=ground.name)
    tground.resistivity = ground.resistivity
    tground.material.diffuse_color = ground.color
    with tu.bmesh_from_mesh_for_update(ground_set.data) as bm:
        bm_verts = [bm.verts.new(vertice) for vertice in ground.vertices]
        for face in ground.faces:
            bm_face = bm.faces.new([bm_verts[vert_idx] for vert_idx in face])
            assign_faces_to_ground(ground_set, bm, [bm_face], tground)


def top_view_edit(context):
    if context.region and context.space_data.type == 'VIEW_3D':
        bpy.ops.view3d.view_axis(type='TOP')
    bpy.ops.object.mode_set(mode='EDIT')
    context.tool_settings.mesh_select_mode = (True, False, False)


def add_level_curve(context, name='Level Curve'):
    bpy.ops.object.add(type="MESH")
    level_curve = context.active_object
    et.set_object_type(level_curve, 'ALTIMETRY')
    et.set_object_subtype(level_curve, 'LEVEL CURVE')
    level_curve.name = name
    return level_curve


class TympanAddLevelCurve(Operator):
    bl_label = 'Add a level curve'
    bl_idname = tu.blty_idname('add_level_curve')
    bl_options = {'UNDO', 'REGISTER'}

    def execute(self, context):
        context.scene.cursor.location.z = 0
        add_level_curve(context)
        top_view_edit(context)
        self.report(
            {'INFO'},
            'Ctrl-Click to add points. '
            + 'Select two ends and type "f" to close.')
        return {'FINISHED'}


def add_water_body(context, name='Water Body'):
    bpy.ops.object.add(type="MESH")
    water_body = context.active_object
    water_body.name = name
    et.set_object_type(water_body, 'ALTIMETRY')
    et.set_object_subtype(water_body, 'WATER BODY')
    mat = bpy.data.materials.new(name='Default Ground')
    water_body.data.materials.append(mat)
    mat.diffuse_color = (0, 0.2, 1, 1)
    return water_body


class TympanAddWaterBody(Operator):
    bl_label = 'Add a water body'
    bl_idname = tu.blty_idname('add_water_body')
    bl_options = {'UNDO', 'REGISTER'}

    def execute(self, context):
        context.scene.cursor.location.z = 0
        add_water_body(context)
        top_view_edit(context)
        self.report(
            {'INFO'},
            'Ctrl-Click to add points. '
            + 'Select two ends and type "f" to close.')
        return {'FINISHED'}


def add_ground_set(context, name="Ground Set"):
    bpy.ops.object.add(type="MESH")
    o = context.active_object
    o.name = name
    et.set_object_type(o, 'ALTIMETRY')
    et.set_object_subtype(o, 'GROUND SET')
    with tu.bmesh_from_mesh_for_update(o.data) as bm:
        bm.faces.layers.int.new('ground')
    return o


class TympanAddGroundSet(Operator):
    bl_label = 'Add a ground set'
    bl_idname = tu.blty_idname('add_ground_set')
    bl_options = {'UNDO', 'REGISTER'}

    def execute(self, context):
        ground_set = add_ground_set(context)
        ground_set.active_ground_index = 0
        add_ground(ground_set, name="Default Ground")
        top_view_edit(context)
        self.report(
            {'INFO'},
            'Ctrl-Click to add points. '
            + 'Select all vertices and type "f" to close.')
        return {'FINISHED'}


def active_ground(ground_set):
    return ground_set.tympan_grounds[ground_set.active_ground_index]


def active_ground_id(ground_set):
    return active_ground(ground_set).id


def faces_with_ground(bm, ground_id):
    ground_layer = bm.faces.layers.int.get('ground')
    for face in bm.faces:
        if face[ground_layer] == ground_id:
            yield face


def add_ground(ground_set, name=None):
    if ground_set.tympan_grounds:
        new_id = ground_set.tympan_grounds[-1].id + 1
    else:
        new_id = 0
    ground = ground_set.tympan_grounds.add()
    ground.id = new_id
    if name is None:
        ground.name = f'Ground {new_id}'
    else:
        ground.name = name
    mat = bpy.data.materials.new(name=ground.name)
    ground_set.data.materials.append(mat)
    ground.material = mat
    return ground


class TympanAddGround(Operator):
    bl_label = 'Add a ground to the active ground set'
    bl_idname = tu.blty_idname('add_ground')
    bl_options = {'UNDO', 'REGISTER'}

    def execute(self, context):
        obj = context.object
        add_ground(obj)
        obj.active_ground_index = len(obj.tympan_grounds) - 1
        return {'FINISHED'}

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and et.is_ground_set(obj)


class TympanRemoveActiveGround(Operator):
    bl_label = 'Remove active ground'
    bl_idname = tu.blty_idname('remove_ground')

    def execute(self, context):
        obj = context.object
        ground_id = active_ground_id(obj)
        with tu.bmesh_from_mesh_for_update(obj.data) as bm:
            ground_layer = bm.faces.layers.int.get('ground')
            for face in faces_with_ground(bm, ground_id):
                face[ground_layer] = 0
                face.material_index = 0
        mat = obj.tympan_grounds[obj.active_ground_index].material
        bpy.data.materials.remove(mat)
        obj.tympan_grounds.remove(obj.active_ground_index)
        obj.active_ground_index = 0
        return {'FINISHED'}

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and et.is_ground_set(obj) and active_ground_id(obj) != 0


class TympanSelectActiveGroundFaces(Operator):
    bl_label = 'Select faces with the active ground'
    bl_idname = tu.blty_idname('select_active_ground_faces')
    bl_options = {'UNDO', 'REGISTER'}

    def execute(self, context):
        obj = context.object
        ground_id = active_ground_id(obj)
        with tu.bmesh_from_mesh_for_update(obj.data) as bm:
            for face in faces_with_ground(bm, ground_id):
                face.select = True
        return {'FINISHED'}

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and et.is_ground_set(obj)


class TympanDeselectActiveGroundFaces(Operator):
    bl_label = 'Deselect faces with the active ground'
    bl_idname = tu.blty_idname('deselect_active_ground_faces')
    bl_options = {'UNDO', 'REGISTER'}

    def execute(self, context):
        obj = context.object
        ground_id = active_ground_id(obj)
        with tu.bmesh_from_mesh_for_update(obj.data) as bm:
            for face in faces_with_ground(bm, ground_id):
                face.select = False
        return {'FINISHED'}

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and et.is_ground_set(obj)


def get_material_index(obj, material):
    for index, mat in enumerate(obj.material_slots):
        if mat.name == material.name:
            return index


def ground_by_id(ground_set, ground_id):
    for ground in ground_set.tympan_grounds:
        if ground.id == ground_id:
            return ground


def assign_faces_to_ground(ground_set, bm,  bm_faces, ground):
    material = ground.material
    material_index = get_material_index(ground_set, material)
    ground_layer = bm.faces.layers.int.get('ground')
    for face in bm_faces:
        face[ground_layer] = ground.id
        face.material_index = material_index


class TympanAssignSeletedFacesToActiveGround(Operator):
    bl_label = 'Assign selected faces to the active ground'
    bl_idname = tu.blty_idname('assign_faces_to_ground')
    bl_options = {'UNDO', 'REGISTER'}

    def execute(self, context):
        obj = context.object
        ground_id = active_ground_id(obj)
        ground = ground_by_id(obj, ground_id)
        with tu.bmesh_from_mesh_for_update(obj.data) as bm:
            bm_faces = [bm_face for bm_face in bm.faces if bm_face.select]
            assign_faces_to_ground(obj, bm, bm_faces, ground)
        return {'FINISHED'}

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and et.is_ground_set(obj)


class TympanUpdateAltimetryProjection(Operator):
    bl_label = 'Update Projection'
    bl_description = 'Re-project the selected image on this altimetry mesh'
    bl_idname = tu.blty_idname('update_altimetry_projection')
    bl_options = {'UNDO', 'REGISTER'}

    def execute(self, context):
        obj = context.object

        topo_maps = (o for o in bpy.data.objects if et.is_topographic_map(o))
        mass_plans = (o for o in bpy.data.objects if et.is_mass_plan(o))
        for idx in range(4):
            tu.remove_texture(obj, f"Image-{idx}")
        projected_images = [*topo_maps, *mass_plans][:4]
        obj.projected_images.clear()

        for projected_image in projected_images:
            item = obj.projected_images.add()
            item.name = projected_image.name

        for idx, image in enumerate(projected_images):
            tu.project_image_empty_on_object(obj, f"Image-{idx}", idx, image)
        return {'FINISHED'}

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and et.is_altimetry_mesh(obj)


class TympanAltimetryErrorsItem(bpy.types.PropertyGroup):
    message: bpy.props.StringProperty()


class TympanProjectedImageNamesItem(bpy.types.PropertyGroup):
    name: bpy.props.StringProperty()


classes = (
    TympanAddLevelCurve,
    TympanAddGround,
    TympanAddWaterBody,
    TympanRemoveActiveGround,
    TympanAddGroundSet,
    TympanSelectActiveGroundFaces,
    TympanDeselectActiveGroundFaces,
    TympanAssignSeletedFacesToActiveGround,
    TympanUpdateAltimetryProjection,
    TympanAltimetryErrorsItem,
    TympanProjectedImageNamesItem,
)


def register():
    for cls in classes:
        register_class(cls)
    # Ground Set properties
    bpy.types.Object.active_ground_index = \
        bpy.props.IntProperty(name='Active ground', default=0,
                              min=0, options=set())
    # Altimetry properties
    bpy.types.Object.projected_images = bpy.props.CollectionProperty(
        type=TympanProjectedImageNamesItem)
    # Landtake properties
    bpy.types.Scene.landtake_xsize = \
        bpy.props.FloatProperty(name="X Size", unit="LENGTH", min=0,
                                soft_min=10, soft_max=10000, default=400)
    bpy.types.Scene.landtake_ysize = \
        bpy.props.FloatProperty(name="Y Size", unit="LENGTH", min=0,
                                soft_min=10, soft_max=10000, default=400)
    bpy.types.Scene.landtake_used_as_level_curve = \
        bpy.props.BoolProperty(name="Use as Level Curve", default=True)
    bpy.types.Scene.landtake_height = \
        bpy.props.FloatProperty(name="Height", unit="LENGTH", min=-100,
                                soft_min=0, soft_max=8848, default=0)
    # Computation properties
    bpy.types.Scene.altimetry_errors = \
        bpy.props.CollectionProperty(
            type=TympanAltimetryErrorsItem,
            name="Latest Altimetry Computation Errors"
        )


def unregister():
    for cls in classes:
        unregister_class(cls)
