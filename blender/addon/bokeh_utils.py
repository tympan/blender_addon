from bokeh.models import NumeralTickFormatter
from bokeh.plotting import (
    figure,
)


def create_histogram(source):
    plot = figure(title="Spectrum", x_axis_type="log")
    plot.quad(
        source=source,
        bottom=0,
        top='values',
        left='left',
        right='right',
        line_color='black',
    )
    plot.xaxis.axis_label = 'Frequencies (HZ)'
    plot.yaxis.axis_label = 'Values'
    plot.min_border_left = 100  # row layout
    plot.xaxis[0].formatter = NumeralTickFormatter(format="0")
    return plot
