"""IO utilities for tympan Blender addon."""

from bokeh.layouts import (
    column,
    widgetbox,
    row,
)
from bokeh.models import (
    ColumnDataSource,
    CustomJS,
)
from bokeh.models.widgets import (
    DataTable,
    Div,
    HTMLTemplateFormatter,
    Select,
    TableColumn,
)
from bokeh.plotting import (
    output_file,
    save,
)
import bpy
from bpy.types import Operator
from bpy.utils import (
    register_class,
    unregister_class,
)
import os

from . import (
    bokeh_utils,
    tympan_utils as tu,
    results,
)


class TympanResultExporter:

    def __init__(self, result):
        self.result = result
        self.result_table_factory = results.ResultTableFactory(result)

    @property
    def tables_by_frequency(self):
        """Precompute all result tables by frequency"""
        tables = {}
        for index, freq in enumerate(tu.LOWER_CUTOFF_FREQUENCIES):
            table_name = "{}Hz".format(freq)
            tables[table_name] = self.result_table_factory.for_frequency(index)
        return tables

    @property
    def couples_spectra_tables(self):
        tables = {}
        for src in self.result.sources:
            tables[src] = {}
            for rec in self.result.receivers:
                tables[src][rec] = [
                    round(f, 2) for f in
                    self.result.get_couple_spectrum(src, rec).to_dB().values
                ]
        return tables

    def _create_main_html_table(self):
        '''Create the results table (global or by frequencies).
        Also create the select widget to switch between frequencies'''
        frequencies = ['{}Hz'.format(f) for f in tu.LOWER_CUTOFF_FREQUENCIES]
        select = Select(
            title="",
            options=["global"] + frequencies,
            width=80,
        )
        columns = [
            TableColumn(field="col1_src", title=""),
            TableColumn(field="col2_lw", title="LW"),
        ]
        template = """
        <div style="background:<%=colors{}%>"; color="white";>
        <%= (value) %></div>
        """
        for index, rec in enumerate(self.result.receivers):
            cformatter = HTMLTemplateFormatter(template=template.format(index))
            columns.append(
                TableColumn(
                    field=rec,
                    title=rec,
                    formatter=cformatter,
                )
            )
        data = {"global": self.result_table_factory.global_table}
        data.update(self.tables_by_frequency)
        source = ColumnDataSource(data['global'])
        main_table = DataTable(source=source, columns=columns, height=250)
        callback = CustomJS(
            args=dict(source=source, data=data, select=select),
            code="""
            // update sources
            es = document.getElementById(select.id);
            value = es.options[es.selectedIndex].value;
            source.data = data[value]
            source.change.emit();
            """
        )
        select.js_on_change('value', callback)
        return widgetbox(select, main_table)

    def _create_couple_spectrum_result(self):
        """Create spectrum table and histogram."""
        section_title = widgetbox(
            Div(text="""<h2>Couples spectra</h2>""")
        )
        select_src = Select(title="Source", value="src1",
                            options=self.result.sources)
        select_rec = Select(title="Receiver", value="rec1",
                            options=self.result.receivers)
        data = self.couples_spectra_tables
        default_data = {
            'freq': tu.LOWER_CUTOFF_FREQUENCIES,
            'values': data[self.result.sources[0]][self.result.receivers[0]],
            'left': tu.LOWER_CUTOFF_FREQUENCIES,
            'right': tu.LOWER_CUTOFF_FREQUENCIES[1:] + (20000,),
        }
        source = ColumnDataSource(default_data)
        columns = [
            TableColumn(field="freq", title="Frequencies"),
            TableColumn(field="values", title="Values"),
        ]
        data_table = DataTable(
            source=source,
            columns=columns,
            height=800,
            width=600,
        )
        histogram = bokeh_utils.create_histogram(source)
        callback = CustomJS(
            args=dict(
                source=source,
                data=data,
                src=select_src,
                rec=select_rec,
            ),
            code="""
            // update sources
            es = document.getElementById(src.id);
            src_value = es.options[es.selectedIndex].value;
            er = document.getElementById(rec.id);
            rec_value = er.options[er.selectedIndex].value;
            source.data.values = data[src_value][rec_value];
            source.change.emit();
            """
        )
        select_src.js_on_change('value', callback)
        select_rec.js_on_change('value', callback)
        return column(
            section_title,
            row(select_src, select_rec),
            row(data_table, histogram),
        )

    def _display_solver_parameters(self):
        props = bpy.context.scene.solver_properties
        categories = list(props.parameters_groups)
        select = Select(title="", options=categories, width=200)
        columns = [
            TableColumn(field="names", title="Param Name"),
            TableColumn(field="values", title="Param Value"),
        ]
        data = {}
        for category in categories:
            data[category] = {'names': [], 'values': []}
            current_group = props.parameters_groups[category]
            for param_name in current_group:
                data[category]['names'].append(param_name)
                data[category]['values'].append(getattr(props, param_name))
        source = ColumnDataSource(data[categories[0]])
        param_table = DataTable(source=source, columns=columns, height=250)
        callback = CustomJS(
            args=dict(source=source, data=data, select=select),
            code="""
            // update sources
            es = document.getElementById(select.id);
            value = es.options[es.selectedIndex].value;
            source.data = data[value]
            source.change.emit();
            """
        )
        select.js_on_change('value', callback)
        return widgetbox(select, param_table)

    def write_html_result(self, result_file_path):
        """Write and display global results."""
        document_title = widgetbox(
            Div(text="""<h1>Tympan Result<h1>""")
        )
        global_section_title = widgetbox(
            Div(text="""<h2>Global/Frequency results</h2>""")
        )
        solver_parameters_section_title = widgetbox(
            Div(text="""<h2>Solver Parameters</h2>""")
        )
        output_file(result_file_path, mode="inline")
        save(column(
            document_title,
            global_section_title,
            self._create_main_html_table(),
            self._create_couple_spectrum_result(),
            solver_parameters_section_title,
            self._display_solver_parameters(),
        ))


class TympanDisplayResults(Operator):
    bl_label = 'Display Solver Results'
    bl_idname = tu.blty_idname('display_results')

    def execute(self, context):
        bpy.ops.wm.path_open(filepath=os.getenv('TYMPAN_RESULT_FILE'))
        return {'FINISHED'}


def register():
    register_class(TympanDisplayResults)


def unregister():
    unregister_class(TympanDisplayResults)
