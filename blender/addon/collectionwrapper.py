import bpy
from bpy.app import handlers

from blty.element_types import (
    is_building,
    is_external_source,
    is_external_machine,
    is_screen,
    is_receiver,
    is_altimetry_mesh,
    is_level_curve,
    is_ground_set,
    is_water_body,
    is_reference_image,
)


class CollectionWrapper:

    def __init__(self, name, children, accepts_fn=None):
        self._name = name
        self._children = children
        self._accepts_fn = accepts_fn

    @property
    def name(self):
        return self._name

    def accepts_object(self, obj):
        return self._accepts_fn and self._accepts_fn(obj)

    def exists(self):
        return self._name in bpy.data.collections

    def get_collection(self):
        return bpy.data.collections[self._name]

    @property
    def children(self):
        return frozenset(self._children)

    def all_wrappers(self):
        yield self
        for child_wrapper in self.children:
            yield from child_wrapper.all_wrappers()


class BuildingsCollectionWrapper(CollectionWrapper):

    @property
    def children(self):

        def accept_fn(building):
            return lambda x: x == building or x.parent_building == building

        buildings = []
        for obj in bpy.context.scene.objects:
            if is_building(obj):
                buildings.append(
                    CollectionWrapper(obj.name, [], accept_fn(obj))
                )
        return frozenset(buildings)


def get_collection_parents(collection, in_collection):
    if collection.name in in_collection.children:
        yield in_collection
    for child_collection in in_collection.children:
        yield from get_collection_parents(collection, child_collection)


class TreeBuilder:

    def __init__(self, root_wrapper):
        self._root_wrapper = root_wrapper

    def build_tree(self, target_collection):
        self._build_tree(target_collection, self._root_wrapper)

    def _build_tree(self, target_collection, wrapper):
        collection = self._get_or_create_collection(wrapper)
        self._ensure_only_in_collection(collection, target_collection)
        for child_wrapper in wrapper.children:
            self._build_tree(collection, child_wrapper)

    def _get_or_create_collection(self, wrapper):
        if wrapper.exists():
            return wrapper.get_collection()
        else:
            return bpy.data.collections.new(wrapper.name)

    def _ensure_only_in_collection(self, collection, target_collection):
        scene_collection = bpy.context.scene.collection
        for parent in get_collection_parents(collection, scene_collection):
            if parent is not target_collection:
                parent.children.unlink(collection)
        if collection.name not in target_collection.children:
            target_collection.children.link(collection)


class TreeUpdater:

    def __init__(self, root_wrapper, tree_builder):
        self._root_wrapper = root_wrapper
        self._tree_builder = tree_builder

    def update(self):
        root_col = bpy.context.scene.collection
        self._tree_builder.build_tree(root_col)
        self._all_wrappers = list(self._root_wrapper.all_wrappers())
        self._place_objects()
        self._remove_other_collections(root_col, [self._root_wrapper])

    def _remove_other_collections(self, from_collection, kept_wrappers):
        """Recursively unlink collections from `in_collection` which are not
        wrapped by a wrapper from `wrappers`."""
        for collection in from_collection.children:
            col_name = collection.name
            try:
                wrapper = next(w for w in kept_wrappers if w.name == col_name)
            except StopIteration:
                self._remove_recursively(collection, from_collection)
                continue
            self._remove_other_collections(collection, wrapper.children)

    def _remove_recursively(self, collection, from_collection):
        for child_collection in collection.children:
            self._remove_recursively(child_collection, collection)
        from_collection.children.unlink(collection)

    def _place_objects(self):
        for obj in bpy.context.scene.objects:
            self._place_object(obj)

    def _place_object(self, obj):
        target_collections = []
        for wrapper in self._all_wrappers:
            if wrapper.accepts_object(obj):
                target_collections.append(wrapper.get_collection())
        scene_collection = bpy.context.scene.collection
        if not target_collections:
            target_collections = [scene_collection]
        for target_collection in target_collections:
            if target_collection not in obj.users_collection:
                target_collection.objects.link(obj)
        for collection in obj.users_collection:
            if collection not in target_collections:
                collection.objects.unlink(obj)


def create_tympan_collection_definition():
    root_wrapper = CollectionWrapper('Tympan', [
        CollectionWrapper('Sources', [], (is_external_source)),
        CollectionWrapper('Receivers', [], is_receiver),
        CollectionWrapper('Infrastructures', [
            CollectionWrapper('Machines', [], (is_external_machine)),
            BuildingsCollectionWrapper('Buildings', []),
            CollectionWrapper('Screens', [], is_screen),
        ]),
        CollectionWrapper('Altimetry', [
            CollectionWrapper('Altimetry Meshes', [], is_altimetry_mesh),
            CollectionWrapper('Level Curves', [], is_level_curve),
            CollectionWrapper('Ground Sets', [], is_ground_set),
            CollectionWrapper('Water Bodies', [], is_water_body),
        ]),
        CollectionWrapper('Reference Images', [], is_reference_image),
    ])
    tree_builder = TreeBuilder(root_wrapper)
    return TreeUpdater(root_wrapper, tree_builder)


def update_project_tree(updater):
    updater.update()


def register_handlers():
    updater = create_tympan_collection_definition()
    handlers.depsgraph_update_post.append(
        handlers.persistent(lambda x: update_project_tree(updater))
    )
