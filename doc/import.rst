Import d'anciens modèles Code Tympan (.xml)
===========================================

L'import d'anciens modèles (fichiers .xml) Code Tympan est disponible depuis
le menu standard ``File > Import`` via l'entrée ``Code Tympan project
(.xml)``.

Actuellement cet import supporte les données suivantes :

* sources ponctuelles positionnées à l'origine
