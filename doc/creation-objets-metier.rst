Création et édition d'objets "métier"
=====================================

Création et édition d'objets Acoustiques
----------------------------------------

L'atelier Code Tympan permet la création et l'édition (partielles) d'objets
métier de types suivants :

* source ponctuelle
* récepteur acoustique

La création de ces objets se fait via l'onglet ``Create`` du panneau de gauche
en sélectionnant le type d'objet voulu dans la section ``Tympan``. La position
des objets dépend de la position courante du curseur. Il est ensuite possible
d'en éditer les propriétés (position, nom, etc.) depuis l'onglet ``Object``
(icône : cube orange) à droite :

.. figure:: images/blender-object-edit.png

   Édition des propriétés d'un objet (ici un récepteur acoustique)

Pour les sources acoustiques, le spectre d'émission est éditable dans le même
panneau :

.. figure:: images/blender-edit-source.png

   Édition du spectre d'une source acoustique


Création et édition d'éléments d'infrastructure
-----------------------------------------------

L'atelier Code Tympan permet la création et l'édition (partielles)
d'éléments d'infrastructure de types suivants :

* Bâtiment
* Écran
* Machine

La création de ces éléments se fait via l'onglet ``Create`` du panneau
de gauche en sélectionnant la forme d'objet voulu dans la section
``Tympan``. La position des objets dépend de la position courante du
curseur. Il est ensuite possible d'en éditer les propriétés (position,
nom, etc.) depuis l'onglet ``Object`` (icône : cube orange) à droite :

.. figure:: images/blender-infra-create.png

   Édition des propriétés d'un objet (ici un bâtiment)


Pour les objets d'infrastructure, il est possible d'ajouter un matériau
et d'éditer son spectre d'absorption depuis l'onglet ``material``
(icône : sphère) à droite :

.. figure:: images/blender-infra-material-edit.png

   Édition du spectre d'absorption d'un matériau


Discrétisation des éléments d'infrastructure rayonnant en sources ponctuelles
-----------------------------------------------------------------------------

Il est possible de rendre rayonnant un ensemble de faces d'un élément
d'infrastructure via le bouton à cocher ``emissive`` présent
au niveau des matériaux. Toutes les faces associées à un matériau
émissif seront donc rayonnantes.

.. figure:: images/blender-infra-emit.png

   Discrétisation d'un bâtiment en sources ponctuelles


Il est ensuite possible d'éditer les propriétés des sources (spectre
et algorithme de discrétisation avec les éventuels paramètres
associés) depuis le sous panel ``Emissivity`` du matériau.

Les algorithmes de discrétisation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Center

  Positionne une source ponctuelle au centre de chaque face émissive.

.. figure:: images/blender-infra-emit-center.png

            Un cube dont toutes les faces sont emissives


* Subdivide

  Subdivise chaque face émissive n fois (subdivision number) et
  positionne une source ponctuelle au centre de chaques sous faces du
  nouveau maillage.

.. figure:: images/blender-infra-emit-subdivide.png

            Un cude dont 2 faces sont émissives (4 subdivisions)


* Poke

  Positionne une source ponctuelle au centre de chaque face émissive
  puis subdivise chaque face émissive en éventail de triangles et
  positionne une source ponctuelle au centre de chaque face ainsi
  créée.

  .. figure:: images/blender-infra-emit-poke.png

            Un cylindre avec sa face circulaire emissive

* Uniform

  Positionne des sources sur une grille alignée avec une arête de la face.
  La densité de la grille peut être spécifiée.

 .. figure:: images/blender-infra-emit-uniform.png

             Un parallélépipède dont toutes les faces sont emissives

* Circular

  Positionne des sources sur des cercles concentriques autour du centre de la face.
  Le nombre de cercles peut être spécifié.

 .. figure:: images/blender-infra-emit-circular.png

             Un cylindre dont la face circulaire du dessus est emissive
