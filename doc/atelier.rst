L'atelier Code Tympan
=====================

Cette section décrit l'atelier Code Tympan dans Blender, c'est-à-dire un
ensemble d'*addons* Blender permettant d'exposer des fonctionnalités métiers
pour Code Tympan dans l'interface de Blender ainsi que des préférences
utilisateurs qui ont pour vocation de fournir une interface utilisable dès le
démarrage, en particulier en sélectionnant les bons éléments d'interface.

Description de l'atelier
------------------------

On part de l'interface par défaut de Blender (2.79) :

.. figure:: images/blender-default-ui.png

   Interface par défaut de Blender

L'idée est de simplifier cette interface pour n'afficher que les éditeurs et
actions pertinents d'un point de vue métier pour Code Tympan.

La :numref:`fig-blender-atelier-tympan` montre une première version de
l'interface de l'atelier Blender/Tympan obtenue au démarrage par défaut.

.. _fig-blender-atelier-tympan:
.. figure:: images/blender-simple.png

   L'atelier Code Tympan dans Blender au démarrage


Les changements effectués sont les suivants (voir l'image où sont mis en
évidence les changements) :

1.  ne pas créer les éléments initiaux de la scène (cube, lampe, caméra) pour
    partir d'une scène vide ;

2.  ne pas afficher l'éditeur inférieur

3.  simplifier l'éditeur de propriétés (à droite)

    *  retirer les panneaux non-pertinents

4.  simplifier la barre de l'éditeur 3D (en bas)

    *  retirer les éléments d'interface existants

    *  laisser la possibilité de choisir entre les rendus graphiques Surface et Fil de fer

    *  permettre à l'utilisateur de choisir entre différentes vues (Dessus, Face, Gauche, Droite)

5.  simplifier la barre d'informations (en haut)

    *  retirer les éléments d'interface existants

    *  conserver la possibilité de naviguer dans les menus conventionels (File, Window, Help)

    * laisser la possibilité de choisir entre plusieurs organisation de fenêtre

6.  ne pas afficher les panneaux d'outils et de propriétés de la vue 3D qui ne sont pas utiles
