Topographie et altimétrie
=========================

La création d'objets topographiques (c'est-à-dire ayant une influence directe
sur l'altimétrie du site) est possible depuis le menu "Create" en utilisant le
bouton "Altimetric feature".

Par exemple, pour créer une courbe de niveau :

1. cliquer sur "Altimetric feature"
2. dessiner les points de la courbe en faisant des clics droits [#clic]_ tout en maintenant la touche
   ``CTRL`` enfoncée ; pour fermer la courbe, sélectionner le premier point et
   taper ``f``
3. passer en vue perspective
4. taper ``g`` puis ``z`` pour activer l'opérateur de translation selon l'axe
   ``Z``
5. déplacer l'objet avec la souris pour fixer son altitude

.. figure:: images/blender-levelcurves.png

   Coubes de niveau saisies depuis l'interface Blender

.. figure:: images/blender-altimetry.png

   Maillage d'altimétrie calculé sur la base de courbes de niveau

.. [#clic] Clic gauche si la sélection se fait avec le clic droit
